-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Vector = require('math/Vector')

import IMG_MASK_HEART from require('Resources')
import ALPHA_MASK, VIDEO_FX from require('Shaders')


TRANSITION_DURATION = 0.6
MIN_BRIGHTNESS = 100
MAX_BRIGHTNESS = 40

TRANSITION_POS = Vector(love.graphics.getDimensions()) / 2
TRANSITION_CENTER = Vector(IMG_MASK_HEART\getDimensions()) / 2


class Game
    new: () =>
        @screens = {}
        @screen, @nextScreen = nil, nil
        @transitionStart, @transitionScale = nil, nil
        @setBrightness(0.5)
        @setContrast(0.5)
        @time = 0

        love.graphics.setShader(VIDEO_FX)

    addScreen: (name, screen) =>
        @screens[name] = screen
        screen\load(@resources)

    showInitialScreen: (name) =>
        @screen = @screens[name]
        @screen\willDisplay(nil)
        @screen\displayed()

    displayScreen: (name) =>
        @changeScreen(@screens[name])

    changeScreen: (nextScreen) =>
        if @transitionStart == nil
            @nextScreen = nextScreen
            @transitionStart = @time

            if @screen
                @screen\willHide(@nextScreen)
                @screen.nextScreen = nil

            @nextScreen\willDisplay(@screen)
        elseif @screen
            @screen.nextScreen = nil

    update: (dt) =>
        @time += dt

        if @screen and @screen.nextScreen
            @displayScreen(@screen.nextScreen)

        if @transitionStart
            if (@time - @transitionStart) >= TRANSITION_DURATION
                @transitionStart = nil
                @nextScreen\displayed()
                @screen = @nextScreen
                @nextScreen = nil

        if @screen
            @screen\update(dt)

        if @transitionStart
            @nextScreen\update(dt)
            progress = @time - @transitionStart
            @transitionScale = 1.5 * ((progress / TRANSITION_DURATION) ^ 3)

    setBrightness: (@brightness) =>
        VIDEO_FX\send('brightness', @brightness * 0.5 - 0.25)

    setContrast: (@contrast) =>
        VIDEO_FX\send('contrast', 0.4 + 1.2 * @contrast)

    draw: () =>
        @screen\draw() if @screen

        if @transitionStart
            love.graphics.setStencil(() ->
                love.graphics.setShader(ALPHA_MASK)
                love.graphics.origin()
                love.graphics.draw(
                    IMG_MASK_HEART, TRANSITION_POS.x, TRANSITION_POS.y, 0, @transitionScale, @transitionScale, TRANSITION_CENTER\vals()
                )
                love.graphics.setShader(VIDEO_FX)
            )
            @nextScreen\draw()
            love.graphics.setStencil()
