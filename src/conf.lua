-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

function love.conf(t)
    t.identity = 'tba'
    t.version = '0.9.2'
    t.window.title = 'TBA Productions - Spread the Löve'
end
