-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Layers = require('Layers')

-- Pre-sort the layers so we don't have to do it each draw()
SortedLayers = [v for k, v in pairs Layers]
table.sort(SortedLayers)


class BaseScreen
    new: (@game) =>
        @sprites, @layers = {}, {}
        @nextScreen = nil

        -- Creates an empty table (set) for a layer
        -- on first access
        setmetatable(@layers, {
            __index: (t, k) ->
                t[k] = {}
                t[k]
        })

    -- Called when the screen is loaded (allows loading resources)
    load: () =>
        sprite\load() for sprite, _ in pairs @sprites

    -- Called right before the screen is displayed
    -- (useful for starting music, etc. that is screen-specific)
    willDisplay: () =>
        -- Stub

    -- Called once the screen is fully displayed
    -- (after the old screen completely disappears)
    displayed: () =>
        -- Stub

    -- Called right before the screen is hidden
    -- (useful for stopping music, etc. that is screen-specific)
    willHide: () =>
        -- Stub

    -- Called on every game update when the screen is active
    update: (dt) =>
        sprite\update(dt) for sprite, _ in pairs @sprites

    -- Called on every draw when the screen is active
    draw: () =>
        for layer in *SortedLayers
            if @layers[layer]
                for sprite, _ in pairs @layers[layer]
                    sprite\draw()

    changeScreen: (screenName) =>
        @nextScreen = screenName

    addSprite: (sprite) =>
        @sprites[sprite] = true
        @layers[sprite.layer][sprite] = true

    removeSprite: (sprite) =>
        @sprites[sprite] = nil
        @layers[sprite.layer][sprite] = nil
