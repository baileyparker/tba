-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseScreen = require('screen/BaseScreen')

import Input, Audio from require('Services')
import
    IMG_CUTSCENE5_FRAMES
    MUS_CUTSCENE5_BG
    FNT_DEBUG
    from require('Resources')


class FinalCutscene extends BaseScreen
    new: (game) =>
        super(game)

        @time = 0
        @image = nil
        @hasKidnapped = false
        @transitioning = false

    willDisplay: () =>
        super()
        @time = 0
        @hasKidnapped = false
        @transitioning = false
        Audio\play(MUS_CUTSCENE5_BG)

    willHide: () =>
        super()
        Audio\stop(MUS_CUTSCENE5_BG)

    update: (dt) =>
        super(dt)

        @advance() if Input\isDown(Input.NAV_SKIP)

        @time += dt

        @image = if(@time < 3)
                     IMG_CUTSCENE5_FRAMES[1]
                 elseif(@time < 7)
                     IMG_CUTSCENE5_FRAMES[2]
                 elseif(@time < 11)
                     IMG_CUTSCENE5_FRAMES[3]
                 elseif(@time < 15)
                     IMG_CUTSCENE5_FRAMES[4]
                 elseif(@time < 19)
                     IMG_CUTSCENE5_FRAMES[5]
                 elseif(@time < 22)
                     IMG_CUTSCENE5_FRAMES[6]
                 elseif(@time < 26)
                     IMG_CUTSCENE5_FRAMES[7]
                 elseif(@time < 30)
                     IMG_CUTSCENE5_FRAMES[8]
                 elseif(@time < 34)
                     IMG_CUTSCENE5_FRAMES[9]
                 elseif(@time < 37)
                     IMG_CUTSCENE5_FRAMES[10]
                 elseif(@time < 40)
                     IMG_CUTSCENE5_FRAMES[11]
                 elseif(@time < 45)
                     IMG_CUTSCENE5_FRAMES[12]
                 elseif(@time < 48)
                     IMG_CUTSCENE5_FRAMES[13]
                 else
                     IMG_CUTSCENE5_FRAMES[14]

        if @time > 52
            @advance()

    advance: () =>
        if not @transitioning
            @transitioning = true
            if @game.score == nil
                @game.score = 1
            @changeScreen('you_win')

    draw: () =>
        if @image
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(@image)

        super()

        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setFont(FNT_DEBUG)
        love.graphics.print('Press SPACE to skip cutscene', 540, 580)

        love.graphics.setColor(255, 255, 255, 255)
