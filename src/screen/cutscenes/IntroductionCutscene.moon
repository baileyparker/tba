-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseScreen = require('screen/BaseScreen')

import Input, Audio from require('Services')
import
    IMG_CUTSCENE1_FRAMES
    MUS_CUTSCENE1_BG
    SFX_CUTSCENE1_KIDNAP
    FNT_DEBUG
    from require('Resources')


class IntroductionCutscene extends BaseScreen
    new: (game) =>
        super(game)

        @time = 0
        @image = nil
        @hasKidnapped = false
        @transitioning = false

    willDisplay: () =>
        super()
        @time = 0
        @hasKidnapped = false
        @transitioning = false
        Audio\play(MUS_CUTSCENE1_BG)

    willHide: () =>
        super()
        Audio\stop(MUS_CUTSCENE1_BG)

    update: (dt) =>
        super(dt)

        @advance() if Input\isDown(Input.NAV_SKIP)

        @time += dt

        @image = if(@time < 2)
                     IMG_CUTSCENE1_FRAMES[1]
                 elseif(@time < 5)
                     IMG_CUTSCENE1_FRAMES[2]
                 elseif(@time < 6)
                     IMG_CUTSCENE1_FRAMES[3]
                 elseif(@time < 9)
                     IMG_CUTSCENE1_FRAMES[4]
                 elseif(@time < 12)
                     IMG_CUTSCENE1_FRAMES[5]
                 elseif(@time < 15)
                     IMG_CUTSCENE1_FRAMES[6]
                 elseif(@time < 18)
                     IMG_CUTSCENE1_FRAMES[7]
                 elseif(@time < 19)
                     IMG_CUTSCENE1_FRAMES[8]
                 elseif(@time < 20)
                     if not @hasKidnapped
                         Audio\play(SFX_CUTSCENE1_KIDNAP)
                         @hasKidnapped = true
                     IMG_CUTSCENE1_FRAMES[9]
                 elseif(@time < 21)
                     IMG_CUTSCENE1_FRAMES[10]
                 elseif(@time < 24)
                     IMG_CUTSCENE1_FRAMES[11]
                 else
                     IMG_CUTSCENE1_FRAMES[12]

        if @time > 27
            @advance()

    advance: () =>
        if not @transitioning
            @transitioning = true
            @changeScreen('stage1')

    draw: () =>
        if @image
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(@image)

        super()

        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setFont(FNT_DEBUG)
        love.graphics.print('Press SPACE to skip cutscene', 540, 580)

        love.graphics.setColor(255, 255, 255, 255)
