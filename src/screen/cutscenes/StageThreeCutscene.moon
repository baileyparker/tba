-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseScreen = require('screen/BaseScreen')

import Input, Audio from require('Services')
import
    IMG_CUTSCENE3_FRAMES
    MUS_CUTSCENE2_BG
    FNT_DEBUG
    from require('Resources')


class StageThreeCutscene extends BaseScreen
    new: (game) =>
        super(game)

        @time = 0
        @image = nil
        @transitioning = false

    willDisplay: () =>
        super()
        @time = 0
        @transitioning = false
        Audio\play(MUS_CUTSCENE2_BG)

    willHide: () =>
        super()
        Audio\stop(MUS_CUTSCENE2_BG)

    update: (dt) =>
        super(dt)

        @advance() if Input\isDown(Input.NAV_SKIP)

        @time += dt

        @image = if(@time < 1)
                     IMG_CUTSCENE3_FRAMES[1]
                 elseif(@time < 4)
                     IMG_CUTSCENE3_FRAMES[2]
                 elseif(@time < 7)
                     IMG_CUTSCENE3_FRAMES[3]
                 elseif(@time < 10)
                     IMG_CUTSCENE3_FRAMES[4]
                 elseif(@time < 12)
                     IMG_CUTSCENE3_FRAMES[5]
                 elseif(@time < 15)
                     IMG_CUTSCENE3_FRAMES[6]
                 elseif(@time < 17)
                     IMG_CUTSCENE3_FRAMES[7]
                 elseif(@time < 20)
                     IMG_CUTSCENE3_FRAMES[8]
                 else
                     IMG_CUTSCENE3_FRAMES[9]

        if(@time >= 22)
            @advance()

    advance: () =>
        if not @transitioning
            @transitioning = true
            @changeScreen('stage3')

    draw: () =>
        if @image
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(@image)

        super()

        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setFont(FNT_DEBUG)
        love.graphics.print('Press SPACE to skip cutscene', 540, 580)

        love.graphics.setColor(255, 255, 255, 255)

