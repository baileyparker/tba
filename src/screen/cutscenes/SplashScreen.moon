-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseScreen = require('screen/BaseScreen')
Vector     = require('math/Vector')

import Audio from require('Services')
import IMG_LOGO, SFX_LOGO from require('Resources')


LOGO_WIDTH = 178
LOGO_HEIGHT = 112

FADE_TIME = 0.5
SFX_START = FADE_TIME + 0.5
T_DOWN = SFX_START + 0.07
T_UP   = SFX_START + 0.27
B_DOWN = SFX_START + 0.33
B_UP   = SFX_START + 0.52
A_DOWN = SFX_START + 0.59
A_UP   = SFX_START + 0.82

FULL_FADE_START = SFX_START + 1.15
FULL_FADE_END   = SFX_START + 1.43
FULL_FADE_TIME  = FULL_FADE_END - FULL_FADE_START

FADE_OUT_START = 3.75


class SpashScreen extends BaseScreen
    new: (game) =>
        super(game)
        @time = 0
        @alpha = 0
        @hasPlayed = false

        @quads = {
            default: love.graphics.newQuad(
                0, 0 * LOGO_HEIGHT, LOGO_WIDTH, LOGO_HEIGHT, IMG_LOGO\getDimensions()
            )
            t_down: love.graphics.newQuad(
                0, 1 * LOGO_HEIGHT, LOGO_WIDTH, LOGO_HEIGHT, IMG_LOGO\getDimensions()
            )
            b_down: love.graphics.newQuad(
                0, 2 * LOGO_HEIGHT, LOGO_WIDTH, LOGO_HEIGHT, IMG_LOGO\getDimensions()
            )
            a_down: love.graphics.newQuad(
                0, 3 * LOGO_HEIGHT, LOGO_WIDTH, LOGO_HEIGHT, IMG_LOGO\getDimensions()
            )
            full: love.graphics.newQuad(
                0, 4 * LOGO_HEIGHT, LOGO_WIDTH, LOGO_HEIGHT, IMG_LOGO\getDimensions()
            )
        }

        @quad = 'default'
        @fullAlpha = 0
        @transitioning = false

        screenDimensions = Vector(love.graphics.getDimensions())
        logoDimensions = Vector(LOGO_WIDTH, LOGO_HEIGHT)
        @logoPos = (screenDimensions - logoDimensions) / 2

    update: (dt) =>
        super(dt)
        @time += dt
        
        @alpha = if @time < FADE_TIME
            (@time / FADE_TIME) * 255
        elseif @time > FADE_OUT_START
            math.max(1 - ((@time - FADE_OUT_START) / FADE_TIME), 0) * 255
        else
            255

        if @time > SFX_START and not @hasPlayed
            @hasPlayed = true
            Audio\play(SFX_LOGO)

        @quad = if T_DOWN <= @time and @time <= T_UP
                    't_down'
                elseif B_DOWN <= @time and @time <= B_UP
                    'b_down'
                elseif A_DOWN <= @time and @time <= A_UP
                    'a_down'
                else
                    'default'

        if @time > FULL_FADE_START
            @fullAlpha = math.min((@time - FULL_FADE_START) / FULL_FADE_TIME, 1) * 255

        if @time > (FADE_OUT_START + FADE_TIME) and not @transitioning
            @transitioning = true
            @changeScreen('title')

    draw: () =>
        super()
        love.graphics.setColor(157, 157, 157, @alpha)
        love.graphics.rectangle('fill', 0, 0, love.graphics.getDimensions())

        love.graphics.setColor(@alpha, @alpha, @alpha, @alpha)
        love.graphics.draw(IMG_LOGO, @quads[@quad], @logoPos\vals())

        if @time <= FADE_OUT_START
            love.graphics.setColor(255, 255, 255, @fullAlpha)
        else
            love.graphics.setColor(@alpha, @alpha, @alpha, @alpha)

        love.graphics.draw(IMG_LOGO, @quads['full'], @logoPos\vals())
