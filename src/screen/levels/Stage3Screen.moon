-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Hero                 = require('sprite/level/Hero')
Enemy                = require('sprite/level/Enemy')
EnemyType4           = require('sprite/level/EnemyType4')
EnemyType8           = require('sprite/level/EnemyType8')
EnemyType9           = require('sprite/level/EnemyType9')
EnemyType10          = require('sprite/level/EnemyType10')
EnemyTypeBoss3       = require('sprite/level/EnemyTypeBoss3')
EnemyShot            = require('sprite/level/EnemyShot')
PlayerMissile        = require('sprite/level/PlayerMissile')
Vector               = require('math/Vector')
CollisionDetector    = require('util/collision/CollisionDetector')
StageBannerEffect    = require('sprite/level/StageBannerEffect')
BaseLevelScreen      = require('screen/levels/BaseLevelScreen')
ChargeCompleteEffect = require('sprite/level/ChargeCompleteEffect')
BarFillEffect        = require('sprite/level/BarFillEffect')
DialogueManager      = require('sprite/level/DialogueManager')

import Input, Campaign, Audio from require('Services')
import
    IMG_LEVEL3_BG
    IMG_LEVEL3_CLOUDS
    MUS_LEVEL3_BG
    MUS_LEVEL3_BOSS
    FNT_HEADING
    SFX_PLAYERSHOT
    SFX_POWERUP
    SFX_ASTRALUP
    from require('Resources')


class Stage3Screen extends BaseLevelScreen
    new: (game) =>
        super(game)

        @loveregenpertime = 125  --reset in prestage dialogue

        @shottype = 2

        @coroutines = {}

        @controltime = 0 --used specifically for stage control
        @enemyspawncontrol = -1 --used for controlling when things happen in stage. -1 is pre-stage dialogue
        @drawbanneralpha = 0
        @onpause = false

        @blanketcooldown = 0
        @blanketactive = false

        @prestagedialogue()

    reset: () =>
        @game\addScreen('stage3', Stage3Screen(@game))

    restart: () =>
        super()
        @changeScreen('stage3')

    controlStage: (dt) =>
        if @enemyspawncontrol == 0 and @controltime > 1
            newenemy = EnemyType8(Vector(850, 200), Vector(-90, 0), self, 5)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            newenemy = EnemyType8(Vector(850, 400), Vector(-90, 0), self, 5)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            @enemyspawncontrol = 1

        if @enemyspawncontrol == 1 and @controltime > 7
            for i = -3, 3 do
                newenemy = EnemyType8(Vector(500, 300)+480*Vector(math.cos(math.rad(i*20)), math.sin(math.rad(i*20))), -80*Vector(math.cos(math.rad(i*20)), math.sin(math.rad(i*20))), self, 5)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            @enemyspawncontrol = 2

        if @enemyspawncontrol == 2 and @controltime > 14
            --Provision for stage banner and potential dialogue event
            @drawBanner()
            @enemyspawncontrol = 3.25
            @onpause = true

        if @enemyspawncontrol == 3.25 and @controltime > 16
            d1 = DialogueManager(0, 303, self, 4)
            @addSprite(d1)
            @enemyspawncontrol = 3.5

        if @enemyspawncontrol == 3.5 and @controltime > 15 and @onpause == false
            @enemyspawncontrol = 4

        if @enemyspawncontrol == 3 and @controltime > 15 and @onpause == false
            newenemy = EnemyType9(Vector(500, -64), Vector(0, 0), 200, self, 18)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            newenemy = EnemyType9(Vector(500, 664), Vector(0, 0), 400, self, 18)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            @enemyspawncontrol = 4

        if @enemyspawncontrol == 4 and @controltime > 24 and @onpause == false
            newenemy = EnemyType10(Vector(864, 300), Vector(0, 0), 600, self, 24)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            @enemyspawncontrol = 5

        if @enemyspawncontrol == 5 and @controltime > 30 and @onpause == false
            newenemy4 = EnemyType4(Vector(800, 300), Vector(-50, 0), self, 24)
            newenemy4\load(@resources)
            @addSprite(newenemy4)
            @enemyCount += 1
            @enemyspawncontrol = 6

        if @enemyspawncontrol == 6 and @controltime > 38 and @onpause == false
            for i = 0, 4
                newenemy8 = EnemyType8(Vector(420 + i*75, -64), Vector(0, 50 + i*30), self, 5)
                newenemy8\load(@resources)
                @addSprite(newenemy8)
                @enemyCount += 1
                newenemy8 = EnemyType8(Vector(420 + i*75, 664), Vector(0, -50 - i*30), self, 5)
                newenemy8\load(@resources)
                @addSprite(newenemy8)
                @enemyCount += 1
            @enemyspawncontrol = 7

        if @enemyspawncontrol == 7 and @controltime > 45 and @onpause == false
            newenemy = EnemyType10(Vector(864, 100), Vector(0, 0), 500, self, 24)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            newenemy = EnemyType10(Vector(864, 500), Vector(0, 0), 500, self, 24)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            @enemyspawncontrol = 8

        if @enemyspawncontrol == 8 and @controltime > 56
            --Provision for Pre-Boss Dialogue
            @enemyspawncontrol = 100 --temporary. Should be set in the dialogue.

        if @enemyspawncontrol == 100 and @enemyCount <= 0
            Audio\stop(MUS_LEVEL3_BG)
            Audio\play(MUS_LEVEL3_BOSS)

            --Provision for Boss
            boss3 = EnemyTypeBoss3(Vector(1200, 300), Vector(-25, 0), self, 600)
            boss3\load(@resources)
            @addSprite(boss3)
            @boss = boss3
            @enemyCount += 1
            @shotenabled = false

            @enemyspawncontrol = 150

        if @enemyspawncontrol == 150 and @controltime > 57 -- Dialogue with Boss
            @prebossdialogue()

        if @enemyspawncontrol == 199 and @controltime > 58 -- End Dialogue
            @boss.dialoguestatus = 1 --enable boss firing and moving
            @shotenabled = true
            @enemyspawncontrol = 200

        if @enemyspawncontrol == 200 and @enemyCount <= 0
            --Provision for Post-Boss Dialogue
            @enemyspawncontrol = 250 --temporary. Should be set in the dialogue.
            @controltime = 58
            @killBullets()
            d1 = DialogueManager(0, 306, self)
            @addSprite(d1)

        if @enemyspawncontrol == 250 and @enemyCount <= 0 and @controltime > 61
            @enemyspawncontrol = 251
            Campaign\saveProgress(3)
            @game.screens['stage4'].score += @score
            @changeScreen('cutscene4')
            @reset()

    prestagedialogue: () =>
        dialogue_co = coroutine.create(() ->
            Audio\stop(MUS_LEVEL3_BG)
            @loveregenpertime = 0
            d1 = DialogueManager(0, 301, self)
            @addSprite(d1)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d2 = DialogueManager(0, 302, self)
            @addSprite(d2)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            @enemyspawncontrol = 0
            Audio\play(MUS_LEVEL3_BG)
            @loveregenpertime = 125
        )
        table.insert(@coroutines, dialogue_co)

    prebossdialogue: () =>
        dialogue_co = coroutine.create(() ->
            @enemyspawncontrol = 151
            d1 = DialogueManager(0, 304, self)
            @addSprite(d1)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d2 = DialogueManager(1, 305, self)
            @addSprite(d2)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()

            @enemyspawncontrol = 199
        )
        table.insert(@coroutines, dialogue_co)

    drawBanner: () =>
        for i = -12, 12 do
            for j = -4, 4 do
                if math.abs(i) > 8 or math.abs(j) > 2
                    s = StageBannerEffect(Vector(math.random(1000)-100, math.random(800)-100), Vector(400 + i*16, 200 + j*16), 128, 96, 255, self)
                    s\load(@resources)
                    @addSprite(s, false)

        @drawbanneralpha = 1

    drawChargeCompleteEffect: () =>
        for i = 0, 35 do
            eff = ChargeCompleteEffect(Vector(136-4, 576-4), Vector(15*math.cos(i*(math.pi*2/36)), 15*math.sin(i*(math.pi*2/36))), 0, 192, 255, self)
            eff\load(@resources)
            @addSprite(eff, false)

    addBarFillEffect: () =>
        eff = BarFillEffect(Vector(22 + 124 - @blanketcooldown * 124 / 15, 576), 0, 192, 255, self)
        eff\load(@resources)
        @addSprite(eff, false)

    willDisplay: () =>
        super()

        --Audio\play(MUS_LEVEL3_BG)

    willHide: () =>
        super()

        Audio\stop(MUS_LEVEL3_BG)
        Audio\stop(MUS_LEVEL3_BOSS)

    _togglePauseMenu: () =>
        super()

        if @pauseMenu
            if Audio\isPlaying(MUS_LEVEL3_BG)
                Audio\pause(MUS_LEVEL3_BG)
            if Audio\isPlaying(MUS_LEVEL3_BOSS)
                Audio\pause(MUS_LEVEL3_BOSS)
        else
            if Audio\isPaused(MUS_LEVEL3_BG)
                Audio\resume(MUS_LEVEL3_BG)
            if Audio\isPaused(MUS_LEVEL3_BOSS)
                Audio\resume(MUS_LEVEL3_BOSS)

    update: (dt) =>
        if love.keyboard.isDown(';')
            dt *= 10

        for i, cr in ipairs(@coroutines)
                coroutine.resume(cr, dt) if coroutine.status(cr) != "dead"

        if love.keyboard.isDown('l') and @time > 0.5 --next stage
            @enemyspawncontrol = 250
            @enemyCount = -80 --manual override
            @controltime = 5000

        super(dt)

        if not @pauseMenu and @enemyspawncontrol >= 0
            @controltime += dt

            if @drawbanneralpha > 0 and @drawbanneralpha < 255
                @drawbanneralpha += (255 / 5) * dt
            else
                @drawbanneralpha = 0
                @onpause = false

            --Power Ups
            if Input\isDown(Input.POWERUP) and @blanketcooldown <= 0 and not(@hero.exploding) --blanket
                @hero.blanket = true
                @hero.blanketlength = 3
                @blanketcooldown = 18
                Audio\play(SFX_POWERUP)
                @blanketactive = true
            else
                @blanketcooldown -= dt

            if @blanketcooldown > 0 and @hero.blanketlength <= 0
                @addBarFillEffect()

            if @blanketcooldown <= 0 --correction for cooldown bar just in case
                @blanketcooldown = 0
                if @blanketactive
                    @blanketactive = false
                    Audio\play(SFX_ASTRALUP)
                    @drawChargeCompleteEffect()

    draw: () =>
        -- Background
        if @enemyspawncontrol >= 0
            scrollableWidth = IMG_LEVEL3_BG\getWidth() - 800
            scrollAmount = math.min(@controltime / 56, 1)
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(IMG_LEVEL3_BG, -math.floor(scrollAmount * scrollableWidth))

            scrollableWidth = IMG_LEVEL3_CLOUDS\getWidth() - 800
            love.graphics.draw(IMG_LEVEL3_CLOUDS, -math.floor(scrollAmount * scrollableWidth))
        else
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(IMG_LEVEL3_BG, 0)
            love.graphics.draw(IMG_LEVEL3_CLOUDS, 0)

        --scrollableWidth = @planets\getWidth() - 800
        --love.graphics.draw(@planets, -math.floor(scrollAmount * scrollableWidth))

        --START Cooldown Bars
        love.graphics.origin()
        blanketrectlength = 128
        love.graphics.setColor(0, 128, 255, 255)
        love.graphics.rectangle('line', 16, 568, blanketrectlength, 16) --border

        love.graphics.setColor(0, 192, 255, 255)
        if @hero.blanketlength > 0 --currently using blanket
            blanketrectlength = @hero.blanketlength*124/3
            love.graphics.setColor(0, 192, 255, 255)
        elseif @blanketcooldown > 0 --currently on cooldown
            blanketrectlength = 124 - @blanketcooldown * 124 / 15
            if blanketrectlength < 0 --To prevent weird and accidental graphical glitches
                blanketrectlength = 0
            love.graphics.setColor(0, 192-@blanketcooldown*8, 255-@blanketcooldown*8, 255)
        else
            blanketrectlength = 124

        love.graphics.rectangle('fill', 18, 570, blanketrectlength, 12)

        --END Cooldown Bars

        if @drawbanneralpha > 0 and @drawbanneralpha < 255
            love.graphics.origin()
            love.graphics.setColor(192, 150, 255, @drawbanneralpha)
            love.graphics.setFont(FNT_HEADING)
            text = 'Chapter 3'
            width, height = FNT_HEADING\getWidth(text), FNT_HEADING\getHeight(text)
            love.graphics.print(text, 400 - width/2, 200 - height/2)

        -- Draw rest of sprites
        super()
