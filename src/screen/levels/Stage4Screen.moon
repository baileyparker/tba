-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Hero                 = require('sprite/level/Hero')
Enemy                = require('sprite/level/Enemy')
EnemyType11          = require('sprite/level/EnemyType11')
EnemyType12          = require('sprite/level/EnemyType12')
EnemyType13          = require('sprite/level/EnemyType13')
EnemyTypeBoss4       = require('sprite/level/EnemyTypeBoss4')
EnemyShot            = require('sprite/level/EnemyShot')
EnemyLaser           = require('sprite/level/EnemyLaser')
PlayerMissile        = require('sprite/level/PlayerMissile')
Vector               = require('math/Vector')
CollisionDetector    = require('util/collision/CollisionDetector')
StageBannerEffect    = require('sprite/level/StageBannerEffect')
BaseLevelScreen      = require('screen/levels/BaseLevelScreen')
ChargeCompleteEffect = require('sprite/level/ChargeCompleteEffect')
BarFillEffect        = require('sprite/level/BarFillEffect')
DialogueManager      = require('sprite/level/DialogueManager')

import Input, Campaign, Audio from require('Services')
import
    IMG_LEVEL4_BG
    MUS_LEVEL4_BG
    MUS_LEVEL4_BOSS
    FNT_HEADING
    SFX_PLAYERSHOT
    SFX_POWERUP
    SFX_ASTRALUP
    from require('Resources')


class Stage4Screen extends BaseLevelScreen
    new: (game) =>
        super(game)

        @loveregenpertime = 125

        @shottype = 2

        @coroutines = {}

        @controltime = 0 --used specifically for stage control
        @enemyspawncontrol = -1 --used for controlling when things happen in stage. -1 is pre-stage dialogue
        @drawbanneralpha = 0
        @onpause = false

        @blanketcooldown = 0
        @blanketactive = false

        @prestagedialogue()

        --jump to boss
        --@enemyspawncontrol = 100
        --@controltime = 50
        --@love = @maxlove 

    reset: () =>
        @game\addScreen('stage4', Stage4Screen(@game))

    restart: () =>
        super()
        @changeScreen('stage4')

    controlStage: (dt) =>
        if @enemyspawncontrol == 0 and @controltime > 1
            newenemy11 = EnemyType11(Vector(80, 664), Vector(0, -80), self, 5)
            newenemy11\load(@resources)
            @addSprite(newenemy11)
            @enemyCount += 1
            @enemyspawncontrol = 0.1
        if @enemyspawncontrol == 0.1 and @controltime > 2
            newenemy11 = EnemyType11(Vector(240, 664), Vector(0, -80), self, 5)
            newenemy11\load(@resources)
            @addSprite(newenemy11)
            @enemyCount += 1
            @enemyspawncontrol = 0.2
        if @enemyspawncontrol == 0.2 and @controltime > 3
            newenemy11 = EnemyType11(Vector(400, 664), Vector(0, -80), self, 5)
            newenemy11\load(@resources)
            @addSprite(newenemy11)
            @enemyCount += 1
            @enemyspawncontrol = 0.3
        if @enemyspawncontrol == 0.3 and @controltime > 6
            newenemy11 = EnemyType11(Vector(560, -64), Vector(0, 80), self, 5)
            newenemy11\load(@resources)
            @addSprite(newenemy11)
            @enemyCount += 1
            @enemyspawncontrol = 0.4
        if @enemyspawncontrol == 0.4 and @controltime > 7
            newenemy11 = EnemyType11(Vector(720, -64), Vector(0, 80), self, 5)
            newenemy11\load(@resources)
            @addSprite(newenemy11)
            @enemyCount += 1
            @enemyspawncontrol = 2

        if @enemyspawncontrol == 2 and @controltime > 14
            --Provision for stage banner and potential dialogue event
            @drawBanner()
            @enemyspawncontrol = 3
            @onpause = true
            @prepareLava()

        if @enemyspawncontrol == 3 and @controltime > 15 and @onpause == false
            newenemy = EnemyType12(Vector(864, 200), Vector(0, 0), self, 18)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            newenemy = EnemyType12(Vector(864, 400), Vector(0, 0), self, 18)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            @enemyspawncontrol = 4

        if @enemyspawncontrol == 4 and @controltime > 32 and @onpause == false
            @gravityline()
            @enemyspawncontrol = 5

        if @enemyspawncontrol == 5 and @controltime > 50
            --Provision for Pre-Boss Dialogue
            @enemyspawncontrol = 100 --temporary. Should be set in the dialogue.

        if @enemyspawncontrol == 100 and @enemyCount <= 0
            Audio\stop(MUS_LEVEL4_BG)
            Audio\play(MUS_LEVEL4_BOSS)

            --Provision for Boss
            boss4 = EnemyTypeBoss4(Vector(1200, 300), Vector(-25, 0), self, 3000)
            boss4\load(@resources)
            @addSprite(boss4)
            @boss = boss4
            @prepareLava2()
            @enemyCount += 1
            @shotenabled = false

            @enemyspawncontrol = 150

        if @enemyspawncontrol == 150 and @controltime > 51 -- Dialogue with Boss
            @prebossdialogue()

        if @enemyspawncontrol == 199 and @controltime > 52 -- End Dialogue
            @boss.dialoguestatus = 1 --enable boss firing and moving
            @shotenabled = true
            @enemyspawncontrol = 200

        if @enemyspawncontrol == 200 and @enemyCount <= 0
            --Provision for Post-Boss Dialogue
            @enemyspawncontrol = 250 --temporary. Should be set in the dialogue.
            @controltime = 52
            @killBullets()

        if @enemyspawncontrol == 250 and @enemyCount <= 0 and @controltime > 55
            @enemyspawncontrol = 251
            Campaign\saveProgress(4)
            @game.score = @score
            @changeScreen('cutscene5')
            @reset()

    prepareLava: () =>
        lava_co = coroutine.create(() ->
            while @enemyspawncontrol < 100
                newbullet = EnemyShot(Vector(math.random(800), 600), Vector(0, -50 - math.random(150)), EnemyShot.SHOT_GRAPHIC_BALL_RED, self)
                newbullet\load(@resources)
                @addSprite(newbullet)
                time = 0
                while time < 0.2
                    time += coroutine.yield()
        )
        table.insert(@coroutines, lava_co)
        coroutine.resume(lava_co)

    prepareLava2: () =>
        lava_co = coroutine.create(() ->
            while @boss.hp > 500
                coroutine.yield()
            while @boss.hp > 0
                graph = EnemyShot.SHOT_GRAPHIC_DOT_MIDNIGHT
                if @boss.hp < 250
                    graph = EnemyShot.SHOT_GRAPHIC_DOT_CORAL
                newbullet = EnemyShot(Vector(math.random(800), 600), Vector(0, -50 - math.random(100)), graph, self)
                newbullet\load(@resources)
                @addSprite(newbullet)
                time = 0
                while time < 0.5
                    time += coroutine.yield()
        )
        table.insert(@coroutines, lava_co)
        coroutine.resume(lava_co)

    gravityline: () =>
        gravity_co = coroutine.create(() ->
            --Spawn lines of 5 gravity spewing enemies
            for i = 0, 4
                timeinit = @time
                while @time < timeinit + 0.25
                    coroutine.yield()
                newenemy = EnemyType13(Vector(864, 64), Vector(-150, 50), self, 3)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            for i = 0, 4
                timeinit = @time
                while @time < timeinit + 0.25
                    coroutine.yield()
                newenemy = EnemyType13(Vector(864, 320), Vector(-250, -40), self, 3)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            for i = 0, 4
                timeinit = @time
                while @time < timeinit + 0.25
                    coroutine.yield()
                newenemy = EnemyType13(Vector(864, 230), Vector(-100, 12), self, 3)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            for i = 0, 4
                timeinit = @time
                while @time < timeinit + 0.25
                    coroutine.yield()
                newenemy = EnemyType13(Vector(864, 420), Vector(-200, -20), self, 3)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
        )
        table.insert(@coroutines, gravity_co)

    prestagedialogue: () =>
        dialogue_co = coroutine.create(() ->
            Audio\stop(MUS_LEVEL4_BG)
            @loveregenpertime = 0
            d1 = DialogueManager(0, 401, self)
            @addSprite(d1)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d2 = DialogueManager(0, 402, self)
            @addSprite(d2)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            @enemyspawncontrol = 0
            Audio\play(MUS_LEVEL4_BG)
            @loveregenpertime = 175
        )
        table.insert(@coroutines, dialogue_co)

    prebossdialogue: () =>
        dialogue_co = coroutine.create(() ->
            @enemyspawncontrol = 151
            d1 = DialogueManager(0, 403, self)
            @addSprite(d1)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d2 = DialogueManager(1, 404, self)
            @addSprite(d2)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d3 = DialogueManager(0, 405, self)
            @addSprite(d3)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d4 = DialogueManager(1, 406, self)
            @addSprite(d4)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d5 = DialogueManager(0, 407, self)
            @addSprite(d5)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d6 = DialogueManager(1, 408, self)
            @addSprite(d6)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d7 = DialogueManager(0, 409, self)
            @addSprite(d7)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d8 = DialogueManager(1, 410, self)
            @addSprite(d8)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()

            @enemyspawncontrol = 199
        )
        table.insert(@coroutines, dialogue_co)

    drawBanner: () =>
        for i = -12, 12 do
            for j = -4, 4 do
                if math.abs(i) > 8 or math.abs(j) > 2
                    s = StageBannerEffect(Vector(math.random(1000)-100, math.random(800)-100), Vector(400 + i*16, 200 + j*16), 255, 96, 255, self)
                    s\load(@resources)
                    @addSprite(s, false)

        @drawbanneralpha = 1

    drawChargeCompleteEffect: () =>
        for i = 0, 35 do
            eff = ChargeCompleteEffect(Vector(136-4, 576-4), Vector(15*math.cos(i*(math.pi*2/36)), 15*math.sin(i*(math.pi*2/36))), 0, 192, 255, self)
            eff\load(@resources)
            @addSprite(eff, false)

    addBarFillEffect: () =>
        eff = BarFillEffect(Vector(22 + 124 - @blanketcooldown * 124 / 15, 576), 0, 192, 255, self)
        eff\load(@resources)
        @addSprite(eff, false)

    willDisplay: () =>
        super()

        --Audio\play(MUS_LEVEL4_BG)

    willHide: () =>
        super()

        Audio\stop(MUS_LEVEL4_BG)
        Audio\stop(MUS_LEVEL4_BOSS)

    _togglePauseMenu: () =>
        super()

        if @pauseMenu
            if Audio\isPlaying(MUS_LEVEL4_BG)
                Audio\pause(MUS_LEVEL4_BG)
            if Audio\isPlaying(MUS_LEVEL4_BOSS)
                Audio\pause(MUS_LEVEL4_BOSS)
        else
            if Audio\isPaused(MUS_LEVEL4_BG)
                Audio\resume(MUS_LEVEL4_BG)
            if Audio\isPaused(MUS_LEVEL4_BOSS)
                Audio\resume(MUS_LEVEL4_BOSS)

    update: (dt) =>
        if love.keyboard.isDown(';')
            dt *= 10

        for i, cr in ipairs(@coroutines)
                --print(coroutine.status(cr))
                coroutine.resume(cr, dt) if coroutine.status(cr) != "dead"

        super(dt)

        --if love.keyboard.isDown('l') and @time > 0.5 --next stage
        --    @enemyspawncontrol = 250
        --    @enemyCount = -80 --manual override
        --    @controltime = 5000

        if not @pauseMenu and @enemyspawncontrol >= 0
            @controltime += dt

            if @drawbanneralpha > 0 and @drawbanneralpha < 255
                @drawbanneralpha += (255 / 5) * dt
            else
                @drawbanneralpha = 0
                @onpause = false

            --Power Ups
            if Input\isDown(Input.POWERUP) and @blanketcooldown <= 0 and not(@hero.exploding) --blanket
                @hero.blanket = true
                @hero.blanketlength = 3
                @blanketcooldown = 18
                Audio\play(SFX_POWERUP)
                @blanketactive = true
            else
                @blanketcooldown -= dt

            if @blanketcooldown > 0 and @hero.blanketlength <= 0
                @addBarFillEffect()

            if @blanketcooldown <= 0 --correction for cooldown bar just in case
                @blanketcooldown = 0
                if @blanketactive
                    @blanketactive = false
                    Audio\play(SFX_ASTRALUP)
                    @drawChargeCompleteEffect()

    draw: () =>
        -- Background
        if @enemyspawncontrol >= 0
            scrollableWidth = IMG_LEVEL4_BG\getWidth() - 800
            scrollAmount = math.min(@controltime / 50, 1)
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(IMG_LEVEL4_BG, -math.floor(scrollAmount * scrollableWidth))
        else
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(IMG_LEVEL4_BG, 0)

            --scrollableWidth = @planets\getWidth() - 800
            --love.graphics.draw(@planets, -math.floor(scrollAmount * scrollableWidth))

        --START Cooldown Bars
        love.graphics.origin()
        blanketrectlength = 128
        love.graphics.setColor(0, 128, 255, 255)
        love.graphics.rectangle('line', 16, 568, blanketrectlength, 16) --border

        love.graphics.setColor(0, 192, 255, 255)
        if @hero.blanketlength > 0 --currently using blanket
            blanketrectlength = @hero.blanketlength*124/3
            love.graphics.setColor(0, 192, 255, 255)
        elseif @blanketcooldown > 0 --currently on cooldown
            blanketrectlength = 124 - @blanketcooldown * 124 / 15
            if blanketrectlength < 0 --To prevent weird and accidental graphical glitches
                blanketrectlength = 0
            love.graphics.setColor(0, 192-@blanketcooldown*8, 255-@blanketcooldown*8, 255)
        else
            blanketrectlength = 124

        love.graphics.rectangle('fill', 18, 570, blanketrectlength, 12)

        --END Cooldown Bars

        if @drawbanneralpha > 0 and @drawbanneralpha < 255
            love.graphics.origin()
            love.graphics.setColor(192, 150, 255, @drawbanneralpha)
            love.graphics.setFont(FNT_HEADING)
            text = 'Chapter 4'
            width, height = FNT_HEADING\getWidth(text), FNT_HEADING\getHeight(text)
            love.graphics.print(text, 400 - width/2, 200 - height/2)

        -- Draw rest of sprites
        super()
