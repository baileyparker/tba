-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Hero              = require('sprite/level/Hero')
Enemy             = require('sprite/level/Enemy')
EnemyType1        = require('sprite/level/EnemyType1')
EnemyType2        = require('sprite/level/EnemyType2')
EnemyType3        = require('sprite/level/EnemyType3')
EnemyType4        = require('sprite/level/EnemyType4')
EnemyTypeBoss1    = require('sprite/level/EnemyTypeBoss1')
EnemyShot         = require('sprite/level/EnemyShot')
PlayerMissile     = require('sprite/level/PlayerMissile')
Vector            = require('math/Vector')
CollisionDetector = require('util/collision/CollisionDetector')
StageBannerEffect = require('sprite/level/StageBannerEffect')
BaseLevelScreen   = require('screen/levels/BaseLevelScreen')
DialogueManager   = require('sprite/level/DialogueManager')

import Campaign, Audio from require('Services')
import
    IMG_LEVEL1_BG
    IMG_LEVEL1_PLANETS
    MUS_LEVEL1_BG
    MUS_LEVEL1_BOSS
    SFX_PLAYERSHOT
    FNT_HEADING
    from require('Resources')


class Stage1Screen extends BaseLevelScreen
    new: (game) =>
        super(game)

        @loveregenpertime = 175 --reset in prestage dialogue

        @coroutines = {}

        @controltime = 0 --used specifically for stage control
        @enemyspawncontrol = -1 --used for controlling when things happen in stage. -1 is pre-stage dialogue
        @drawbanneralpha = 0
        @onpause = false
        @prestagedialogue()

    reset: () =>
        @game\addScreen('stage1', Stage1Screen(@game))

    restart: () =>
        super()
        @changeScreen('stage1')

    controlStage: (dt) =>
        if @enemyspawncontrol == 0 and @controltime > 1
            for i = -2, 2 do
                newenemy = EnemyType1(Vector(832, 300+i*100), Vector(-100 + 25*math.abs(i), 0), self, 4)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1

            @enemyspawncontrol = 1

        if @enemyspawncontrol == 1 and @controltime > 4
            @enemyspawncontrol = 2

        if @enemyspawncontrol == 2
            newenemy2 = EnemyType2(Vector(832, 300), Vector(-25, 0), self, 8)
            newenemy2\load(@resources)
            @addSprite(newenemy2)
            @enemyCount += 1
            @enemyspawncontrol = 3

        if @enemyspawncontrol == 3 and @controltime > 10
            @drawBanner()
            @enemyspawncontrol = 3.25
            @onpause = true

        if @enemyspawncontrol == 3.25 and @controltime > 12
            d1 = DialogueManager(0, 105, self, 4)
            @addSprite(d1)
            @enemyspawncontrol = 3.5

        if @enemyspawncontrol == 3.5 and @controltime > 11 and @onpause == false
            @enemyspawncontrol = 4

        if @enemyspawncontrol == 4
            newenemy3 = EnemyType3(Vector(832, 150), Vector(-25, 0), self, 9)
            newenemy3\load(@resources)
            @addSprite(newenemy3)
            @enemyCount += 1
            newenemy3 = EnemyType3(Vector(832, 450), Vector(-25, 0), self, 9)
            newenemy3\load(@resources)
            @addSprite(newenemy3)
            @enemyCount += 1
            @enemyspawncontrol = 5

        if @enemyspawncontrol == 5 and @controltime > 16
            @enemyspawncontrol = 6

        if @enemyspawncontrol == 6
            newenemy4 = EnemyType4(Vector(800, 300), Vector(-50, 0), self, 24)
            newenemy4\load(@resources)
            @addSprite(newenemy4)
            @enemyCount += 1
            @enemyspawncontrol = 7

        if @enemyspawncontrol == 7 and @controltime > 22
            @enemyspawncontrol = 8

        if @enemyspawncontrol == 8
            for i = -1, 1 do
                newenemy = EnemyType1(Vector(832, 300), Vector(-100 + 25 * math.abs(i), 30 * i), self, 4)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1

            @enemyspawncontrol = 9

        if @enemyspawncontrol == 9 and @controltime > 25
            @enemyspawncontrol = 10

        if @enemyspawncontrol == 10
            newenemy3 = EnemyType3(Vector(832, 150), Vector(-25, 0), self, 9)
            newenemy3\load(@resources)
            @addSprite(newenemy3)
            @enemyCount += 1
            newenemy3 = EnemyType3(Vector(832, 450), Vector(-25, 0), self, 9)
            newenemy3\load(@resources)
            @addSprite(newenemy3)
            @enemyCount += 1
            @enemyspawncontrol = 11

        if @enemyspawncontrol == 11 and @controltime > 27
            @enemyspawncontrol = 12

        if @enemyspawncontrol == 12
            newenemy4 = EnemyType4(Vector(800, 300), Vector(-50, 0), self, 24)
            newenemy4\load(@resources)
            @addSprite(newenemy4)
            @enemyCount += 1
            @enemyspawncontrol = 13

        if @enemyspawncontrol == 13 and @controltime > 43 and @enemyCount <= 0
            --Provision for Pre-Boss Dialogue
            @enemyspawncontrol = 100 --temporary. Should be set in the dialogue.

        if @enemyspawncontrol == 100 --Boss Appears
            Audio\stop(MUS_LEVEL1_BG)
            Audio\play(MUS_LEVEL1_BOSS)

            --Provision for Boss
            boss1 = EnemyTypeBoss1(Vector(1200, 300), Vector(-25, 0), self, 300)
            boss1\load(@resources)
            @addSprite(boss1)
            @boss = boss1
            @enemyCount += 1

            @shotenabled = false

            @enemyspawncontrol = 150

        if @enemyspawncontrol == 150 and @controltime > 44 -- Dialogue with Boss
            @prebossdialogue()

        if @enemyspawncontrol == 199 and @controltime > 45 -- End Dialogue
            @boss.dialoguestatus = 1 --enable boss firing and moving
            @shotenabled = true
            @enemyspawncontrol = 200

        if @enemyspawncontrol == 200 and @enemyCount <= 0
            @enemyspawncontrol = 250
            @controltime = 45
            @killBullets()
            d1 = DialogueManager(0, 109, self)
            @addSprite(d1)

        if @enemyspawncontrol == 250 and @enemyCount <= 0 and @controltime > 48
            @enemyspawncontrol = 251
            Campaign\saveProgress(1)
            @game.screens['stage2'].score += @score
            @changeScreen('cutscene2')
            @reset()

    prestagedialogue: () =>
        dialogue_co = coroutine.create(() ->
            Audio\stop(MUS_LEVEL1_BG)
            @loveregenpertime = 0
            d1 = DialogueManager(0, 101, self)
            @addSprite(d1)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d2 = DialogueManager(0, 102, self)
            @addSprite(d2)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d3 = DialogueManager(0, 103, self)
            @addSprite(d3)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d4 = DialogueManager(0, 104, self)
            @addSprite(d4)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            @enemyspawncontrol = 0
            Audio\play(MUS_LEVEL1_BG)
            @loveregenpertime = 175
        )
        table.insert(@coroutines, dialogue_co)

    prebossdialogue: () =>
        dialogue_co = coroutine.create(() ->
            @enemyspawncontrol = 151
            d1 = DialogueManager(0, 106, self)
            @addSprite(d1)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d2 = DialogueManager(1, 107, self)
            @addSprite(d2)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d3 = DialogueManager(0, 108, self)
            @addSprite(d3)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()

            @enemyspawncontrol = 199
        )
        table.insert(@coroutines, dialogue_co)

    drawBanner: () =>
        for i = -12, 12 do
            for j = -4, 4 do
                if math.abs(i) > 8 or math.abs(j) > 2
                    s = StageBannerEffect(Vector(math.random(1000)-100, math.random(800)-100), Vector(400 + i*16, 200 + j*16), 150, 150, 255, self)
                    s\load(@resources)
                    @addSprite(s, false)

        @drawbanneralpha = 1

    willDisplay: () =>
        super()

        --Audio\play(MUS_LEVEL1_BG)

    willHide: () =>
        super()

        Audio\stop(MUS_LEVEL1_BG)
        Audio\stop(MUS_LEVEL1_BOSS)

    _togglePauseMenu: () =>
        super()

        if @pauseMenu
            if Audio\isPlaying(MUS_LEVEL1_BG)
                Audio\pause(MUS_LEVEL1_BG)
            if Audio\isPlaying(MUS_LEVEL1_BOSS)
                Audio\pause(MUS_LEVEL1_BOSS)
        else
            if Audio\isPaused(MUS_LEVEL1_BG)
                Audio\resume(MUS_LEVEL1_BG)
            if Audio\isPaused(MUS_LEVEL1_BOSS)
                Audio\resume(MUS_LEVEL1_BOSS)

    update: (dt) =>
        if love.keyboard.isDown(';')
            dt *= 10

        for i, cr in ipairs(@coroutines)
            coroutine.resume(cr, dt) if coroutine.status(cr) != "dead"

        if love.keyboard.isDown('l') and @time > 0.5 --next stage
                @enemyspawncontrol = 250
                @enemyCount = -80 --manual override
                @controltime = 5000

        super(dt)

        if not @pauseMenu and @enemyspawncontrol >= 0
            @controltime += dt

            if @drawbanneralpha > 0 and @drawbanneralpha < 255
                @drawbanneralpha += (255 / 5) * dt
            else
                @drawbanneralpha = 0
                @onpause = false

    draw: () =>
        -- Background
        if @enemyspawncontrol >= 0
            scrollableWidth = IMG_LEVEL1_BG\getWidth() - 800
            scrollAmount = math.min(@controltime / 43, 1)
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(IMG_LEVEL1_BG, -math.floor(scrollAmount * scrollableWidth))

            scrollableWidth = IMG_LEVEL1_PLANETS\getWidth() - 800
            love.graphics.draw(IMG_LEVEL1_PLANETS, -math.floor(scrollAmount * scrollableWidth))
        else
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(IMG_LEVEL1_BG, 0)
            love.graphics.draw(IMG_LEVEL1_PLANETS, 0)

        if @drawbanneralpha > 0 and @drawbanneralpha < 255
            love.graphics.origin()
            love.graphics.setColor(255, 150, 150, @drawbanneralpha)
            love.graphics.setFont(FNT_HEADING)
            text = 'Chapter 1'
            width, height = FNT_HEADING\getWidth(text), FNT_HEADING\getHeight(text)
            love.graphics.print(text, 400 - width/2, 200 - height/2)

        -- Draw rest of sprites
        super()
