-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseScreen        = require('screen/BaseScreen')
Hero              = require('sprite/level/Hero')
Enemy             = require('sprite/level/Enemy')
BossEnemy         = require('sprite/level/BossEnemy')
EnemyTypeBoss4    = require('sprite/level/EnemyTypeBoss4')
EnemyShot         = require('sprite/level/EnemyShot')
EnemyLaser        = require('sprite/level/EnemyLaser')
PlayerMissile     = require('sprite/level/PlayerMissile')
Vector            = require('math/Vector')
CollisionDetector = require('util/collision/CollisionDetector')
ScoreText         = require('sprite/level/ScoreText')
StageHeart        = require('sprite/level/StageHeart')
BulletDelete      = require('sprite/level/BulletDelete')
PauseMenu         = require('sprite/level/PauseMenu')
DialogueManager   = require('sprite/level/DialogueManager')

import Input, Audio from require('Services')
import
    SFX_ENEMYHIT
    SFX_ENEMYHIT2
    SFX_ENEMYHIT3
    SFX_ENEMYHIT4
    SFX_PLAYERSHOT
    SFX_SHIELD
    FNT_HEADING
    FNT_DEBUG
    FNT_BODY
    from require('Resources')


class BaseLevelScreen extends BaseScreen
    new: (game) =>
        super(game)

        @collisions = CollisionDetector()
        @showDebug = false

        -- Debouncing keys
        @pDown = false
        @escDown = false
        @spaceDown = false

        @pauseMenu = false
        @pauseMenuSprite = PauseMenu(self)

        @next_screen = nil

        @time = 0

        @rapidFireTimer = 0

        @invincibilitymode = false
        @shottype = 1

        @boss = nil

        -- Score Text Display
        @scoretext = ScoreText()
        self\addSprite(@scoretext)

        @score = 0
        @maxlove = 10000
        @love = @maxlove/2 --defaults to half of maximum
        @lovepershot = 75 --how much LOVE to deduct per shot wave fired
        @loveregainedperhit = 10 --how much LOVE to regain after successfully hitting the enemy with one of the fired shots
        @loveregainedperdefeat = 250 --how much LOVE to regain after successfully LOVEing an enemy
        @loveregenpertime = 150 --Override in individual stages
        @loveregainedpershot = 50 --shield

        @maxinvincibilitytime = 2 --buffer after hit until death counted
        @invincibilitytime = 0

        @shotenabled = true

        self\setup()
        @enemyCount = 0

    --Creates hero and handles collision detection and scoring
    setup: () =>
        @hero = Hero(self)
        @hero.pos.y = 300 - 32
        self\addSprite(@hero)

        @enemyhitsepool = {SFX_ENEMYHIT, SFX_ENEMYHIT2, SFX_ENEMYHIT3, SFX_ENEMYHIT4}

        @collisions\onCollision((event) ->
            if event\between(Hero, EnemyShot) and not @invincibilitymode and @invincibilitytime == 0
                if @hero.blanket == false
                    @love -= @maxlove/4
                    @invincibilitytime = @maxinvincibilitytime
                    if @love < 0
                        event\get(Hero).exploding = true
                else
                    @love += @loveregainedpershot
                @removeSprite(event\get(EnemyShot))
            elseif event\between(Hero, EnemyLaser) and not @invincibilitymode and @invincibilitytime == 0
                if @hero.blanket == false
                    @love -= @maxlove/4
                    @invincibilitytime = @maxinvincibilitytime
                    if @love < 0
                        event\get(Hero).exploding = true
                else
                    @love += (@loveregainedpershot / 4)
            elseif event\between(Enemy, PlayerMissile)
                enemy = event\get(Enemy)

                if not enemy.exploding
                    if not enemy.blanket
                        @score += 10
                        @love += @loveregainedperhit
                        @enemyhitsepool[math.random(1,4)]\play()
                        enemy.hp -= 1
                    else 
                        Audio\play(SFX_SHIELD)

                    if enemy.hp <= 0
                        @love += @loveregainedperdefeat
                        enemy.exploding = true
                        @enemyCount -= 1
                        @score += 50

                        for i = 0, 5
                            particle = StageHeart(enemy.pos + enemy.center + Vector(math.random(-24,24), math.random(-24,24)), self)
                            particle\load(@resources)
                            @addSprite(particle)

                        if is_a(enemy, BossEnemy) --additional explosion
                            for i = 0, 70
                                particle = StageHeart(enemy.pos + enemy.center + Vector(math.random(-72,72), math.random(-72,72)), self)
                                particle\load(@resources)
                                @addSprite(particle)
                            @score += 250

                    if not enemy.blanket --disable hearts and boss lifebar particles
                        randangle = math.random(math.pi*2)
                        particle = StageHeart(enemy.pos + enemy.center + Vector(math.random(-6,6), math.random(-6,6)), self, 16*Vector(math.cos(randangle), math.sin(randangle)))

                        if is_a(enemy, BossEnemy)
                            particle = StageHeart(enemy.pos + enemy.center + Vector(math.random(-72,72), math.random(-72,72)), self, 16*Vector(math.cos(randangle), math.sin(randangle)))
                            enemy\addLifebarParticle(self)

                        particle\load(@resources)
                        @addSprite(particle)

                    @removeSprite(event\get(PlayerMissile))
            elseif event\between(Enemy, Hero) and not @invincibilitymode and @invincibilitytime == 0
                @love -= @maxlove/4
                @invincibilitytime = @maxinvincibilitytime
                if @love < 0
                    event\get(Hero).exploding = true
        )

    reset: () =>
        @game\addScreen('default', DefaultScreen(@game))

    restart: () =>
        @reset()

    controlStage: (dt) =>
        -- Stub

    addSprite: (sprite, collides=true) =>
        super(sprite)
        @collisions\add(sprite) if collides

    removeSprite: (sprite, collides=true) =>
        super(sprite)
        @collisions\remove(sprite) if collides

    bulletDeleteEffect: (spawnpos) =>
        particle = BulletDelete(spawnpos - Vector(8, 8), self)
        particle\load(@resources)
        @addSprite(particle)

    --Remove all bullets on screen
    killBullets: () =>
        -- Gtaher sprites to delete, since we can't delete while iterating over the set
        bullets = [s for s, _ in pairs @sprites when is_a(s, EnemyShot)]

        for bullet in *bullets
            @removeSprite(bullet)
            @bulletDeleteEffect(bullet.pos)

    update: (dt) =>
        -- Toggle debug screen
        if love.keyboard.isDown('p')
            if not @pDown
                @showDebug = not @showDebug
                @pDown = true
        else
            @pDown = false

        -- Toggle pause menu
        if Input\isDown(Input.NAV_PAUSE)
            if not @escDown
                @escDown = true
                @_togglePauseMenu()
        else
            @escDown = false

        if love.keyboard.isDown('i')
            @invincibilitymode = true

        if love.keyboard.isDown('u')
            @invincibilitymode = false

        if not @pauseMenu
            -- Fire bullets
            if not @hero.exploding and Input\isDown(Input.FIRE) and @love > @lovepershot and @shotenabled
                -- Fire bullet upon pressing spacebar
                if not @spaceDown
                    @rapidFireTimer = 0
                    Audio\play(SFX_PLAYERSHOT)

                    --@hero.pos is top left of player graphics.
                    playercenterpos = @hero.pos + @hero.center

                    if @shottype == 1
                        bullet = PlayerMissile(playercenterpos + Vector(-8, 6), Vector(500, 0), 0, self)
                        bullet\load(@resources)
                        @addSprite(bullet)

                        bullet = PlayerMissile(playercenterpos + Vector(-8, -18), Vector(500, 0), 0, self)
                        bullet\load(@resources)
                        @addSprite(bullet)
                    if @shottype == 2
                        bullet = PlayerMissile(playercenterpos + Vector(-8, 6), Vector(600, 90*math.cos(@time*2)^2), 0, self)
                        bullet\load(@resources)
                        @addSprite(bullet)

                        bullet = PlayerMissile(playercenterpos + Vector(-8, -6), Vector(500, 10), 0, self)
                        bullet\load(@resources)
                        @addSprite(bullet)

                        bullet = PlayerMissile(playercenterpos + Vector(-8, -6), Vector(500, -10), 0, self)
                        bullet\load(@resources)
                        @addSprite(bullet)

                        bullet = PlayerMissile(playercenterpos + Vector(-8, -18), Vector(600, -90*math.cos(@time*2)^2), 0, self)
                        bullet\load(@resources)
                        @addSprite(bullet)

                    -- deduct used LOVE
                    @love -= @lovepershot

                -- Hold for rapid fire
                @spaceDown = true
                @rapidFireTimer += dt
            else
                @spaceDown = false
                @rapidFireTimer = 0

            -- Rapid fire of bullets
            if @spaceDown and (@rapidFireTimer > .1) and @love > @lovepershot and @shotenabled
                @rapidFireTimer = 0
                Audio\play(SFX_PLAYERSHOT)

                --@hero.pos is top left of player graphics.
                playercenterpos = @hero.pos + @hero.center

                if @shottype == 1
                    bullet = PlayerMissile(playercenterpos + Vector(-8, 6), Vector(500, 0), 0, self)
                    bullet\load(@resources)
                    @addSprite(bullet)

                    bullet = PlayerMissile(playercenterpos + Vector(-8, -18), Vector(500, 0), 0, self)
                    bullet\load(@resources)
                    @addSprite(bullet)
                if @shottype == 2
                    bullet = PlayerMissile(playercenterpos + Vector(-8, 6), Vector(600, 90*math.cos(@time*2)^2), 0, self)
                    bullet\load(@resources)
                    @addSprite(bullet)

                    bullet = PlayerMissile(playercenterpos + Vector(-8, -6), Vector(500, 10), 0, self)
                    bullet\load(@resources)
                    @addSprite(bullet)

                    bullet = PlayerMissile(playercenterpos + Vector(-8, -6), Vector(500, -10), 0, self)
                    bullet\load(@resources)
                    @addSprite(bullet)

                    bullet = PlayerMissile(playercenterpos + Vector(-8, -18), Vector(600, -90*math.cos(@time*2)^2), 0, self)
                    bullet\load(@resources)
                    @addSprite(bullet)

                -- deduct used LOVE
                @love -= @lovepershot

            super(dt)

            @collisions\detect()

            @scoretext\setText("Score: " .. @score)

            self\controlStage(dt)

            @time += dt

            -- LOVE regeneration over time
            if not @hero.exploding
                @love += dt * @loveregenpertime

            if @love > @maxlove
                @love = @maxlove
            if @love < 0
                @love = 0

            if @invincibilitytime > 0 and not @hero.exploding
                @invincibilitytime -= dt
            if @invincibilitytime < 0
                @invincibilitytime = 0
        else
            @pauseMenuSprite\update(dt)

    _togglePauseMenu: () =>
        @pauseMenu = not @pauseMenu

    _pauseContinue: () =>
        @_togglePauseMenu()

    _pauseRestart: () =>
        @restart()

    _pauseExit: () =>
        @changeScreen('title')
        @reset()

    --Background drawing handled in individual stages
    draw: () =>
        -- Draw rest of sprites
        super()

        --START LOVEBAR
        love.graphics.origin()
        lifebarlength = 192
        love.graphics.setColor(255, 192, 192, 192)
        if @invincibilitytime * 1000 % 200 < 100 and @invincibilitytime > 0 and not @hero.exploding
            love.graphics.setColor(0, 0, 0, 0)
        love.graphics.rectangle('line', 400 - 192/2, 16, lifebarlength, 16) --border

        love.graphics.setColor(255, 150, 150, 255)
        if @love/@maxlove < 0.25
            love.graphics.setColor(255, 0, 0, 255)
        if @invincibilitytime * 1000 % 200 < 100 and @invincibilitytime > 0 and not @hero.exploding
            love.graphics.setColor(255, 255, 255, 255)
        lifebarlength = 188 * @love/@maxlove
        love.graphics.rectangle('fill', 400 - 192/2 + 2, 18, lifebarlength, 12) --main

        love.graphics.setColor(255, 255, 255, 255)
        --END LOVEBAR

        love.graphics.setColor(215, 203, 198, 255)
        love.graphics.setFont(FNT_BODY)
        love.graphics.print("LOVE:", 212, 9)

        -- FPS Debug
        if @showDebug then
            invinstring = if @invincibilitymode then "true" else "false"

            love.graphics.origin()
            love.graphics.setColor(0, 0, 0, 127)
            love.graphics.rectangle('fill', 0, 0, 184, 64)

            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.setFont(FNT_DEBUG)
            love.graphics.print('FPS: ' .. love.timer.getFPS() .. '\n' .. 'Enemy Count: ' .. @enemyCount .. '\nInvincibility: ' .. invinstring, 5, 5)

        if @pauseMenu
            @pauseMenuSprite\draw()
