-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Hero                 = require('sprite/level/Hero')
Enemy                = require('sprite/level/Enemy')
EnemyType3           = require('sprite/level/EnemyType3')
EnemyType5           = require('sprite/level/EnemyType5')
EnemyType6           = require('sprite/level/EnemyType6')
EnemyType7           = require('sprite/level/EnemyType7')
EnemyTypeBoss2       = require('sprite/level/EnemyTypeBoss2')
EnemyShot            = require('sprite/level/EnemyShot')
PlayerMissile        = require('sprite/level/PlayerMissile')
Vector               = require('math/Vector')
CollisionDetector    = require('util/collision/CollisionDetector')
StageBannerEffect    = require('sprite/level/StageBannerEffect')
BaseLevelScreen      = require('screen/levels/BaseLevelScreen')
ChargeCompleteEffect = require('sprite/level/ChargeCompleteEffect')
BarFillEffect        = require('sprite/level/BarFillEffect')
DialogueManager      = require('sprite/level/DialogueManager')

import Input, Campaign, Audio from require('Services')
import
    IMG_LEVEL2_BG
    MUS_LEVEL2_BG
    MUS_LEVEL2_BOSS
    FNT_HEADING
    SFX_PLAYERSHOT
    SFX_POWERUP
    SFX_ASTRALUP
    from require('Resources')


class Stage2Screen extends BaseLevelScreen
    new: (game) =>
        super(game)

        @loveregenpertime = 150  --reset in prestage dialogue

        @coroutines = {}

        @controltime = 0 --used specifically for stage control
        @enemyspawncontrol = -1 --used for controlling when things happen in stage. -1 is pre-stage dialogue
        @drawbanneralpha = 0
        @onpause = false

        @blanketcooldown = 0
        @blanketactive = false

        @prestagedialogue()

        --jump to boss
        --@enemyspawncontrol = 100
        --@controltime = 64

    reset: () =>
        @game\addScreen('stage2', Stage2Screen(@game))

    restart: () =>
        super()
        @changeScreen('stage2')

    controlStage: (dt) =>
        if @enemyspawncontrol == 0 and @controltime > 1
            for i = 0, 5 do
                newenemy = EnemyType5(Vector(920, 300), Vector(-100, 0), self, 4, i)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            @enemyspawncontrol = 1

        if @enemyspawncontrol == 1 and @controltime > 5
            for i = -2, 2 do
                newenemy = EnemyType3(Vector(820, 300 + 100*i), Vector(-100, 0), self, 6)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            @enemyspawncontrol = 2

        if @enemyspawncontrol == 2 and @controltime > 12
            --Provision for stage banner and potential dialogue event
            @drawBanner()
            @enemyspawncontrol = 2.25
            @onpause = true

        if @enemyspawncontrol == 2.25 and @controltime > 14
            d1 = DialogueManager(0, 203, self, 4)
            @addSprite(d1)
            @enemyspawncontrol = 2.5

        if @enemyspawncontrol == 2.5 and @controltime > 13 and @onpause == false
            @enemyspawncontrol = 3

        if @enemyspawncontrol == 3 and @controltime > 13 and @onpause == false
            newenemy6 = EnemyType6(Vector(820, 620), Vector(-50, -50), self, 12)
            newenemy6\load(@resources)
            @addSprite(newenemy6)
            @enemyCount += 1
            @enemyspawncontrol = 4

        if @enemyspawncontrol == 4 and @controltime > 16 and @onpause == false
            for i = 0, 5 do
                newenemy = EnemyType5(Vector(920, 150), Vector(-100, 0), self, 4, i)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            @enemyspawncontrol = 5

        if @enemyspawncontrol == 5 and @controltime > 25 and @onpause == false
            newenemy6 = EnemyType6(Vector(820, -20), Vector(-50, 50), self, 12)
            newenemy6\load(@resources)
            @addSprite(newenemy6)
            @enemyCount += 1
            @enemyspawncontrol = 6

        if @enemyspawncontrol == 6 and @controltime > 28 and @onpause == false
            for i = 0, 5 do
                newenemy = EnemyType5(Vector(920, 450), Vector(-100, 0), self, 4, i)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            @enemyspawncontrol = 7

        if @enemyspawncontrol == 7 and @controltime > 36 and @onpause == false
            for i = -1, 2 do
                newenemy = EnemyType7(Vector(820, 300 + 100*i - 50), Vector(-1, 0), self, 3, 1, 0.5)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            @enemyspawncontrol = 8

        if @enemyspawncontrol == 8 and @controltime > 38 and @onpause == false
            for i = -1, 1 do
                newenemy = EnemyType7(Vector(820, 300 + 200*i), Vector(-1, 0), self, 3, -1, 0.5)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            @enemyspawncontrol = 9

        if @enemyspawncontrol == 9 and @controltime > 42 and @onpause == false
            for i = 0, 5 do
                newenemy = EnemyType5(Vector(920, 150), Vector(-100, 0), self, 4, i)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            for i = 0, 5 do
                newenemy = EnemyType5(Vector(920, 450), Vector(-100, 0), self, 4, i)
                newenemy\load(@resources)
                @addSprite(newenemy)
                @enemyCount += 1
            @enemyspawncontrol = 10

        if @enemyspawncontrol == 10 and @controltime > 47 and @onpause == false
            newenemy6 = EnemyType6(Vector(820, 620), Vector(-50, -50), self, 12)
            newenemy6\load(@resources)
            @addSprite(newenemy6)
            @enemyCount += 1
            newenemy6 = EnemyType6(Vector(820, -20), Vector(-50, 50), self, 12)
            newenemy6\load(@resources)
            @addSprite(newenemy6)
            @enemyCount += 1
            @enemyspawncontrol = 11

        if @enemyspawncontrol == 11 and @controltime > 51 and @onpause == false
            newenemy = EnemyType7(Vector(820, 150), Vector(-1, 0), self, 3, -1, 0.5)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            newenemy = EnemyType7(Vector(820, 450), Vector(-1, 0), self, 3, 1, 0.5)
            newenemy\load(@resources)
            @addSprite(newenemy)
            @enemyCount += 1
            @enemyspawncontrol = 12

        if @enemyspawncontrol == 12 and @controltime > 64
            --Provision for Pre-Boss Dialogue
            @enemyspawncontrol = 100 --temporary. Should be set in the dialogue.

        if @enemyspawncontrol == 100 and @enemyCount <= 0
            Audio\stop(MUS_LEVEL2_BG)
            Audio\play(MUS_LEVEL2_BOSS)

            --Provision for Boss
            boss2 = EnemyTypeBoss2(Vector(1200, 300), Vector(-25, 0), self, 300)
            boss2\load(@resources)
            @addSprite(boss2)
            @boss = boss2
            @enemyCount += 1
            
            @shotenabled = false

            @enemyspawncontrol = 150

        if @enemyspawncontrol == 150 and @controltime > 65 -- Dialogue with Boss
            @prebossdialogue()

        if @enemyspawncontrol == 199 and @controltime > 66 -- End Dialogue
            @boss.dialoguestatus = 1 --enable boss firing and moving
            @shotenabled = true
            @enemyspawncontrol = 200

        if @enemyspawncontrol == 200 and @enemyCount <= 0
            --Provision for Post-Boss Dialogue
            @enemyspawncontrol = 250 --temporary. Should be set in the dialogue.
            @controltime = 66
            @killBullets()
            d1 = DialogueManager(0, 206, self)
            @addSprite(d1)

        if @enemyspawncontrol == 250 and @enemyCount <= 0 and @controltime > 69
            @enemyspawncontrol = 251
            Campaign\saveProgress(2)
            @game.screens['stage3'].score += @score
            @changeScreen('cutscene3')
            @reset()

    prestagedialogue: () =>
        dialogue_co = coroutine.create(() ->
            Audio\stop(MUS_LEVEL2_BG)
            @loveregenpertime = 0
            d1 = DialogueManager(0, 201, self)
            @addSprite(d1)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d2 = DialogueManager(0, 202, self)
            @addSprite(d2)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            @enemyspawncontrol = 0
            Audio\play(MUS_LEVEL2_BG)
            @loveregenpertime = 150
        )
        table.insert(@coroutines, dialogue_co)

    prebossdialogue: () =>
        dialogue_co = coroutine.create(() ->
            @enemyspawncontrol = 151
            d1 = DialogueManager(0, 204, self)
            @addSprite(d1)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()
            d2 = DialogueManager(1, 205, self)
            @addSprite(d2)
            timeinit = @time
            while @time < timeinit + 2
                coroutine.yield()

            @enemyspawncontrol = 199
        )
        table.insert(@coroutines, dialogue_co)

    drawBanner: () =>
        for i = -12, 12 do
            for j = -4, 4 do
                if math.abs(i) > 8 or math.abs(j) > 2
                    s = StageBannerEffect(Vector(math.random(1000)-100, math.random(800)-100), Vector(400 + i*16, 200 + j*16), 255, 96, 64, self)
                    s\load(@resources)
                    @addSprite(s, false)

        @drawbanneralpha = 1

    drawChargeCompleteEffect: () =>
        for i = 0, 35 do
            eff = ChargeCompleteEffect(Vector(136-4, 576-4), Vector(15*math.cos(i*(math.pi*2/36)), 15*math.sin(i*(math.pi*2/36))), 0, 192, 255, self)
            eff\load(@resources)
            @addSprite(eff, false)

    addBarFillEffect: () =>
        eff = BarFillEffect(Vector(22 + 124 - @blanketcooldown * 124 / 15, 576), 0, 192, 255, self)
        eff\load(@resources)
        @addSprite(eff, false)

    willDisplay: () =>
        super()

        --Audio\play(MUS_LEVEL2_BG)

    willHide: () =>
        super()

        Audio\stop(MUS_LEVEL2_BG)
        Audio\stop(MUS_LEVEL2_BOSS)

    _togglePauseMenu: () =>
        super()

        if @pauseMenu
            if Audio\isPlaying(MUS_LEVEL2_BG)
                Audio\pause(MUS_LEVEL2_BG)
            if Audio\isPlaying(MUS_LEVEL2_BOSS)
                Audio\pause(MUS_LEVEL2_BOSS)
        else
            if Audio\isPaused(MUS_LEVEL2_BG)
                Audio\resume(MUS_LEVEL2_BG)
            if Audio\isPaused(MUS_LEVEL2_BOSS)
                Audio\resume(MUS_LEVEL2_BOSS)

    update: (dt) =>
        if love.keyboard.isDown(';')
            dt *= 10

        for i, cr in ipairs(@coroutines)
                coroutine.resume(cr, dt) if coroutine.status(cr) != "dead"

        if love.keyboard.isDown('l') and @time > 0.5 --next stage
                @enemyspawncontrol = 250
                @enemyCount = -80 --manual override
                @controltime = 5000

        super(dt)

        if not @pauseMenu and @enemyspawncontrol >= 0
            @controltime += dt

            if @drawbanneralpha > 0 and @drawbanneralpha < 255
                @drawbanneralpha += (255 / 5) * dt
            else
                @drawbanneralpha = 0
                @onpause = false

            --Power Ups
            if Input\isDown(Input.POWERUP) and @blanketcooldown <= 0 and not(@hero.exploding) --blanket
                @hero.blanket = true
                @hero.blanketlength = 3
                @blanketcooldown = 18
                Audio\play(SFX_POWERUP)
                @blanketactive = true
            else
                @blanketcooldown -= dt

            if @blanketcooldown > 0 and @hero.blanketlength <= 0
                @addBarFillEffect()

            if @blanketcooldown <= 0 --correction for cooldown bar just in case
                @blanketcooldown = 0
                if @blanketactive
                    @blanketactive = false
                    Audio\play(SFX_ASTRALUP)
                    @drawChargeCompleteEffect()

    draw: () =>
        -- Background
        if @enemyspawncontrol >= 0
            scrollableWidth = IMG_LEVEL2_BG\getWidth() - 800
            scrollAmount = math.min(@controltime / 64, 1)
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(IMG_LEVEL2_BG, -math.floor(scrollAmount * scrollableWidth))
        else
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.draw(IMG_LEVEL2_BG, 0)

        --scrollableWidth = @planets\getWidth() - 800
        --love.graphics.draw(@planets, -math.floor(scrollAmount * scrollableWidth))

        --START Cooldown Bars
        love.graphics.origin()
        blanketrectlength = 128
        love.graphics.setColor(0, 128, 255, 255)
        love.graphics.rectangle('line', 16, 568, blanketrectlength, 16) --border

        love.graphics.setColor(0, 192, 255, 255)
        if @hero.blanketlength > 0 --currently using blanket
            blanketrectlength = @hero.blanketlength*124/3
            love.graphics.setColor(0, 192, 255, 255)
        elseif @blanketcooldown > 0 --currently on cooldown
            blanketrectlength = 124 - @blanketcooldown * 124 / 15
            if blanketrectlength < 0 --To prevent weird and accidental graphical glitches
                blanketrectlength = 0
            love.graphics.setColor(0, 192-@blanketcooldown*8, 255-@blanketcooldown*8, 255)
        else
            blanketrectlength = 124

        love.graphics.rectangle('fill', 18, 570, blanketrectlength, 12)

        --END Cooldown Bars

        if @drawbanneralpha > 0 and @drawbanneralpha < 255
            love.graphics.origin()
            love.graphics.setColor(255, 150, 150, @drawbanneralpha)
            love.graphics.setFont(FNT_HEADING)
            text = 'Chapter 2'
            width, height = FNT_HEADING\getWidth(text), FNT_HEADING\getHeight(text)
            love.graphics.print(text, 400 - width/2, 200 - height/2)

        -- Draw rest of sprites
        super()
