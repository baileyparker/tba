-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseMenuScreen = require('screen/menu/BaseMenuScreen')
NewMenuButton  = require('sprite/menu/NewMenuButton')
Vector         = require('math/Vector')
import IMG_NEW_MENU_BG from require('Resources')


class TitleScreen extends BaseMenuScreen
    new: (game) =>
        super(game)

        @background = IMG_NEW_MENU_BG

        @playButton = NewMenuButton(NewMenuButton.ACTION_PLAY, self)
        @optionsButton = NewMenuButton(NewMenuButton.ACTION_OPTIONS, self)
        @quitButton = NewMenuButton(NewMenuButton.ACTION_QUIT, self)

        @playButton\onClick(() -> @changeScreen('campaign_select'))
        @optionsButton\onClick(() -> @changeScreen('settings'))
        @quitButton\onClick(() -> love.event.push('quit'))

        @addSprite(@playButton)
        @addSprite(@optionsButton)
        @addSprite(@quitButton)

        table.insert(@components, @playButton)
        table.insert(@components, @optionsButton)
        table.insert(@components, @quitButton)

        @transitioning = false

    load: () =>
        super(false)

    willDisplay: (oldScreen) =>
        super(oldScreen)
        @transitioning = true

    displayed: () =>
        super()
        @transitioning = false

    update: (dt) =>
        if not @transitioning
            super(dt)
