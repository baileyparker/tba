-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseMenuScreen = require('screen/menu/BaseMenuScreen')
Slider         = require('sprite/menu/Slider')
Vector         = require('math/Vector')

import Input, Settings from require('Services')
import
    IMG_SETTINGS_BG
    from require('Resources')


class SettingsScreen extends BaseMenuScreen
    new: (game) =>
        super(game)

        @background = IMG_SETTINGS_BG

        @brightness   = Slider(Vector(222, 254), Settings.brightness)
        @contrast     = Slider(Vector(523, 254), Settings.contrast)
        @masterVolume = Slider(Vector(372, 395), Settings.masterVolume)
        @musicVolume  = Slider(Vector(222, 484), Settings.musicVolume)
        @sfxVolume    = Slider(Vector(523, 484), Settings.sfxVolume)

        @brightness\onChange((brightness) -> Settings\setBrightness(brightness))
        @contrast\onChange((contrast) -> Settings\setContrast(contrast))
        @masterVolume\onChange((volume) -> Settings\setMasterVolume(volume))
        @musicVolume\onChange((volume) -> Settings\setMusicVolume(volume))
        @sfxVolume\onChange((volume) -> Settings\setSfxVolume(volume))

        @addSprite(@brightness)
        @addSprite(@contrast)
        @addSprite(@masterVolume)
        @addSprite(@musicVolume)
        @addSprite(@sfxVolume)

        @transitioning = false
        @backDown = false

    load: () =>
        super(false)

    willDisplay: (oldScreen) =>
        super(oldScreen)
        @transitioning = true

    displayed: () =>
        super()
        @transitioning = false

    willHide: (newScreen) =>
        super(newScreen)
        Settings\save()

    update: (dt) =>
        if not @transitioning
            super(dt)

            if Input\isDown(Input.NAV_BACK)
                if not @backDown
                    @backDown = true
                    @changeScreen('title')
            else
                @backDown = false
