-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseMenuScreen  = require('screen/menu/BaseMenuScreen')
HighScoresTitle = require('sprite/menu/HighScoresTitle')
HighScoresText  = require('sprite/menu/HighScoresText')
MenuButton      = require('sprite/menu/MenuButton')
Vector          = require('math/Vector')
import IMG_MENU_BTN from require('Resources')


class YouLoseScreen extends BaseMenuScreen
    new: (game) =>
        super(game)

        --@title = HighScoresTitle()
        @text       = HighScoresText()
        @backButton = MenuButton('back')

        @backButton\onClick(() -> @changeScreen('high_scores'))

        --table.insert(@sprites, @title)
        @addSprite(@text)
        @addSprite(@backButton)

        table.insert(@components, @backButton)

    load: () =>
        super()

        @text\setText("You Lose")

        windowWidth = Vector(love.graphics.getWidth(), 0)
        btnHorizOffset = (windowWidth - Vector(IMG_MENU_BTN\getWidth(), 0)) / 2
        @backButton.pos = btnHorizOffset + Vector(0, love.graphics.getHeight() - 150)
