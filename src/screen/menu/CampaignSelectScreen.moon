-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseMenuScreen    = require('screen/menu/BaseMenuScreen')
CampaignSelectBox = require('sprite/menu/CampaignSelectBox')
Vector            = require('math/Vector')
import IMG_MENU_CAMPAIGNS_BG, IMG_CMPGNS_BOX_BG from require('Resources')
import Input, Campaign from require('Services')
import insert from table


class CampaignSelectScreen extends BaseMenuScreen
    new: (game) =>
        super(game)

        @campaignBoxes = [@_makeCampaignBox(i) for i = 1, 3]

        @background = IMG_MENU_CAMPAIGNS_BG
        @transitioning = false
        @backDown = false

    _makeCampaignBox: (i) =>
        boxWidth = Vector(IMG_CMPGNS_BOX_BG\getWidth(), 0)
        offset = (i - 1) * (boxWidth + Vector(2, 0))
        pos = Vector(25, 134) + offset
        campaign = Campaign\get(i)
        box = CampaignSelectBox(self, pos, i, campaign)
        box\onClick(() ->
            Campaign\setActive(campaign)

            level = campaign\getLevel()
            @changeScreen("cutscene#{level}")
        )
        @addSprite(box)
        insert(@components, box)
        box

    load: () =>
        super(false)

    willDisplay: (oldScreen) =>
        super(oldScreen)
        @transitioning = true

        for box in *@campaignBoxes
            box\updateData()

    displayed: () =>
        super()
        @transitioning = false

    update: (dt) =>
        if not @transitioning
            super(dt)

            if Input\isDown(Input.NAV_BACK)
                if not @backDown
                    @backDown = true
                    @changeScreen('title')
            else
                @backDown = false
