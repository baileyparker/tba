-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseMenuScreen  = require('screen/menu/BaseMenuScreen')
HighScoresText  = require('sprite/menu/HighScoresText')
Textfield       = require('sprite/menu/Textfield')
MenuButton      = require('sprite/menu/MenuButton')
Vector          = require('math/Vector')

import HighScores from require('Services')
import IMG_MENU_BTN from require('Resources')


class YouWinScreen extends BaseMenuScreen
    new: (game) =>
        super(game)

        @text         = HighScoresText()
        @textfield    = Textfield()
        @actionBtn    = MenuButton(MenuButton.ACTION_BACK)
        @hasHighScore = false

        -- Center textfield
        hCenter = (Vector(love.graphics.getWidth(), 0) - (2 * @textfield.center\i!)) / 2
        @textfield.pos = hCenter + Vector(0, 220)

        -- Make hitting enter trigger button
        @actionBtn.hasFocus = true

        @actionBtn\onClick(() ->
            if not @hasHighScore or @_saveScore()
                -- Proceed if there was no high score, or if saving succeeded
                @changeScreen('high_scores')
        )

        @addSprite(@text)
        @addSprite(@textfield)
        @addSprite(@actionBtn)

        table.insert(@components, @actionBtn)

    _saveScore: () =>
        unless @hasHighScore and string.len(@textfield.text) == 3
            return false

        HighScores\addScore(@textfield.text, @game.score)
        return true

    willDisplay: (screen) =>
        super(screen)

        @hasHighScore = HighScores\isHighscore(@game.score)
        @textfield\setVisible(@hasHighScore)
        @textfield\setCapturing(@hasHighScore)

        if @hasHighScore
            @text\setText('NEW HIGHSCORE!')
            @actionBtn.action = MenuButton.ACTION_SAVE
        else
            @text\setText('You Win!')
            @actionBtn.action = MenuButton.ACTION_BACK

    willHide: (screen) =>
        super(screen)
        @textfield\setCapturing(false)

    load: () =>
        super()

        windowWidth = Vector(love.graphics.getWidth(), 0)

        btnHorizOffset = (windowWidth - Vector(IMG_MENU_BTN\getWidth(), 0)) / 2
        @actionBtn.pos = btnHorizOffset + Vector(0, love.graphics.getHeight() - 150)
