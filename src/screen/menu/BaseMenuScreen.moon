-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseScreen = require('screen/BaseScreen')
Vector     = require('math/Vector')

import Audio, Input from require('Services')
import
    IMG_MENU_BG
    IMG_MENU_CURSOR
    IMG_MENU_TRAIL
    IMG_PLANET_1
    IMG_PLANET_2
    IMG_PLANET_3
    IMG_PLANET_4
    IMG_PLANET_5
    IMG_PLANET_6
    IMG_PLANET_7
    IMG_PLANET_8
    IMG_PLANET_9
    MUS_MENU_BG
    from require('Resources')

QUAD_SIZE = 64
FRAMES_PER_PLANET = 8
TRAIL_SLOW_EMISSION_RATE = 10
TRAIL_FAST_EMISSION_RATE = 40

class BaseMenuScreen extends BaseScreen
    new: (game) =>
        super(game)

        @components = {}
        -- Not a typo, see MenuScreen\_selectNextComponent()
        @selectedComponent = 0

        @upPressed = false
        @downPressed = false
        @cursorPos = Vector(love.mouse.getPosition())
        @oldCursorVisible = false

        @time = 0
        @background = IMG_MENU_BG

    load: (planets=true) =>
        super()

        @cursor = love.mouse.newCursor(
            IMG_MENU_CURSOR\getData(), 3, 3
        )

        @cursorTrail = love.graphics.newParticleSystem(IMG_MENU_TRAIL, 100)
        @cursorTrail\setParticleLifetime(0.5, 0.8)
        @cursorTrail\setSpread(math.pi / 2)
        @cursorTrail\setDirection(math.pi4 + math.pi / 8)
        @cursorTrail\setColors(207, 0, 110, 255, 207, 0, 110, 0)
        @cursorTrail\setSpeed(120, 180)
        @cursorTrail\setTangentialAcceleration(80, 100)

        if planets
            @planetFrames = {}
            @currentPlanetFrame = 1

            for i=0, (FRAMES_PER_PLANET-1)
                quad = love.graphics.newQuad(QUAD_SIZE * i, 0, QUAD_SIZE, QUAD_SIZE, IMG_PLANET_1\getDimensions())
                table.insert(@planetFrames, quad)

    willDisplay: (oldScreen) =>
        if oldScreen == nil or not is_a(oldScreen, BaseMenuScreen)
            super()

            Audio\play(MUS_MENU_BG)
            love.mouse.setCursor(@cursor)
            @cursorTrail\reset()
            @cursorTrail\start()
            @cursorTrail\setEmissionRate(TRAIL_SLOW_EMISSION_RATE)

    willHide: (newScreen) =>
        if newScreen == nil or not is_a(newScreen, BaseMenuScreen)
            super()

            Audio\stop(MUS_MENU_BG)
            love.mouse.setCursor()

    update: (dt) =>
        super(dt)

        oldCursorPos = @cursorPos
        @cursorPos = Vector(love.mouse.getPosition())

        emissionRate = if Input\isMouseVisible() and @oldCursorVisible
            if oldCursorPos != @cursorPos
                TRAIL_FAST_EMISSION_RATE
            else
                TRAIL_SLOW_EMISSION_RATE
        else
            0

        @oldCursorVisible = Input\isMouseVisible()

        @cursorTrail\setEmissionRate(emissionRate)

        trailPos = @cursorPos + Vector(IMG_MENU_CURSOR\getDimensions()) / 2 + Vector(-2, 2)
        @cursorTrail\moveTo(trailPos\vals())
        @cursorTrail\update(dt)

        if Input\isDown(Input.NAV_UP)
            if not @upPressed then
                @upPressed = true
                @_selectNextComponent(-1)
        else
            @upPressed = false

        if Input\isDown(Input.NAV_DOWN)
            if not @downPressed
                @downPressed = true
                @_selectNextComponent(1)
        else
            @downPressed = false

        -- for updating current frame of planet
        @time += dt

        if @planetFrames and @time > 0.2
            @currentPlanetFrame = (@currentPlanetFrame % FRAMES_PER_PLANET) + 1
            @time = 0

    _selectNextComponent: (direction) =>
        -- <rant>If lua was 0-indexed, we wouldn't need this madness</rant>
        newComponent = 1 + (((@selectedComponent - 1) + direction) % (#@components))

        if newComponent <= #@components and (newComponent != @selectedComponent or not @components[newComponent].hasFocus)
            @components[@selectedComponent]\blur() if @components[@selectedComponent]
            @components[newComponent]\focus()
            @selectedComponent = newComponent

    _yieldCursorFocus: (mouseTarget) =>
        if 1 <= @selectedComponent and @selectedComponent <= #@components and @components[@selectedComponent] != mouseTarget
            @components[@selectedComponent]\blur()
            @selectedComponent = 0

    draw: () =>
        -- Set color so background doesn't have fading effect
        love.graphics.setColor(255, 255, 255, 255)
        -- Draw backgroun, scaled to fit screen
        love.graphics.draw(@background)

        -- Lower alpha and draw planets
        if @planetFrames
            love.graphics.setColor(255, 255, 255, 125)
            love.graphics.draw(IMG_PLANET_1, @planetFrames[@currentPlanetFrame], 720, 410, 0, .75, .75)
            love.graphics.draw(IMG_PLANET_2, @planetFrames[@currentPlanetFrame], 50, 300)
            love.graphics.draw(IMG_PLANET_3, @planetFrames[@currentPlanetFrame], 210, 550, 0, 1.75, 1.75)
            love.graphics.draw(IMG_PLANET_4, @planetFrames[@currentPlanetFrame], 710, 90, 1, 1.75, 1.75)
            love.graphics.draw(IMG_PLANET_5, @planetFrames[@currentPlanetFrame], -20, 520)
            love.graphics.draw(IMG_PLANET_6, @planetFrames[@currentPlanetFrame], 350, 20)
            love.graphics.draw(IMG_PLANET_7, @planetFrames[@currentPlanetFrame], 615, 300)
            love.graphics.draw(IMG_PLANET_8, @planetFrames[@currentPlanetFrame], 100, 100, 0, 1.5, 1.5)
            love.graphics.draw(IMG_PLANET_9, @planetFrames[@currentPlanetFrame], 485, 510)
            love.graphics.draw(IMG_PLANET_9, @planetFrames[@currentPlanetFrame], 550, -20)

        super()

        love.graphics.origin()
        love.graphics.draw(@cursorTrail)

