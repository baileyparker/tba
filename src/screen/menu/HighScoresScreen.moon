-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

BaseMenuScreen  = require('screen/menu/BaseMenuScreen')
HighScoresTitle = require('sprite/menu/HighScoresTitle')
HighScoresText  = require('sprite/menu/HighScoresText')
MenuButton      = require('sprite/menu/MenuButton')
Vector          = require('math/Vector')

import IMG_HS_HEADING, IMG_MENU_BTN from require('Resources')
import HighScores from require('Services')

import insert, concat from table


class HighScoreScreen extends BaseMenuScreen
    new: (game) =>
        super(game)

        @title      = HighScoresTitle()
        @text       = HighScoresText()
        @backButton = MenuButton('back')

        @backButton\onClick(() -> @changeScreen('title'))

        self\addSprite(@title)
        self\addSprite(@text)
        self\addSprite(@backButton)

        insert(@components, @backButton)

    willDisplay: () =>
        super()

        highscores = if HighScores\size() > 0
            concat(["#{i}    #{s}" for i, s in HighScores\get()], '\n')
        else
            'NO HIGHSCORES!'

        @text\setText(highscores)

    load: () =>
        super()

        windowWidth = Vector(love.graphics.getWidth(), 0)
        @title.pos = (windowWidth - Vector(IMG_HS_HEADING\getWidth())) / 2 + Vector(0, 100)

        btnHorizOffset = (windowWidth - Vector(IMG_MENU_BTN\getWidth(), 0)) / 2
        @backButton.pos = btnHorizOffset + Vector(0, love.graphics.getHeight() - 150)
