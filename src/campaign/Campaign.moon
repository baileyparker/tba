-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

class Campaign
    new: (@service) =>
        @used = false
        @level = 1
        @name = 'Willy'

    updateFromFile: (data) =>
        if data.used
            @used = data.used

        if data.level
            @level = data.level

        if data.name
            @name = data.name

    isUsed: () =>
        @used

    use: () =>
        if @used
            return

        @used = true
        @service\save()

    getLevel: () =>
        @level

    setLevel: (level) =>
        unless level != @level
            return

        @level = level
        @service\save()

    getName: () =>
        @name

    setName: (name) =>
        unless name != @name
            return

        @name = name
        @service\save()

    toData: () =>
        {used: @used, level: @level, name: @name}
