-- This is a list of all of the renderable layers
--
-- Notes: Give each layer a descriptive name, and a number.
--        Sort the list by first rendered appearing at the
--        top. The numbers must be unique and ascending (ie.
--        layers to be renedered later should have larger numbers).

{
    DEFAULT: 1
    HEROSHOT: 2
    ENEMY: 3
    HERO: 4
    ENEMYSHOT: 5
    PARTICLE: 6
    OVERLAY: 7
}
