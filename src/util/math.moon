-- Pollute the global scope with these useful math helpers
export *

math.tau = math.pi * 2
math.pi2 = math.pi / 2
math.pi4 = math.pi / 4

math.round = (val) -> math.floor(val + math.sign(val) * 0.5)
math.sign = (val) -> if val < 0 then -1 else 1
