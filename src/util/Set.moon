-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)


class Set
    new: () =>
        @items = {}

    add: (element) =>
        @items[element] = true

    remove: (element) =>
        @items[element] = nil

    toList: () =>
        -- http://stackoverflow.com/a/12674376/568785
        list = {}
        n = 0 -- table.insert is slow, so we'll keep our own index

        for k, _ in pairs(@items)
            n = n + 1
            list[n] = k

        list
