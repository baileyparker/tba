-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)


Set            = require('util/Set')
combinations   = require('math/combinations')
CollisionEvent = require('util/collision/CollisionEvent')

-- TODO: Refactor
--Collidable objects
Hero          = require('sprite/level/Hero')
Enemy         = require('sprite/level/Enemy')
EnemyShot     = require('sprite/level/EnemyShot')
EnemyLaser     = require('sprite/level/EnemyLaser')
PlayerMissile = require('sprite/level/PlayerMissile')


collides = (a, b) ->
    for _, abox in pairs(a.hitboxes)
        relAbox = abox\relativeTo(a.pos)

        for _, bbox in pairs(b.hitboxes)
            relBbox = bbox\relativeTo(b.pos)

            if relAbox\intersects(relBbox)
                return true

    false

class CollisionDetector
    new: () =>
        @bodies = Set()
        @callbacks = {}

    add: (body) =>
        @bodies\add(body)

    remove: (body) =>
        @bodies\remove(body)

    onCollision: (callback) =>
        table.insert(@callbacks, callback)

    --In the future, this function should only test certain types of collisions and ignore the rest
    detect: () =>
        -- Test all possible collisions
        for a, b in combinations(@bodies\toList())
            -- TODO: Beyond fragile, this need to be RFACTORED!
            if (is_a(a, Hero) and is_a(b, EnemyShot)) or (is_a(a, EnemyShot) and is_a(b, Hero)) or (is_a(a, Hero) and is_a(b, EnemyLaser)) or (is_a(a, EnemyLaser) and is_a(b, Hero)) or (is_a(a, Hero) and is_a(b, Enemy)) or (is_a(a, Enemy) and is_a(b, Hero)) or (is_a(a, Enemy) and is_a(b, PlayerMissile)) or (is_a(a, PlayerMissile) and is_a(b, Enemy))
                if collides(a, b)
                    event = CollisionEvent(a, b)

                    for _, callback in pairs(@callbacks)
                        callback(event)

return CollisionDetector
