-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)


class CollisionEvent
    new: (@a, @b) =>
        -- pass

    between: (typeA, typeB) =>
        (is_a(@a, typeA) and is_a(@b, typeB)) or (is_a(@a, typeB) and is_a(@b, typeA))

    get: (t) =>
        if is_a(@a, t)
            @a
        elseif is_a(@b, t)
            @b
