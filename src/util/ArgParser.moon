-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

class ArgParser
    new: (args) =>
        @named = {}
        @positional = {}

        posIdx = 1
        name = nil

        for value in *args[2,]
            if value\sub(1, 1) == '-'
                if name != nil
                    @named[name] = true

                name = value\sub(if value\len() == 2 then 2 else 3)
            else
                if name == nil
                    @positional[posIdx] = value
                    posIdx += 1
                else
                    @named[name] = value
                    name = nil

        if name != nil
            @named[name] = true

    _get: (longName, shortName) =>
        if type(longName) == 'number'
            @positional[longName]
        else
            @named[shortName] or @named[longName]

    has: (longName, shortName=nil) =>
        @_get(longName, shortName) != nil

    get: (longName, shortName=nil, default=false) =>
        @_get(longName, shortName) or default
