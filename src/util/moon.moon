-- These are normally defined by moonscript, but since
-- we may be running without it, we have to manually define them
-- Source: https://github.com/leafo/moonscript/blob/24b3a86/moonscript/util.moon
export *

is_object = (value) -> -- is a moonscript object
    type(value) == "table" and value.__class

is_a = (thing, t) -> -- tests if object is instance of type
    return false unless type(thing) == "table"
    cls = thing.__class
    while cls
        if cls == t
            return true
        cls = cls.__parent

    false
