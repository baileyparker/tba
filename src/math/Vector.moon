-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

class Vector
    new: (@x = 0, @y = 0) =>
        @_dist, @_angle = nil, nil

    -- Vector length
    dist: () =>
        @_dist or= math.sqrt(@x ^ 2 + @y ^ 2)
        @_dist

    -- Return a normalized copy of this vector
    norm: () =>
        if @dist() == 0
            Vector(0, 0)
        else
            Vector(@x / @dist(), @y / @dist())

    -- Angle this vector forms with the positive x-axis
    angle: () =>
        var_atan = math.atan2(@y, @x)
        if var_atan < 0
            @_angle or= math.abs(var_atan)
        else
            @_angle or= math.pi*2 - var_atan
        @_angle

    -- Angle from this vector to another
    angleTo: (other) =>
        other\angle() - @angle()

    -- Angle between two vectors
    angleBetween: (other) =>
        if math.abs(@angleTo(other)) > math.pi
            math.pi*2 - math.abs(@angleTo(other))
        else
            math.abs(@angleTo(other))

    -- Restrict components to a range
    restrict: (minX, maxX, minY, maxY) =>
        x = math.max(minX, math.min(@x, maxX))
        y = math.max(minY, math.min(@y, maxY))

        if @x ~= x or @y ~= y then Vector(x, y) else self

    dot: (other) =>
        @x * other.x + @y * other.y

    proj: (other) =>
        (other\dot(self) / (other\dist() ^ 2)) * other

    i: () =>
        unless @_i
            @_i = Vector(@x, 0)

        @_i

    j: () =>
        unless @_j
            @_j = Vector(0, @y)

        @_j

    -- Adds two vectors
    __add: (other) =>
        Vector(@x + other.x, @y + other.y)

    -- Subtracts two vectors
    __sub: (other) =>
        Vector(@x - other.x, @y - other.y)

    -- Multiple a vector by a scalar
    __mul: (self, factor) ->
        if type(self) == 'number'
            self, factor = factor, self

        Vector(@x * factor, @y * factor)

    -- Divide a vector by a scalar
    __div: (self, factor) ->
        if type(self) == 'number'
            self, factor = factor, self

        self * (1 / factor)

    -- Unary minus of the vector
    __unm: () =>
        self * -1

    -- Vector equality testing
    __eq: (other) =>
        @x == other.x and @y == other.y

    -- Vector comparison (compares both coords...useful for bounds checking)
    __lt: (other) =>
        @x < other.x and @y < other.y

    __le: (other) =>
        @x <= other.x and @y <= other.y

    -- Return vector as tuple (convenient for passing as args)
    vals: () =>
        @x, @y

Vector.i = Vector(1, 0)
Vector.j = Vector(0, 1)

Vector
