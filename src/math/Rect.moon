-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)


class Rect
    new: (@top_left, @dimensions) =>
        @center       = top_left + (dimensions / 2)
        @bottom_right = top_left + dimensions

    relativeTo: (origin) =>
        Rect(@top_left + origin, @dimensions)

    intersects: (other) =>
        @top_left.x     < other.bottom_right.x and
        @bottom_right.x > other.top_left.x     and
        @top_left.y     < other.bottom_right.y and
        @bottom_right.y > other.top_left.y
