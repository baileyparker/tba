-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)


-- Iterator that produces a list of 2-tuple
-- combinations from the input list (no replacement)
--
-- ex. combinations({1,2,3}) == {(1,2), (1,3), (2,3)}
combinations = (list) ->
    i, j = 1, 1
    len = #list -- cache len because its fairly slow

    () ->
        j += 1
        
        if j > len
            i += 1
            j = i + 1

        if i <= len and j <= len
            return list[i], list[j]

-- "Return" the function when this file is require'd
-- (not necessary for classes)
combinations
