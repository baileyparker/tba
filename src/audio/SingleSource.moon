-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Audio = require('audio/Audio')


class SingleSource extends Audio
    new: (@source) =>
        super()

    isPlaying: () =>
        @source\isPlaying()

    play: () =>
        @source\play()

    isPaused: () =>
        @source\isPaused()

    pause: () =>
        @source\pause()

    resume: () =>
        @source\resume()

    stop: () =>
        @source\stop()

    setVolume: (volume) =>
        super(volume)
        @source\setVolume(volume)
