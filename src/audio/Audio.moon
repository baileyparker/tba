-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)


class Audio
    isPlaying: () =>
        -- pass

    play: () =>
        -- pass

    isPaused: () =>
        -- pass

    pause: () =>
        -- pass

    resume: () =>
        -- pass

    stop: () =>
        -- pass

    setVolume: (@volume) =>
        -- pass
