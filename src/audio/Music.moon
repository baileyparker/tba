-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

SingleSource = require('audio/SingleSource')

import newSource from love.audio


class Music extends SingleSource
    new: (path, loops=true) =>
        super(newSource(path, 'stream'))
        @source\setLooping(loops)
