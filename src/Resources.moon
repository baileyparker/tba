-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

SoundEffect = require('audio/SoundEffect')
Music       = require('audio/Music')

import newImage, newFont from love.graphics
import newSource from love.audio


RES_DIR = 'res/'
IMG_DIR = RES_DIR .. 'images/'
SND_DIR = RES_DIR .. 'sounds/'
FNT_DIR = RES_DIR .. 'fonts/'

loadImage = (path)             -> newImage(IMG_DIR .. path)
loadSfx   = (path)             -> SoundEffect(SND_DIR .. path, 'static')
loadMusic = (path, loops=true) -> Music(SND_DIR .. path, loops)
loadFont  = (path, size)       -> newFont(FNT_DIR .. path, size)

dialogues = {
    101, 102, 103, 104, 105, 106, 107, 108, 109,
    201, 202, 203, 204, 205, 206,
    301, 302, 303, 304, 305, 306,
    401, 402, 403, 404, 405, 406, 407, 408, 409, 410
}


{
    -- Image Assets
    IMG_LOGO: loadImage('logo.png')
    IMG_MENU_BG: loadImage('bg/menuBackground.png')
    IMG_NEW_MENU_BG: loadImage('menu.png')
    IMG_MENU_CAMPAIGNS_BG: loadImage('bg/menu_campaigns.png')
    IMG_SETTINGS_BG: loadImage('settingsBg.png')

    IMG_MENU_CURSOR: loadImage('cursor.png')
    IMG_MENU_BTN: loadImage('menu_sprites.png')
    IMG_MENU_HVR_CAMPAIGN: loadImage('menu_hover_campaign.png')
    IMG_MENU_HVR_OPTIONS: loadImage('menu_hover_options.png')
    IMG_MENU_HVR_QUIT: loadImage('menu_hover_quit.png')
    IMG_MENU_TEXTFIELD: loadImage('textfield.png')
    IMG_MENU_TRAIL: loadImage('cursor_trail.png')

    IMG_CMPGNS_BOX_BG: loadImage('menu/campaign_select_box.png')
    IMG_CMPGNS_BOX_PREVIEWS: [loadImage("menu/preview_stage#{i}.png") for i = 1, 4]
    IMG_CMPGNS_BOX_NEW: loadImage('menu/campaign_select_new.png')
    IMG_CMPGNS_BOX_NAME_BG: loadImage('menu/campaign_select_name_bg.png')
    IMG_CMPGNS_BOX_PLAY: loadImage('menu/campaign_select_play.png')

    IMG_PAUSE_MENU: loadImage('pause_menu.png')

    IMG_HS_HEADING: loadImage('highscores_title.png')
    IMG_SETTINGS_HEADING: loadImage('settings_title.png')
    IMG_SLIDER_TRACK: loadImage('slider_track.png')
    IMG_SLIDER_NOTCH: loadImage('slider_notch.png')

    IMG_CUTSCENE1_FRAMES: [loadImage("cutscenes/introduction/frame_#{i}.png") for i = 1, 12]
    IMG_CUTSCENE2_FRAMES: [loadImage("cutscenes/two/frame_#{i}.png") for i = 1, 7]
    IMG_CUTSCENE3_FRAMES: [loadImage("cutscenes/three/frame_#{i}.png") for i = 1, 9]
    IMG_CUTSCENE4_FRAMES: [loadImage("cutscenes/four/frame_#{i}.png") for i = 1, 10]
    IMG_CUTSCENE5_FRAMES: [loadImage("cutscenes/final/frame_#{i}.png") for i = 1, 14]

    IMG_LEVEL1_BG: loadImage('bg/background_empty.png')
    IMG_LEVEL1_PLANETS: loadImage('bg/background_planets.png')
    IMG_LEVEL2_BG: loadImage('bg/background_stage2base.jpg')
    IMG_LEVEL3_BG: loadImage('bg/background_stage3_base.jpg')
    IMG_LEVEL3_CLOUDS: loadImage('bg/background_stage3_clouds.png')
    IMG_LEVEL4_BG: loadImage('bg/background_stage4.png')

    IMG_PLANET_1: loadImage('bg/planet_1_green.png')
    IMG_PLANET_2: loadImage('bg/planet_1_purple.png')
    IMG_PLANET_3: loadImage('bg/planet_1_red.png')
    IMG_PLANET_4: loadImage('bg/planet_2_blue.png')
    IMG_PLANET_5: loadImage('bg/planet_2_red.png')
    IMG_PLANET_6: loadImage('bg/planet_2_yellow.png')
    IMG_PLANET_7: loadImage('bg/planet_3_blue.png')
    IMG_PLANET_8: loadImage('bg/planet_3_green.png')
    IMG_PLANET_9: loadImage('bg/planet_3_purple.png')

    IMG_SHOTS: loadImage('shotsheet.png')
    IMG_LASER: loadImage('laser.png')
    IMG_BULLETDELETE: loadImage('bulletdelete.png')
    IMG_HEART: loadImage('heart.png')
    IMG_STAGE_HEART: loadImage('heart_s.png')
    IMG_BANNERBOX: loadImage('eff_particle1.png')
    IMG_CHARGECOMPLETE: loadImage('eff_particle2.png')
    IMG_DARKCHARGE: loadImage('eff_particle3.png')

    IMG_WHALE: loadImage('dyingWilly.png')
    IMG_WHALE_BAD: loadImage('bad_whale.png')

    IMG_ENEMY_1: loadImage('enemy1_green_s.png')
    IMG_ENEMY_2: loadImage('enemy2_red_s.png')
    IMG_ENEMY_3: loadImage('enemy3_yellow_s.png')
    IMG_ENEMY_4: loadImage('enemy2_red_s.png')
    IMG_ENEMY_5: loadImage('enemy2_red_s.png')
    IMG_ENEMY_6: loadImage('enemy1_green_s.png')
    IMG_ENEMY_7: loadImage('enemy2_red_s.png')
    IMG_ENEMY_8: loadImage('enemy3_yellow_s.png')
    IMG_ENEMY_9: loadImage('enemy1_green_s.png')
    IMG_ENEMY_10: loadImage('enemy2_red_s.png')
    IMG_ENEMY_11: loadImage('enemy3_yellow_s.png')
    IMG_ENEMY_12: loadImage('enemy1_green_s.png')
    IMG_ENEMY_13: loadImage('enemy2_red_s.png')
    IMG_ENEMY_SPECIAL: loadImage('enemy4.png')
    IMG_ENEMY_BOSS_1: loadImage('boss1animation.png')
    IMG_ENEMY_BOSS_2: loadImage('boss2animation.png')
    IMG_ENEMY_BOSS_3_PSY: loadImage('enemy1_psy.png')
    IMG_ENEMY_BOSS_1_SHIP: loadImage('boss1_ship.png')
    IMG_ENEMY_BOSS_2_SHIP: loadImage('boss2_ship.png')
    IMG_ENEMY_BOSS_3_SHIP: loadImage('boss3_ship.png')

    IMG_BLANKET_FRAMES: [loadImage("blanket_#{i}.png") for i = 1, 5]

    IMG_MASK_HEART: loadImage('heart_mask.png')

    IMG_DIALOGUE: {id, loadImage("dialogue/dialogue_#{id}.png") for id in *dialogues}

    -- Sound Effects
    SFX_LOGO: loadSfx('se_logo.mp3')
    SFX_ARROW: loadSfx('se_arrow.mp3')
    SFX_CRASH: loadSfx('se_crash.mp3')
    SFX_MECHA: loadSfx('se_mecha.mp3')
    SFX_SLICEWAVE: loadSfx('se_slicewave.mp3')
    SFX_SLASH: loadSfx('se_slash.mp3')
    SFX_DARKCHARGE: loadSfx('se_darkcharge.mp3')
    SFX_DULLBURST: loadSfx('se_dullburst.mp3')
    SFX_ELEBURST: loadSfx('se_eleburst.mp3')
    SFX_GENTLEWAVE: loadSfx('se_gentlewave.mp3')
    SFX_LONGBURST: loadSfx('se_longburst.mp3')
    SFX_THUNDER: loadSfx('se_thunder.wav')

    SFX_ENEMYHIT: loadSfx('se_enemyhit.mp3')
    SFX_ENEMYHIT2: loadSfx('se_enemyhit2.mp3')
    SFX_ENEMYHIT3: loadSfx('se_enemyhit3.mp3')
    SFX_ENEMYHIT4: loadSfx('se_enemyhit4.mp3')
    SFX_ENEMYCHARGE: loadSfx('se_enemycharge.mp3')
    SFX_PLAYERSHOT: loadSfx('se_playershot.mp3')
    SFX_CUTSCENE1_KIDNAP: loadSfx('se_cutscenekidnap.mp3')
    SFX_POWERUP: loadSfx('se_powerup.mp3')
    SFX_MOOSE: loadSfx('se_moose.mp3')
    SFX_ASTRALUP: loadSfx('se_astralup.mp3')
    SFX_BOUNCE: loadSfx('se_bounce.mp3')
    SFX_EXPLODE: loadSfx('se_fall.mp3')
    SFX_SHIELD: loadSfx('se_shield.wav')

    -- Music Assets
    MUS_MENU_BG: loadMusic('menu_song.mp3')
    MUS_CUTSCENE1_BG: loadMusic('intro.mp3', false)
    MUS_CUTSCENE2_BG: loadMusic('cutscene_2.mp3', false)
    MUS_CUTSCENE5_BG: loadMusic('cutscene_5.mp3', false)
    MUS_LEVEL1_BG: loadMusic('bgm_stage1.mp3')
    MUS_LEVEL1_BOSS: loadMusic('bgm_boss1.mp3')
    MUS_LEVEL2_BG: loadMusic('bgm_stage2.mp3')
    MUS_LEVEL2_BOSS: loadMusic('bgm_boss2.mp3')
    MUS_LEVEL3_BG: loadMusic('bgm_stage3.mp3')
    MUS_LEVEL3_BOSS: loadMusic('bgm_boss3.mp3')
    MUS_LEVEL4_BG: loadMusic('bgm_stage4.mp3')
    MUS_LEVEL4_BOSS: loadMusic('bgm_boss4.mp3')

    -- Font Assets
    FNT_BODY: loadFont('BABYCAKE.ttf', 20)
    FNT_BODY_MED: loadFont('Georgia Belle.ttf', 24)
    FNT_BODY_SMALL: loadFont('Georgia Belle.ttf', 20)
    FNT_HEADING: loadFont('BABYCAKE.ttf', 48)
    FNT_DEBUG: loadFont('Andale Mono.ttf', 15)
}
