// Discards transparent pixles from a texture
// (useful for using an image as an alpha mask for a stencil)

vec4 effect(vec4 color, Image tex, vec2 tex_coords, vec2 screen_coords) {
    if(Texel(tex, tex_coords).a == 0.0) {
        discard;
    }

    return vec4(1.0);
}
