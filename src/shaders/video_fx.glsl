// ADjusts brightness and contrast

extern number contrast;
extern number brightness;

vec4 effect(vec4 color, Image tex, vec2 text_coords, vec2 screen_coords) {
    vec4 tex_color = Texel(tex, text_coords);

    // Undo alpha pre-multiply
    tex_color.rgb /= tex_color.a;

    // Apply constrast adjust
    tex_color.rgb = (tex_color.rgb - 0.5f) * contrast + 0.5f;

    // Apply brightness adjust
    tex_color.rgb += brightness;

    // Redo pre-multiplied alpha
    tex_color.rgb *= tex_color.a;

    return tex_color * color;
}
