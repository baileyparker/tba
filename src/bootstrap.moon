-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

-- Pollutes global scope with some necessitites
require('util/math')
require('util/moon')

-- Instantiate all services by requiring this for the first time
Services             = require('Services')

-- Regular requires
Game                 = require('Game')
SettingsService      = require('services/SettingsService')
ArgParser            = require('util/ArgParser')
SplashScreen         = require('screen/cutscenes/SplashScreen')
IntroductionCutscene = require('screen/cutscenes/IntroductionCutscene')
StageTwoCutscene     = require('screen/cutscenes/StageTwoCutscene')
StageThreeCutscene   = require('screen/cutscenes/StageThreeCutscene')
StageFourCutscene   = require('screen/cutscenes/StageFourCutscene')
FinalCutscene        = require('screen/cutscenes/FinalCutscene')
Stage1Screen         = require('screen/levels/Stage1Screen')
Stage2Screen         = require('screen/levels/Stage2Screen')
Stage3Screen         = require('screen/levels/Stage3Screen')
Stage4Screen         = require('screen/levels/Stage4Screen')
TitleScreen          = require('screen/menu/TitleScreen')
CampaignSelectScreen = require('screen/menu/CampaignSelectScreen')
HighScoresScreen     = require('screen/menu/HighScoresScreen')
SettingsScreen       = require('screen/menu/SettingsScreen')
YouWinScreen         = require('screen/menu/YouWinScreen')
YouLoseScreen        = require('screen/menu/YouLoseScreen')


game = nil

love.load = (args) ->
    arguments = ArgParser(args)

    game = Game()

    -- Setttings has dependencies on other services, so we delay its creation
    Services.Settings.game = game
    Services.Settings\load() -- try to load settings from file

    -- Prepares all the game's screens
    -- (load their resources)
    splashScreen = SplashScreen(game)
    game\addScreen('splash', splashScreen)

    titleScreen = TitleScreen(game)
    game\addScreen('title', titleScreen)

    campaignSelectScreen = CampaignSelectScreen(game)
    game\addScreen('campaign_select', campaignSelectScreen)

    highScoresScreen = HighScoresScreen(game)
    game\addScreen('high_scores', highScoresScreen)

    settingsScreen = SettingsScreen(game)
    game\addScreen('settings', settingsScreen)

    introCutsceneScreen = IntroductionCutscene(game)
    game\addScreen('cutscene1', introCutsceneScreen)

    stage2CutsceneScreen = StageTwoCutscene(game)
    game\addScreen('cutscene2', stage2CutsceneScreen)

    stage3CutsceneScreen = StageThreeCutscene(game)
    game\addScreen('cutscene3', stage3CutsceneScreen)

    stage4CutsceneScreen = StageFourCutscene(game)
    game\addScreen('cutscene4', stage4CutsceneScreen)

    finalCutsceneScreen = FinalCutscene(game)
    game\addScreen('cutscene5', finalCutsceneScreen)

    stage1Screen = Stage1Screen(game)
    game\addScreen('stage1', stage1Screen)

    stage2Screen = Stage2Screen(game)
    game\addScreen('stage2', stage2Screen)

    stage3Screen = Stage3Screen(game)
    game\addScreen('stage3', stage3Screen)

    stage4Screen = Stage4Screen(game)
    game\addScreen('stage4', stage4Screen)

    youWinScreen = YouWinScreen(game)
    game\addScreen('you_win', youWinScreen)

    youLoseScreen = YouLoseScreen(game)
    game\addScreen('you_lose', youLoseScreen)

    -- No music flag
    if arguments\get('no-music', 'm', false)
        Services.Audio\setMusicVolume(0)

    -- Special konami code easer egg
    if arguments\get('konami', nil, false)
        require('sprite/level/EnemyType1').enableKonamiMode()

    -- Delegates all calls to update() and draw() to the default screen
    game\showInitialScreen(arguments\get('screen', 's', 'splash'))


love.keypressed = (key) ->
    -- pass


love.mousefocus = (visible) ->
    Services.Input\onMouseVisibility(visible)


love.joystickadded = (joystick) ->
    name = joystick\getName()
    print("Joystick connected: #{name}")
    Services.Input\setJoystick(joystick)


love.update = (dt) ->
    game\update(dt)


love.draw = () ->
    game\draw()


love.quit = () ->
    -- Nothing important to do here yet
