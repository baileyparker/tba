-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
utf8   = require('utf8')

import IMG_MENU_TEXTFIELD, FNT_BODY from require('Resources')


-- TODO: refactor global state
target = nil

-- TODO: Refactor, this conflicts with other love.textinput defs
love.textinput = (c) ->
    if target
        target\_addChar(c)


class Textfield extends Sprite
    new: (pos, @text='') =>
        super(pos)

        @backspaceDown = false
        @setCapturing(false)
        @setVisible(true)
        @center = Vector(IMG_MENU_TEXTFIELD\getDimensions()) / 2

    setCapturing: (@capturing) =>
        if @capturing
            @setVisible(true)

        target = if @capturing then self else nil

    setVisible: (@visible) =>
        unless @visible
            @setCapturing(false)

    _addChar: (c) =>
        -- TODO: refactor out highscore specific constraints
        unless string.len(@text) == 3 or c == ' '
            @text ..= c

    update: (dt) =>
        super(dt)

        if @capturing
            if love.keyboard.isDown('backspace')
                if not @backspaceDown
                    offset = utf8.offset(@text, -1)

                    if offset
                        @text = string.sub(@text, 1, offset - 1)

                    @backspaceDown = true
            else
                @backspaceDown = false

    draw: () =>
        super()

        if @visible
            @drawImage(IMG_MENU_TEXTFIELD)
            love.graphics.setColor(255, 255, 255, 255)
            love.graphics.setFont(FNT_BODY)
            pad = ((@center.y * 2) - FNT_BODY\getHeight()) / 2
            love.graphics.print(@text, pad, pad, 0, 1, 1, @center\vals())
