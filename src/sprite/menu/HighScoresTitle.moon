-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')

import IMG_HS_HEADING from require('Resources')


class HighScoresTitle extends Sprite
    load: () =>
        super()

        imageDimensions = Vector(IMG_HS_HEADING\getDimensions())
        screenDimensions = Vector(love.graphics.getDimensions())

        @pos = (screenDimensions - imageDimensions) / 2
        @center = imageDimensions / 2

    draw: () =>
        super()

        love.graphics.setColor(255, 255, 255, 255)
        @drawImage(IMG_HS_HEADING)
