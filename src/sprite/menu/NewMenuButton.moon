-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')

import Input from require('Services')
import
    IMG_MENU_HVR_CAMPAIGN
    IMG_MENU_HVR_OPTIONS
    IMG_MENU_HVR_QUIT
    from require('Resources')


class NewMenuButton extends Sprite
    @ACTION_PLAY       = 'campaign'
    @ACTION_OPTIONS    = 'options'
    @ACTION_QUIT       = 'quit'

    new: (action, @screen, pos=Vector(0, 0)) =>
        super(pos)

        @action = action
        @mouseDown = false
        @clickListeners = {}
        @hasFocus = false
        @enterPressed = false
        @active = false

        if action == @@ACTION_PLAY
            @hover = IMG_MENU_HVR_CAMPAIGN
            @pos = Vector(182, 245)
        elseif action == @@ACTION_OPTIONS
            @hover = IMG_MENU_HVR_OPTIONS
            @pos = Vector(190, 317)
        else
            @hover = IMG_MENU_HVR_QUIT
            @pos = Vector(236, 389)

        @center = Vector(@hover\getDimensions()) / 2

    onClick: (callback) =>
        table.insert(@clickListeners, callback)

    _fireClick: () =>
        for i, callback in ipairs(@clickListeners)
            callback()

    blur: () =>
        @hasFocus = false

    focus: () =>
        @hasFocus = true

    update: (dt) =>
        super(dt)

        mousePos = Vector(love.mouse.getPosition())
        topLeft = @pos + Vector(10, 10)
        bottomRight = @pos + @getDimensions() - Vector(10, 10)

        if topLeft <= mousePos and mousePos <= bottomRight
            @screen\_yieldCursorFocus(self)
            if love.mouse.isDown('l')
                @active = true
                @mouseDown = true
            else
                if @mouseDown
                    @_fireClick()
                else
                    @active = true

                @mouseDown = false
        else
            @active = false
            @mouseDown = false

        if @hasFocus
            @active = true

            if Input\isDown(Input.NAV_SELECT)
                @active = true
                @enterPressed = true
            else
                if @enterPressed
                    @enterPressed = false
                    @_fireClick()

    getDimensions: () =>
        @center * 2

    draw: () =>
        super()

        if @active
            @drawImage(@hover)
