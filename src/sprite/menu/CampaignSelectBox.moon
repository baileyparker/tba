-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')

import Input from require('Services')
import
    IMG_CMPGNS_BOX_BG
    IMG_CMPGNS_BOX_PREVIEWS
    IMG_CMPGNS_BOX_NAME_BG
    IMG_CMPGNS_BOX_PLAY
    IMG_CMPGNS_BOX_NEW
    FNT_BODY_MED
    FNT_BODY_SMALL
    from require('Resources')

STAGE_NAMES = {'Chapter One', 'Chapter Two', 'Chapter Three', 'Chapter Four'}

ALPHA_NORMAL = 0.70
ALPHA_HOVER  = 0.97
ALPHA_ACTIVE = 1.00

SCALE_NORMAL = 0.96
SCALE_HOVER  = 1.00
SCALE_ACTIVE = 0.98

class CampaignSelectBox extends Sprite
    new: (@screen, pos, @index, @campaign) =>
        super(pos)

        @updateData()

        @mouseDown = false
        @clickListeners = {}
        @hasFocus = false
        @enterPressed = false
        @active = false
        @hover = false

        @center = Vector(IMG_CMPGNS_BOX_BG\getDimensions()) / 2

    onClick: (callback) =>
        table.insert(@clickListeners, callback)

    _fireClick: () =>
        for i, callback in ipairs(@clickListeners)
            callback()

    blur: () =>
        @hasFocus = false

    focus: () =>
        @hasFocus = true

    updateData: () =>
        @used = @campaign\isUsed()
        @level = @campaign\getLevel()
        @playerName = @campaign\getName()

    update: (dt) =>
        super(dt)

        mousePos = Vector(love.mouse.getPosition())
        topLeft = @pos
        bottomRight = @pos + @getDimensions()

        if topLeft <= mousePos and mousePos <= bottomRight
            @hover = true
            @screen\_yieldCursorFocus(self)
            if love.mouse.isDown('l')
                @active = true
                @mouseDown = true
            else
                if @mouseDown
                    @_fireClick()

                @mouseDown = false
        else
            @active = false
            @hover = false
            @mouseDown = false

        if @hasFocus
            @hover = true

            if Input\isDown(Input.NAV_SELECT)
                @active = true
                @enterPressed = true
            else
                if @enterPressed
                    @enterPressed = false
                    @_fireClick()

    getDimensions: () =>
        @center * 2

    draw: () =>
        super()

        alpha, scale = if @active
            ALPHA_ACTIVE, SCALE_ACTIVE
        elseif @hover
            ALPHA_HOVER , SCALE_HOVER
        else
            ALPHA_NORMAL, SCALE_NORMAL

        -- Draw bg
        love.graphics.setColor(255, 255, 255, 255 * alpha)
        switch @index
            when 2
                @drawImage(IMG_CMPGNS_BOX_BG, 0, scale, -scale)
            when 3
                @drawImage(IMG_CMPGNS_BOX_BG, 0, -scale, scale)
            else
                @drawImage(IMG_CMPGNS_BOX_BG, 0, scale)

        if @used
            @drawImage(IMG_CMPGNS_BOX_PREVIEWS[@level], 0, scale)
            @drawImage(IMG_CMPGNS_BOX_NAME_BG, 0, scale)

            -- Player Name
            love.graphics.setFont(FNT_BODY_MED)
            love.graphics.setColor(216, 99, 138, 255 * alpha)
            love.graphics.printf(@playerName, 0, 5, 166, "center", 0, scale, scale, 83, 12)
            love.graphics.setColor(255, 255, 255, 255 * alpha)

            -- Level Name
            love.graphics.setFont(FNT_BODY_SMALL)
            love.graphics.setColor(99, 107, 113, 255 * alpha)
            love.graphics.printf(STAGE_NAMES[@level], 0, 55, 166, "center", 0, scale, scale, 83, 10)
            love.graphics.setColor(255, 255, 255, 255 * alpha)

            @drawImage(IMG_CMPGNS_BOX_PLAY, 0, scale)
        else
            @drawImage(IMG_CMPGNS_BOX_NEW, 0, scale)
