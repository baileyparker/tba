-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')

import insert from table

import
    IMG_SLIDER_TRACK
    IMG_SLIDER_NOTCH
    from require('Resources')


class Slider extends Sprite
    new: (pos, @value = 0.5) =>
        super(pos)
        @listeners = {}

        @trackWidth = IMG_SLIDER_TRACK\getWidth()
        @center = Vector(@trackWidth, IMG_SLIDER_NOTCH\getHeight()) / 2
        @mouseDown = false
        @mouseDownInside = false

    onChange: (callback) =>
        insert(@listeners, callback)

    update: (dt) =>
        if love.mouse.isDown('l')
            newValue = @_getMouseValue()

            if newValue != nil and (@mouseDownInside or not @mouseDown)
                @mouseDownInside = true
                oldvalue, @value = @value, newValue

                if oldvalue != @value
                    callback(@value) for i, callback in ipairs @listeners

            @mouseDown = true
        else
            @mouseDown = false
            @mouseDownInside = false

    _getMouseValue: () =>
        pos = Vector(love.mouse.getPosition())
        topLeft = @pos

        -- Only consider x coordinate after mouse down inside slider
        -- to allow user to move cursor up and down without jittering
        if @mouseDownInside
            if topLeft.x < pos.x and pos.x < (topLeft + @getDimensions()).x
                (pos - topLeft).x / @getDimensions().x
            else
                nil
        else
            if topLeft < pos and pos < (topLeft + @getDimensions())
                (pos - topLeft).x / @getDimensions().x
            else
                nil

    getDimensions: () =>
        @center * 2

    draw: () =>
        super()

        @drawImage(IMG_SLIDER_TRACK)
        notchCenter = Vector(IMG_SLIDER_NOTCH\getDimensions()) / 2
        x = IMG_SLIDER_TRACK\getWidth() * (@value - 0.5)
        love.graphics.draw(IMG_SLIDER_NOTCH, x, 0, 0, 1, 1, notchCenter\vals())
