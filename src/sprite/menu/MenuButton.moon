-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')

import Input from require('Services')
import IMG_MENU_BTN from require('Resources')


class MenuButton extends Sprite
    @ACTION_PLAY       = 'play'
    @ACTION_HIGHSCORES = 'highScores'
    @ACTION_SETTINGS   = 'settings'
    @ACTION_BACK       = 'back'
    @ACTION_SAVE       = 'save'

    new: (action) =>
        super()

        @action = action
        @scale = 1
        @mouseDown = false
        @clickListeners = {}
        @hasFocus = false
        @enterPressed = false

    load: () =>
        super()

        @quads = {
            play: love.graphics.newQuad(
                0, 0, IMG_MENU_BTN\getWidth(), 54, IMG_MENU_BTN\getDimensions()
            ),
            highScores: love.graphics.newQuad(
                0, 54, IMG_MENU_BTN\getWidth(), 54, IMG_MENU_BTN\getDimensions()
            ),
            settings: love.graphics.newQuad(
                0, 108, IMG_MENU_BTN\getWidth(), 54, IMG_MENU_BTN\getDimensions()
            ),
            back: love.graphics.newQuad(
                0, 162, IMG_MENU_BTN\getWidth(), 54, IMG_MENU_BTN\getDimensions()
            ),
            save: love.graphics.newQuad(
                0, 216, IMG_MENU_BTN\getWidth(), 54, IMG_MENU_BTN\getDimensions()
            )
        }

        @center = Vector(IMG_MENU_BTN\getDimensions()) / 2

    onClick: (callback) =>
        table.insert(@clickListeners, callback)

    _fireClick: () =>
        for i, callback in ipairs(@clickListeners)
            callback()

    blur: () =>
        @hasFocus = false

    focus: () =>
        @hasFocus = true

    update: (dt) =>
        super(dt)

        mousePos = Vector(love.mouse.getPosition())
        bottomRight = @pos + @getDimensions()

        if @pos <= mousePos and mousePos <= bottomRight
            if love.mouse.isDown('l')
                @scale = 0.94
                @mouseDown = true
            else
                if @mouseDown
                    @_fireClick()
                else
                    @scale = 1

                @mouseDown = false
        else
            @scale = 0.97
            @mouseDown = false

        if @hasFocus
            @scale = 1

            if Input\isDown(Input.NAV_SELECT)
                @scale = 0.97
                @enterPressed = true
            else
                if @enterPressed
                    @enterPressed = false
                    @_fireClick()

    getDimensions: () =>
        Vector(IMG_MENU_BTN\getWidth(), IMG_MENU_BTN\getHeight() / 5)

    draw: () =>
        super()
        @drawQuad(IMG_MENU_BTN, @quads[@action], 0, @scale)
