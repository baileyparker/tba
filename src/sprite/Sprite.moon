-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Vector = require('math/Vector')
Layers = require('Layers')

class Sprite
    new: (@pos = Vector()) =>
        @center   = Vector(0, 0)
        @hitboxes = {}
        @layer = Layers.DEFAULT

    -- Load all assests for the sprite
    load: () =>
        -- Stub

    -- Set the center of the sprite so that it can positioned properly
    _setCenter: (x, y) =>
        @center = Vector(x, y)

    -- Called on every game update when the sprite is in a visible screen
    update: (dt) =>
        -- Stub

    -- Called on every game draw when the sprite is in a visible screen
    draw: () =>
        -- Reset position transformations
        love.graphics.origin()

        -- Reset color to prevent weird image drawing behavior
        love.graphics.setColor(255, 255, 255, 255)

        -- Translate so that top left is (0, 0)
        love.graphics.translate((@pos + @center)\vals())

    -- Convenience function for drawing images
    drawImage: (image, angle = 0, scaleX = 1, scaleY = scaleX) =>
        love.graphics.draw(image, 0, 0, angle, scaleX, scaleY, @center\vals())

    -- Convenience function for drawing quads from spritesheets
    -- TODO: refactor into spritesheet resource
    drawQuad: (image, quad, angle = 0, scaleX = 1, scaleY = scaleX) =>
        love.graphics.draw(image, quad, 0, 0, angle, scaleX, scaleY, @center\vals())
