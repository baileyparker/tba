-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

import IMG_BANNERBOX from require('Resources')


class StageBannerEffect extends Sprite
    new: (pos = Vector(), @posf = Vector(), r = 255, g = 255, b = 255, screen) =>
        super(pos)

        @rgb = {r, g, b}
        @time = 0 -- relative to sprite's creation
        @alpha = 0
        @angle = 0
        @parentscreen = screen

        @layer = Layers.PARTICLE

    load: () =>
        super()

        @quad = love.graphics.newQuad(0, 0, 16, 16, IMG_BANNERBOX\getDimensions())

    update: (dt) =>
        -- Update velocity and position
        delta = @pos - @posf
        @pos -= delta * dt * 2
        if math.abs(delta.x) < 15 or math.abs(delta.y) < 15
            @pos = @posf

        @alpha = if @time < 1
                     @time * 255
                 elseif @time > 1 and @time < 5
                     255
                 elseif @time > 5 and @alpha > 0
                     255 - (@time - 5) * 255
                 else
                     @alpha

        if @time > 5 and @alpha <= 0
            @parentscreen\removeSprite(self)

        @time += dt

    draw: () =>
        --Below handles translation of graphic
        super()

        love.graphics.setColor(@rgb[1], @rgb[2], @rgb[3], @alpha)
        love.graphics.setBlendMode('alpha')
        @drawQuad(IMG_BANNERBOX, @quad, @angle)

        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setBlendMode('alpha')
