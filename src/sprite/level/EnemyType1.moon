-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import IMG_ENEMY_1, IMG_ENEMY_SPECIAL, SFX_MECHA from require('Resources')


QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 8

class EnemyType1 extends Enemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)

        @attackcounter = 0
        @hitboxes = {Rect(Vector(9, 10), Vector(46, 32))}

        @animdir = 1
        @current_quad = 1

		-- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2

    load: () =>
        super()

        @center = Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_1\getDimensions())
            table.insert(@destroyAnim, quad)

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt

        --Control fining shots here
        @attackcounter += dt

        if @attackcounter > 2 and @hp > 0
            angtoplay = @getAngleToHeroPos(@pos + @center/2)
            for i = 0, 5
               newbullet = EnemyShot(@pos + @center/2, angtoplay * (200 + i * 15), EnemyShot.SHOT_GRAPHIC_BEAM_AQUA, @screen)
               newbullet\load(@screen.resources)
               @screen\addSprite(newbullet)

            @attackcounter = 0
            Audio\play(SFX_MECHA)

        if @time > 5
            @vel = @vel\norm()

        if @time > 8
            @vel = Vector((@time-8)*20, 0)
            @attackcounter = 0 --disable shooting

        --Following Deletes enemy
        if @time > 8 and (@pos.x > 864 or @pos.x < -64 or @pos.y < -64 or @pos.y > 664)
            @screen\removeSprite(self)
            @screen.enemyCount -= 1
            @addSpareBonus()

        if @exploding
            @screen\removeSprite(self)

    @enableKonamiMode = () ->
        IMG_ENEMY_1 = IMG_ENEMY_SPECIAL

    draw: () =>
        super()

		if @timeSinceLastFrame > @frameRate
			@timeSinceLastFrame = 0.0
	        @current_quad += @animdir

	        if @current_quad == 1 and @animdir == -1
	            @animdir = 1
	        elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
	            @animdir = -1
	            @current_quad = FRAMES_PER_ANIM
	     		
        anim = @destroyAnim[@current_quad]

        -- Draw enemy at angle of travel
        @drawQuad(IMG_ENEMY_1, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
