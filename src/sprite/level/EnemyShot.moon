-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

import IMG_SHOTS from require('Resources')

class EnemyShot extends Sprite
    --Below are shot constants that can be used to refer to specific graphics.
    --As a note, there are exactly 21 colors per bullet type.

    @SHOT_GRAPHIC_BALL_GRAY             = 1
    @SHOT_GRAPHIC_BALL_RED              = 2
    @SHOT_GRAPHIC_BALL_DARKORANGE       = 3
    @SHOT_GRAPHIC_BALL_ORANGE           = 4
    @SHOT_GRAPHIC_BALL_YELLOW           = 5
    @SHOT_GRAPHIC_BALL_GREENYELLOW      = 6
    @SHOT_GRAPHIC_BALL_LAWNGREEN        = 7
    @SHOT_GRAPHIC_BALL_GREEN            = 8
    @SHOT_GRAPHIC_BALL_DEEPGREEN        = 9
    @SHOT_GRAPHIC_BALL_LIGHTGREEN       = 10
    @SHOT_GRAPHIC_BALL_AQUA             = 11
    @SHOT_GRAPHIC_BALL_CYAN             = 12
    @SHOT_GRAPHIC_BALL_BLUE             = 13
    @SHOT_GRAPHIC_BALL_DARKBLUE         = 14
    @SHOT_GRAPHIC_BALL_INDIGO           = 15
    @SHOT_GRAPHIC_BALL_MIDNIGHT         = 16
    @SHOT_GRAPHIC_BALL_VIOLET           = 17
    @SHOT_GRAPHIC_BALL_MAGENTA          = 18
    @SHOT_GRAPHIC_BALL_FUSCHIA          = 19
    @SHOT_GRAPHIC_BALL_CORAL            = 20
    @SHOT_GRAPHIC_BALL_LIGHTRED         = 21
    @SHOT_GRAPHIC_BALLINV_GRAY          = 22
    @SHOT_GRAPHIC_BALLINV_CYAN          = 23
    @SHOT_GRAPHIC_BALLINV_BLUE          = 24
    @SHOT_GRAPHIC_BALLINV_DARKBLUE      = 25
    @SHOT_GRAPHIC_BALLINV_INDIGO        = 26
    @SHOT_GRAPHIC_BALLINV_MIDNIGHT      = 27
    @SHOT_GRAPHIC_BALLINV_VIOLET        = 28
    @SHOT_GRAPHIC_BALLINV_MAGENTA       = 29
    @SHOT_GRAPHIC_BALLINV_FUSCHIA       = 30
    @SHOT_GRAPHIC_BALLINV_CORAL         = 31
    @SHOT_GRAPHIC_BALLINV_LIGHTRED      = 32
    @SHOT_GRAPHIC_BALLINV_RED           = 33
    @SHOT_GRAPHIC_BALLINV_DARKORANGE    = 34
    @SHOT_GRAPHIC_BALLINV_ORANGE        = 35
    @SHOT_GRAPHIC_BALLINV_YELLOW        = 36
    @SHOT_GRAPHIC_BALLINV_GREENYELLOW   = 37
    @SHOT_GRAPHIC_BALLINV_LAWNGREEN     = 38
    @SHOT_GRAPHIC_BALLINV_GREEN         = 39
    @SHOT_GRAPHIC_BALLINV_DEEPGREEN     = 40
    @SHOT_GRAPHIC_BALLINV_LIGHTGREEN    = 41
    @SHOT_GRAPHIC_BALLINV_AQUA          = 42
    @SHOT_GRAPHIC_DOT_GRAY              = 43
    @SHOT_GRAPHIC_DOT_RED               = 44
    @SHOT_GRAPHIC_DOT_DARKORANGE        = 45
    @SHOT_GRAPHIC_DOT_ORANGE            = 46
    @SHOT_GRAPHIC_DOT_YELLOW            = 47
    @SHOT_GRAPHIC_DOT_GREENYELLOW       = 48
    @SHOT_GRAPHIC_DOT_LAWNGREEN         = 49
    @SHOT_GRAPHIC_DOT_GREEN             = 50
    @SHOT_GRAPHIC_DOT_DEEPGREEN         = 51
    @SHOT_GRAPHIC_DOT_LIGHTGREEN        = 52
    @SHOT_GRAPHIC_DOT_AQUA              = 53
    @SHOT_GRAPHIC_DOT_CYAN              = 54
    @SHOT_GRAPHIC_DOT_BLUE              = 55
    @SHOT_GRAPHIC_DOT_DARKBLUE          = 56
    @SHOT_GRAPHIC_DOT_INDIGO            = 57
    @SHOT_GRAPHIC_DOT_MIDNIGHT          = 58
    @SHOT_GRAPHIC_DOT_VIOLET            = 59
    @SHOT_GRAPHIC_DOT_MAGENTA           = 60
    @SHOT_GRAPHIC_DOT_FUSCHIA           = 61
    @SHOT_GRAPHIC_DOT_CORAL             = 62
    @SHOT_GRAPHIC_DOT_LIGHTRED          = 63
    @SHOT_GRAPHIC_DOTINV_GRAY           = 64
    @SHOT_GRAPHIC_DOTINV_CYAN           = 65
    @SHOT_GRAPHIC_DOTINV_BLUE           = 66
    @SHOT_GRAPHIC_DOTINV_DARKBLUE       = 67
    @SHOT_GRAPHIC_DOTINV_INDIGO         = 68
    @SHOT_GRAPHIC_DOTINV_MIDNIGHT       = 69
    @SHOT_GRAPHIC_DOTINV_VIOLET         = 70
    @SHOT_GRAPHIC_DOTINV_MAGENTA        = 71
    @SHOT_GRAPHIC_DOTINV_FUSCHIA        = 72
    @SHOT_GRAPHIC_DOTINV_CORAL          = 73
    @SHOT_GRAPHIC_DOTINV_LIGHTRED       = 74
    @SHOT_GRAPHIC_DOTINV_RED            = 75
    @SHOT_GRAPHIC_DOTINV_DARKORANGE     = 76
    @SHOT_GRAPHIC_DOTINV_ORANGE         = 77
    @SHOT_GRAPHIC_DOTINV_YELLOW         = 78
    @SHOT_GRAPHIC_DOTINV_GREENYELLOW    = 79
    @SHOT_GRAPHIC_DOTINV_LAWNGREEN      = 80
    @SHOT_GRAPHIC_DOTINV_GREEN          = 81
    @SHOT_GRAPHIC_DOTINV_DEEPGREEN      = 82
    @SHOT_GRAPHIC_DOTINV_LIGHTGREEN     = 83
    @SHOT_GRAPHIC_DOTINV_AQUA           = 84
    @SHOT_GRAPHIC_PELLET_GRAY           = 85
    @SHOT_GRAPHIC_PELLET_RED            = 86
    @SHOT_GRAPHIC_PELLET_DARKORANGE     = 87
    @SHOT_GRAPHIC_PELLET_ORANGE         = 88
    @SHOT_GRAPHIC_PELLET_YELLOW         = 89
    @SHOT_GRAPHIC_PELLET_GREENYELLOW    = 90
    @SHOT_GRAPHIC_PELLET_LAWNGREEN      = 91
    @SHOT_GRAPHIC_PELLET_GREEN          = 92
    @SHOT_GRAPHIC_PELLET_DEEPGREEN      = 93
    @SHOT_GRAPHIC_PELLET_LIGHTGREEN     = 94
    @SHOT_GRAPHIC_PELLET_AQUA           = 95
    @SHOT_GRAPHIC_PELLET_CYAN           = 96
    @SHOT_GRAPHIC_PELLET_BLUE           = 97
    @SHOT_GRAPHIC_PELLET_DARKBLUE       = 98
    @SHOT_GRAPHIC_PELLET_INDIGO         = 99
    @SHOT_GRAPHIC_PELLET_MIDNIGHT       = 100
    @SHOT_GRAPHIC_PELLET_VIOLET         = 101
    @SHOT_GRAPHIC_PELLET_MAGENTA        = 102
    @SHOT_GRAPHIC_PELLET_FUSCHIA        = 103
    @SHOT_GRAPHIC_PELLET_CORAL          = 104
    @SHOT_GRAPHIC_PELLET_LIGHTRED       = 105
    @SHOT_GRAPHIC_PELLETINV_GRAY        = 106
    @SHOT_GRAPHIC_PELLETINV_CYAN        = 107
    @SHOT_GRAPHIC_PELLETINV_BLUE        = 108
    @SHOT_GRAPHIC_PELLETINV_DARKBLUE    = 109
    @SHOT_GRAPHIC_PELLETINV_INDIGO      = 110
    @SHOT_GRAPHIC_PELLETINV_MIDNIGHT    = 111
    @SHOT_GRAPHIC_PELLETINV_VIOLET      = 112
    @SHOT_GRAPHIC_PELLETINV_MAGENTA     = 113
    @SHOT_GRAPHIC_PELLETINV_FUSCHIA     = 114
    @SHOT_GRAPHIC_PELLETINV_CORAL       = 115
    @SHOT_GRAPHIC_PELLETINV_LIGHTRED    = 116
    @SHOT_GRAPHIC_PELLETINV_RED         = 117
    @SHOT_GRAPHIC_PELLETINV_DARKORANGE  = 118
    @SHOT_GRAPHIC_PELLETINV_ORANGE      = 119
    @SHOT_GRAPHIC_PELLETINV_YELLOW      = 120
    @SHOT_GRAPHIC_PELLETINV_GREENYELLOW = 121
    @SHOT_GRAPHIC_PELLETINV_LAWNGREEN   = 122
    @SHOT_GRAPHIC_PELLETINV_GREEN       = 123
    @SHOT_GRAPHIC_PELLETINV_DEEPGREEN   = 124
    @SHOT_GRAPHIC_PELLETINV_LIGHTGREEN  = 125
    @SHOT_GRAPHIC_PELLETINV_AQUA        = 126
    @SHOT_GRAPHIC_BEAM_GRAY             = 127
    @SHOT_GRAPHIC_BEAM_RED              = 128
    @SHOT_GRAPHIC_BEAM_DARKORANGE       = 129
    @SHOT_GRAPHIC_BEAM_ORANGE           = 130
    @SHOT_GRAPHIC_BEAM_YELLOW           = 131
    @SHOT_GRAPHIC_BEAM_GREENYELLOW      = 132
    @SHOT_GRAPHIC_BEAM_LAWNGREEN        = 133
    @SHOT_GRAPHIC_BEAM_GREEN            = 134
    @SHOT_GRAPHIC_BEAM_DEEPGREEN        = 135
    @SHOT_GRAPHIC_BEAM_LIGHTGREEN       = 136
    @SHOT_GRAPHIC_BEAM_AQUA             = 137
    @SHOT_GRAPHIC_BEAM_CYAN             = 138
    @SHOT_GRAPHIC_BEAM_BLUE             = 139
    @SHOT_GRAPHIC_BEAM_DARKBLUE         = 140
    @SHOT_GRAPHIC_BEAM_INDIGO           = 141
    @SHOT_GRAPHIC_BEAM_MIDNIGHT         = 142
    @SHOT_GRAPHIC_BEAM_VIOLET           = 143
    @SHOT_GRAPHIC_BEAM_MAGENTA          = 144
    @SHOT_GRAPHIC_BEAM_FUSCHIA          = 145
    @SHOT_GRAPHIC_BEAM_CORAL            = 146
    @SHOT_GRAPHIC_BEAM_LIGHTRED         = 147
    @SHOT_GRAPHIC_BEAMINV_GRAY          = 148
    @SHOT_GRAPHIC_BEAMINV_CYAN          = 149
    @SHOT_GRAPHIC_BEAMINV_BLUE          = 150
    @SHOT_GRAPHIC_BEAMINV_DARKBLUE      = 151
    @SHOT_GRAPHIC_BEAMINV_INDIGO        = 152
    @SHOT_GRAPHIC_BEAMINV_MIDNIGHT      = 153
    @SHOT_GRAPHIC_BEAMINV_VIOLET        = 154
    @SHOT_GRAPHIC_BEAMINV_MAGENTA       = 155
    @SHOT_GRAPHIC_BEAMINV_FUSCHIA       = 156
    @SHOT_GRAPHIC_BEAMINV_CORAL         = 157
    @SHOT_GRAPHIC_BEAMINV_LIGHTRED      = 158
    @SHOT_GRAPHIC_BEAMINV_RED           = 159
    @SHOT_GRAPHIC_BEAMINV_DARKORANGE    = 160
    @SHOT_GRAPHIC_BEAMINV_ORANGE        = 161
    @SHOT_GRAPHIC_BEAMINV_YELLOW        = 162
    @SHOT_GRAPHIC_BEAMINV_GREENYELLOW   = 163
    @SHOT_GRAPHIC_BEAMINV_LAWNGREEN     = 164
    @SHOT_GRAPHIC_BEAMINV_GREEN         = 165
    @SHOT_GRAPHIC_BEAMINV_DEEPGREEN     = 166
    @SHOT_GRAPHIC_BEAMINV_LIGHTGREEN    = 167
    @SHOT_GRAPHIC_BEAMINV_AQUA          = 168
    @SHOT_GRAPHIC_FLOWER_GRAY           = 169
    @SHOT_GRAPHIC_FLOWER_RED            = 170
    @SHOT_GRAPHIC_FLOWER_DARKORANGE     = 171
    @SHOT_GRAPHIC_FLOWER_ORANGE         = 172
    @SHOT_GRAPHIC_FLOWER_YELLOW         = 173
    @SHOT_GRAPHIC_FLOWER_GREENYELLOW    = 174
    @SHOT_GRAPHIC_FLOWER_LAWNGREEN      = 175
    @SHOT_GRAPHIC_FLOWER_GREEN          = 176
    @SHOT_GRAPHIC_FLOWER_DEEPGREEN      = 177
    @SHOT_GRAPHIC_FLOWER_LIGHTGREEN     = 178
    @SHOT_GRAPHIC_FLOWER_AQUA           = 179
    @SHOT_GRAPHIC_FLOWER_CYAN           = 180
    @SHOT_GRAPHIC_FLOWER_BLUE           = 181
    @SHOT_GRAPHIC_FLOWER_DARKBLUE       = 182
    @SHOT_GRAPHIC_FLOWER_INDIGO         = 183
    @SHOT_GRAPHIC_FLOWER_MIDNIGHT       = 184
    @SHOT_GRAPHIC_FLOWER_VIOLET         = 185
    @SHOT_GRAPHIC_FLOWER_MAGENTA        = 186
    @SHOT_GRAPHIC_FLOWER_FUSCHIA        = 187
    @SHOT_GRAPHIC_FLOWER_CORAL          = 188
    @SHOT_GRAPHIC_FLOWER_LIGHTRED       = 189
    @SHOT_GRAPHIC_FLOWERINV_GRAY        = 190
    @SHOT_GRAPHIC_FLOWERINV_CYAN        = 191
    @SHOT_GRAPHIC_FLOWERINV_BLUE        = 192
    @SHOT_GRAPHIC_FLOWERINV_DARKBLUE    = 193
    @SHOT_GRAPHIC_FLOWERINV_INDIGO      = 194
    @SHOT_GRAPHIC_FLOWERINV_MIDNIGHT    = 195
    @SHOT_GRAPHIC_FLOWERINV_VIOLET      = 196
    @SHOT_GRAPHIC_FLOWERINV_MAGENTA     = 197
    @SHOT_GRAPHIC_FLOWERINV_FUSCHIA     = 198
    @SHOT_GRAPHIC_FLOWERINV_CORAL       = 199
    @SHOT_GRAPHIC_FLOWERINV_LIGHTRED    = 200
    @SHOT_GRAPHIC_FLOWERINV_RED         = 201
    @SHOT_GRAPHIC_FLOWERINV_DARKORANGE  = 202
    @SHOT_GRAPHIC_FLOWERINV_ORANGE      = 203
    @SHOT_GRAPHIC_FLOWERINV_YELLOW      = 204
    @SHOT_GRAPHIC_FLOWERINV_GREENYELLOW = 205
    @SHOT_GRAPHIC_FLOWERINV_LAWNGREEN   = 206
    @SHOT_GRAPHIC_FLOWERINV_GREEN       = 207
    @SHOT_GRAPHIC_FLOWERINV_DEEPGREEN   = 208
    @SHOT_GRAPHIC_FLOWERINV_LIGHTGREEN  = 209
    @SHOT_GRAPHIC_FLOWERINV_AQUA        = 210

    -- Bullet fired by the player (sidescrolling section)
    new: (pos, @vel, @graphic = 0, screen) =>
        super(pos)

        @accel = Vector(0, 0)
        @time = 0 -- relative to sprite's creation
        @parentscreen = screen
        @setIntersection()
        @sprites = {}

        @layer = Layers.ENEMYSHOT

        @undeletable = false

    load: () =>
        super()

        -- The quads for the bullets in the shotsheet
        for j = 0, 3
            for i = 0, 21
                quadtoadd = love.graphics.newQuad(16 * i, 16*j, 16, 16, IMG_SHOTS\getDimensions())
                table.insert(@sprites, j*21 + i + 1, quadtoadd)

        j = 4
        --Seperate while loops because the enemy shotsheet bypasses the missile shape bullets and player bullets
        for j = 4, 9
            for i = 0, 21
                quadtoadd = love.graphics.newQuad(16 * i, 16*(j+2), 16, 16, IMG_SHOTS\getDimensions())
                table.insert(@sprites, j*21 + i + 1, quadtoadd)

        -- The sprites are 16x16
        @center = Vector(8, 8)

    update: (dt) =>
        -- Update velocity and position
        @vel += dt * @accel
        @pos += dt * @vel

        @time += dt

        -- If out of bounds
        if (@pos.x < -32 or @pos.y < -32 or @pos.x > 832 or @pos.y > 632) and (not @undeletable)
            @parentscreen\removeSprite(self)

    draw: () =>
        super()

        -- Draw EnemyShot at angle of travel
        angle = @vel\angle()

        if @graphic >= 169 and @graphic < 211
            angle = @time * 7

        @drawQuad(IMG_SHOTS, @sprites[@graphic], math.pi * 2 - angle + math.pi/2)

    setIntersection: (l, w) =>
        --First vector is top left of hitbox. Second vector is dementions
        @hitboxes = if @graphic < 43
                        {Rect(Vector(8-5, 8-5), Vector(10, 10))}
                    elseif @graphic < 86
                        {Rect(Vector(8-2, 8-2), Vector(4, 4))}
                    elseif @graphic < 127
                        {Rect(Vector(8-3, 8-3), Vector(6, 6))}
                    elseif @graphic < 169
                        {Rect(Vector(8-2, 8-2), Vector(4, 4))}
                    elseif @graphic < 211
                        {Rect(Vector(8-3, 8-3), Vector(6, 6))}
