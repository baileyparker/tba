-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
import IMG_STAGE_HEART from require('Resources')

Layers = require('Layers')

QUAD_SIZE    = 16
HEART_FRAMES = 13

-- The stage heart particle effect
class StageHeart extends Sprite

    new: (pos = Vector(), screen, @vel = Vector(0, -16)) =>
        super(pos)
        @time = 0
        @currentHeart = 1

        @parentscreen = screen

        @layer = Layers.PARTICLE

    load: () =>
        super()

        @heartAnim = {}
        for i = 0, HEART_FRAMES
            quad = love.graphics.newQuad(QUAD_SIZE * i, 0, QUAD_SIZE, QUAD_SIZE, IMG_STAGE_HEART\getDimensions())
            table.insert(@heartAnim, quad)

        @center = Vector(QUAD_SIZE, QUAD_SIZE) / 2

    update: (dt) =>
        @time += dt
        @pos += dt * @vel

        if @time > 2.2
            @currentHeart = (@currentHeart % HEART_FRAMES) + 1
            @time = 2

        if (1 - @time / 2) < 0
            @parentscreen\removeSprite(self)

    draw: () =>
        super()

        love.graphics.setColor(255, 255, 255, 255 * (1 - @time / 2))
        @drawQuad(IMG_STAGE_HEART, @heartAnim[@currentHeart], 0)
