-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
import IMG_BULLETDELETE from require('Resources')

Layers = require('Layers')

QUAD_SIZE    = 32
FRAMES = 8

-- The stage heart particle effect
class BulletDelete extends Sprite

    new: (pos = Vector(), screen, @vel = Vector(0, 0)) =>
        super(pos)
        @time = 0
        @currentFrame = 1

        @parentscreen = screen

        @layer = Layers.PARTICLE

    load: () =>
        super()

        @heartAnim = {}
        for i = 0, FRAMES
            quad = love.graphics.newQuad(QUAD_SIZE * i, 0, QUAD_SIZE, QUAD_SIZE, IMG_BULLETDELETE\getDimensions())
            table.insert(@heartAnim, quad)

        @center = Vector(QUAD_SIZE, QUAD_SIZE) / 2

    update: (dt) =>
        @time += dt
        @pos += dt * @vel

        if @time > 0.05
            @currentFrame += 1
            @time = 0

        if @currentFrame > FRAMES
            @parentscreen\removeSprite(self)
            @currentFrame = FRAMES --for good measure

    draw: () =>
        super()

        love.graphics.setColor(255, 255, 255, 255 * (1 - @time / 2))
        @drawQuad(IMG_BULLETDELETE, @heartAnim[@currentFrame], 0)
