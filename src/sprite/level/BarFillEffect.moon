-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

import IMG_CHARGECOMPLETE from require('Resources')

-- This is used for filling bars such as lifebars and cooldown bars

class BarFillEffect extends Sprite
    new: (pos = Vector(), r = 255, g = 255, b = 255, screen) =>
        super(pos)

        @vel = Vector(0,0)
        @destpos = pos -- final position
        @angle = math.random(360)
        @disttofinalpos = 32
        @timetilldeletion = 0.5
        @distfromdest = @disttofinalpos
        @rgb = {r, g, b}
        @time = 0 -- relative to sprite's creation
        @alpha = 0
        @parentscreen = screen

        @layer = Layers.PARTICLE

    load: () =>
        super()

        @quad = love.graphics.newQuad(0, 0, 8, 8, IMG_CHARGECOMPLETE\getDimensions())

    update: (dt) =>
        -- Update position
        @distfromdest -= dt*@disttofinalpos/@timetilldeletion
        @pos = @destpos + Vector(@distfromdest*math.cos(math.rad(@angle)), @distfromdest*math.sin(math.rad(@angle)))

        @alpha = if @time < @timetilldeletion
                     (@time/@timetilldeletion) * 150
                 elseif @time > @timetilldeletion and @alpha > 0
                     255
                 else
                     @alpha

        if @time > @timetilldeletion
            @parentscreen\removeSprite(self)

        @time += dt

    draw: () =>
        --Below handles translation of graphic
        super()

        love.graphics.setColor(@rgb[1], @rgb[2], @rgb[3], @alpha)
        love.graphics.setBlendMode('alpha')
        @drawQuad(IMG_CHARGECOMPLETE, @quad, @angle)

        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setBlendMode('alpha')
