-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

import IMG_HEART from require('Resources')


class PlayerMissile extends Sprite
    -- Bullet fired by the player (sidescrolling section)
    new: (pos, @vel, @graphic = 0, screen) =>
        super(pos)

        @accel = Vector(0, 0)

        -- Keep track of which quad is currently being displayed
        @current_quad = 1

        @time = 0 -- relative to sprite's creation
        @parentscreen = screen
        @hitboxes = {Rect(Vector(0, 0), Vector(16, 16))}

        @layer = Layers.HEROSHOT

    load: () =>
        super()

        -- The quads for the bullets in the shotsheet
        @shotanim = {}
        for i = 1, 12
            table.insert(@shotanim, love.graphics.newQuad(64 * i, 0, 64, 64, IMG_HEART\getDimensions()))

        -- The sprite is 64,64 but is rendered 16x16
        @center = Vector(8, 12)

    update: (dt) =>
        -- Update velocity and position
        @vel += dt * @accel
        @pos += dt * @vel
        @time += dt

        if @pos.x < -32 or @pos.y < -32 or @pos.x > 832 or @pos.y > 632
            @parentscreen\removeSprite(self)

    draw: () =>
        --Below handles translation of graphic
        super()

        -- Draw PlayerMissile at angle of travel
        -- Alternate between frames every 0.2 seconds
        if @time % 0.4 > 0.2 and @current_quad <= 12
            @current_quad += 1

        if @current_quad > 12
            @current_quad = 1

        anim = @shotanim[@current_quad]
        angle = @vel\angle()
        @drawQuad(IMG_HEART, anim, angle - math.pi/2, 0.25, 0.25)
