-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import IMG_ENEMY_13 from require('Resources')

--PLEASE DO NOT USE AS A TEMPLATE


QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 8

class EnemyType13 extends Enemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)
        --@vel = Vector(-0.000000001,0)

        @origvel = @vel

        @attackcounter = 0
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(9, 10), Vector(46, 32))}

        @animdir = 1
        @current_quad = 1

        -- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2

    load: () =>
        super()

        @center = Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_13\getDimensions())
            table.insert(@destroyAnim, quad)

        @gravityspawn()

    update: (dt) =>
        super(dt)

        @vel = @origvel + Vector(120*math.cos(@time*3), 0) --Oscillating in x direction

        @timeSinceLastFrame += dt

        --Following Deletes enemy
        if @time > 6 and (@pos.x > 864 or @pos.x < -64 or @pos.y < -64 or @pos.y > 664)
            @screen\removeSprite(self)
            @screen.enemyCount -= 1
            @addSpareBonus()

        if @exploding
            @screen\removeSprite(self)

    gravityspawn: () =>
        gravity_co = coroutine.create(() ->
            while @hp > 0
                newbullet = EnemyShot(@pos + @center/2, Vector(math.random(80) - 40, -1*math.random(120)-60), EnemyShot.SHOT_GRAPHIC_PELLET_MAGENTA, @screen)
                newbullet.accel = Vector(0, math.random(30)+50)
                newbullet\load(@screen.resources)
                @screen\addSprite(newbullet)
                timeinit = @screen.controltime
                while @screen.controltime < timeinit + 0.5
                    coroutine.yield()
        )
        table.insert(@screen.coroutines, gravity_co)

    draw: () =>
        super()

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM
                
        -- Draw enemy at angle of travel
        anim = @destroyAnim[@current_quad]
        @drawQuad(IMG_ENEMY_13, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
