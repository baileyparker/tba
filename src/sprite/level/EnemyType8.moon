-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import IMG_ENEMY_8, SFX_DULLBURST from require('Resources')


QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 8

class EnemyType8 extends Enemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)
        --@vel = Vector(-0.000000001, 0)

        @attackcounter = math.random(100)/100
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(9, 10), Vector(46, 32))}

        @animdir = 1
        @current_quad = 1

        -- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2
        
    load: () =>
        super()

        @center = Vector(QUAD_SIZEX / 2, QUAD_SIZEX / 2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_8\getDimensions())
            table.insert(@destroyAnim, quad)

    update: (dt) =>
        super(dt)
        
        --Control fining shots here
        @attackcounter += dt

        if @attackcounter > 1 and @hp > 0 and not(@pos.x > 832 or @pos.x < -32 or @pos.y < -32 or @pos.y > 632) and @pos.x > 128
            angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center/2)\angle() --2pi is due to Vector using a different coordinate system from Lua
            newbullet = EnemyShot(@pos + @center / 2, Vector(150*math.cos(angleT), 150*math.sin(angleT)), EnemyShot.SHOT_GRAPHIC_FLOWERINV_CORAL, @screen)
            newbullet\load(@screen.resources)
            @screen\addSprite(newbullet)
            for i = 0, 1
                angleT = math.random(2*math.pi)
                newbullet = EnemyShot(@pos + @center / 2, Vector(150*math.cos(angleT), 150*math.sin(angleT)), EnemyShot.SHOT_GRAPHIC_DOT_INDIGO, @screen)
                newbullet\load(@screen.resources)
                @screen\addSprite(newbullet)

            @attackcounter = 0
            Audio\play(SFX_DULLBURST)

        --Following Deletes enemy
        if @time > 2 and (@pos.x > 864 or @pos.x < -64 or @pos.y < -64 or @pos.y > 664)
            @screen\removeSprite(self)
            @screen.enemyCount = @screen.enemyCount - 1
            @addSpareBonus()

        if @exploding
            @screen\removeSprite(self)

    draw: () =>
        super()

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM

        anim = @destroyAnim[@current_quad]
        -- Draw enemy at angle of travel
        @drawQuad(IMG_ENEMY_8, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
