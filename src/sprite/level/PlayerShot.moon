-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

import IMG_SHOTS from require('Resources')


class PlayerShot extends Sprite
    -- Bullet fired by the player (sidescrolling section)
    new: (pos, @vel, @graphic = 0, screen) =>
        super(pos)

        @accel = Vector(0, 0)
        @time = 0 -- relative to sprite's creation

        @parentscreen = screen
        @hitboxes = {Rect(Vector(0, 0), Vector(32, 32))}

        @layer = Layers.HEROSHOT

    load: () =>
        super()

        -- The quads for the bullets in the shotsheet
        @shotanim = {
            love.graphics.newQuad(
                336 + 16 * @graphic, 64, 16, 16, IMG_SHOTS\getDimensions()
            )
        }

        -- The sprite is 32x16
        @center = Vector(8, 8)

    update: (dt) =>
        -- Update velocity and position
        @vel += dt * @accel
        @pos += dt * @vel
        @time += dt

        if @pos.x < -32 or @pos.y < -32 or @pos.x > 832 or @pos.y > 632
            @parentscreen\removeSprite(self)

    draw: () =>
        super()

        -- Draw PlayerShot at angle of travel
        -- Alternate between frames every 0.2 seconds (doesn't actually do this!)
        @drawQuad(IMG_SHOTS, @shotanim[2], @vel\angle())
