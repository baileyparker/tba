-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite     = require('sprite/Sprite')
Vector     = require('math/Vector')
Rect       = require('math/Rect')
PlayerShot = require('sprite/level/PlayerShot')
Layers     = require('Layers')

import Input, Audio from require('Services')
import
    IMG_WHALE
    IMG_BLANKET_FRAMES
    SFX_BOUNCE
    SFX_EXPLODE
    from require('Resources')


-- Velocity of the Hero when moved with WASD/arrow keys
VEL              = 250

QUAD_SIZE        = 64
FRAMES_PER_ANIM  = 3

EXPLOSION_FRAMES = 25
EXPLOSION_SIZE   = 64

BLANKET_FRAMES = 5

class Hero extends Sprite
    new: (screen) =>
        super()

        @screen = screen

        -- Whether the hero is colliding with a wall
        @collided = false
        @visible = true

        @time = 0
        @count = 0
        @angle = 0 --updated from velocity data in Hero\update

        -- Keep track of which quad is currently being displayed
        @current_quad = 1

        -- Keep track of which direction the animation is moving on the spritesheet
        @animdir = 1

        @hitboxes = {Rect(Vector(0, 0), Vector(0, 0))} --See update for actual hitbox control

        --Power Ups
        @blanket = false
        @blanketlength = 0

        @explosionStart = nil
        @explosionFrame = 1

        @layer = Layers.HERO

    load: () =>
        super()

        @center = Vector(QUAD_SIZE / 2, QUAD_SIZE / 2)

        @flameAnim = {}

        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(
                QUAD_SIZE * i, 0, QUAD_SIZE, QUAD_SIZE, IMG_WHALE\getDimensions()
            )
            table.insert(@flameAnim, quad)

        @explosionFrames = {}

        for i = 0, (EXPLOSION_FRAMES - 1)
            quad = love.graphics.newQuad(EXPLOSION_SIZE * i, 0, EXPLOSION_SIZE, EXPLOSION_SIZE, IMG_WHALE\getDimensions())
            table.insert(@explosionFrames, quad)

    update: (dt) =>
        @vel = Vector(0, 0)

        if @blanketlength > 0
            @blanketlength -= dt
        else
            @blanket = false
            @blanketlength = 0

        if @blanket
            @hitboxes = {Rect(Vector(2, 4), Vector(72, 56))} --blanket hitbox
        else
            @hitboxes = {Rect(Vector(21, 24), Vector(37, 20))}

        if not @exploding
            -- Adjust velocity based on keys pressed
            @vel += Vector(0, -VEL) if Input\isDown(Input.MOVE_UP)
            @vel += Vector(0, VEL) if Input\isDown(Input.MOVE_DOWN)
            @vel += Vector(-VEL, 0) if Input\isDown(Input.MOVE_LEFT)
            @vel += Vector(VEL, 0) if Input\isDown(Input.MOVE_RIGHT)

            -- Normalize vector so top speed is VEL (diagonal is otherwise faster)
            @vel = @vel\norm() * VEL

            -- Update position
            @pos += dt * @vel

        -- Handle collision with walls
        -- TODO: Refactor
        width, height = love.graphics.getDimensions()

        if(@pos.x < 0 or @pos.x > (width - QUAD_SIZE) or
            @pos.y < 0 or @pos.y > (height - QUAD_SIZE))

            if not @collided
                @collided = true
                Audio\play(SFX_BOUNCE)
        else
            @collided = false

        @pos = @pos\restrict(0, width - QUAD_SIZE, 0, height - QUAD_SIZE)

        @time += dt --The time since the object was created
        @count += 1 --The number of times the update method has run

        if @exploding
            if @explosionStart == nil
                @explosionStart = @time
                Audio\play(SFX_EXPLODE)

            if @explosionStart
                if @explosionFrame == EXPLOSION_FRAMES
                    @screen\removeSprite(self)
                    @screen\changeScreen('you_lose')
                    @screen\reset()
                else
                    @explosionFrame = math.floor((@time - @explosionStart) / 0.1) + 1

                if @explosionFrame == 4
                    @visible = false

    draw: () =>
        super()

        -- Animate when moving
        if @vel != Vector(0,0)
            if @count % 4 == 0 and @current_quad <= FRAMES_PER_ANIM
                @current_quad += @animdir
                if @current_quad == 1 and @animdir == -1
                    @animdir = 1
                elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                    @animdir = -1
                    @current_quad = FRAMES_PER_ANIM
        else
            @current_quad = 1
            @animdir = 1

        if @visible and @blanket
            frame = IMG_BLANKET_FRAMES[math.floor(((@time * 100) % 101) / 25) + 1]
            dimensions = Vector(frame\getDimensions())
            love.graphics.setColor(255, 255, 255, 200)
            love.graphics.draw(frame, 0, 0, 0, 1, 1, (dimensions / 2)\vals())

        if @visible
            anim = @flameAnim[@current_quad]
            @drawQuad(IMG_WHALE, anim)

        if @exploding
            @drawQuad(IMG_WHALE, @explosionFrames[@explosionFrame])

        --for hitbox in *@hitboxes
        --    love.graphics.setColor(255, 0, 0, 100)
        --    position = hitbox.top_left - Vector(QUAD_SIZE/2, QUAD_SIZE/2)
        --    love.graphics.rectangle('fill', position.x, position.y, hitbox.dimensions.x, hitbox.dimensions.y)
