-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import IMG_ENEMY_11, SFX_CRASH, SFX_DULLBURST from require('Resources')


QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 8

class EnemyType11 extends Enemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)
        --@vel = Vector(-0.000000001,0)

        @attackcounter = 0
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(9, 10), Vector(46, 32))}

        @animdir = 1
        @current_quad = 1

        -- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2

    load: () =>
        super()

        @center = Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_11\getDimensions())
            table.insert(@destroyAnim, quad)

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt
        
        --Control firing shots here
        @attackcounter += dt
        if @attackcounter > 1 and @attackcounter < 2 and @hp > 0
            for i = 0, 4
                @explosionbomb(i)

            @attackcounter = 5
            Audio\play(SFX_DULLBURST)

        if @time > 6
            @vel = Vector(0, -1*(@time-6)*50)
            @attackcounter = 0 --disable shooting while moving

        --Following Deletes enemy
        if @time > 6 and (@pos.x > 864 or @pos.x < -64 or @pos.y < -64 or @pos.y > 664)
            @screen\removeSprite(self)
            @screen.enemyCount -= 1
            @addSpareBonus()

        if @exploding
            @screen\removeSprite(self)

    explosionbomb: (i) =>
        angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center/2)\angle()
        newbullet = EnemyShot(@pos + @center/2, Vector((100 + i*25)*math.cos(angleT), (100 + i*25)*math.sin(angleT)), EnemyShot.SHOT_GRAPHIC_BALLINV_GREEN, @screen)
        newbullet\load(@screen.resources)
        @screen\addSprite(newbullet)
        explode_co = coroutine.create(() ->
            while newbullet.pos.y < 600 and newbullet.pos.y > 0 
                coroutine.yield()
            for i = 0, 12
                angleT = math.random(200*math.pi)/100
                velT = math.random(150) + 150
                bullet = EnemyShot(newbullet.pos + newbullet.center/2, Vector(velT*math.cos(angleT), velT*math.sin(angleT)), EnemyShot.SHOT_GRAPHIC_DOT_AQUA, @screen)
                bullet\load(@screen.resources)
                @screen\addSprite(bullet)
        )
        table.insert(@screen.coroutines, explode_co)

    draw: () =>
        super()

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM
                
        -- Draw enemy at angle of travel
        anim = @destroyAnim[@current_quad]
        @drawQuad(IMG_ENEMY_11, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
