-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import IMG_ENEMY_10, SFX_SLICEWAVE from require('Resources')


QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 8

class EnemyType10 extends Enemy
    new: (pos, vel, finalx, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)
        --@vel = Vector(-0.000000001,0)

        @finalx = finalx --Destination X coordinate during movement

        @attackcounter = 0
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(9, 10), Vector(46, 32))}

        @animdir = 1
        @current_quad = 1

        -- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2

    load: () =>
        super()

        @center = Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_10\getDimensions())
            table.insert(@destroyAnim, quad)

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt
        
        --Control firing shots here
        @attackcounter += dt
        if @attackcounter > 1.9 and @hp > 0
            angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center/2)\angle()
            for i = 0, 11
                newbullet = EnemyShot(@pos + @center/2 + Vector(64*math.cos(angleT + math.rad(i*360/12)), 64*math.sin(angleT + math.rad(i*360/12))), Vector(200*math.cos(angleT), 200*math.sin(angleT)), EnemyShot.SHOT_GRAPHIC_BALLINV_DARKORANGE, @screen)
                newbullet\load(@screen.resources)
                @screen\addSprite(newbullet)
            for i = 0, 11
                newbullet = EnemyShot(@pos + @center/2 + Vector(64*math.cos(angleT + math.rad(i*360/12)), 64*math.sin(angleT + math.rad(i*360/12))), Vector(200*math.cos(angleT + math.rad(90)), 200*math.sin(angleT + math.rad(90))), EnemyShot.SHOT_GRAPHIC_DOT_MIDNIGHT, @screen)
                newbullet\load(@screen.resources)
                @screen\addSprite(newbullet)
            for i = 0, 11
                newbullet = EnemyShot(@pos + @center/2 + Vector(64*math.cos(angleT + math.rad(i*360/12)), 64*math.sin(angleT + math.rad(i*360/12))), Vector(200*math.cos(angleT - math.rad(90)), 200*math.sin(angleT - math.rad(90))), EnemyShot.SHOT_GRAPHIC_DOT_MIDNIGHT, @screen)
                newbullet\load(@screen.resources)
                @screen\addSprite(newbullet)

            @attackcounter = 0
            Audio\play(SFX_SLICEWAVE)

        if @time > 0 and @moving == 0
            @vel = @vel\norm()
            @moving = 1
            @movement_co = coroutine.create(
                (dest, newtime, factor, reset) ->
                    delta = @pos - dest
                    t_old = @time
                    t_elapsed = 0 --since start
                    while t_elapsed < newtime
                        t_elapsed = @time - t_old
                        delta = @pos - dest
                        -- one frame is 1/60 second. dt is supposed to be one frame. dt*60 is the scaling factor for lower FPS rates
                        @pos -= delta * factor * dt * 60
                        coroutine.yield()

                    if reset == 1
                        @pos = dest

                    @moving = 2 --disable movement
            )
            coroutine.resume(@movement_co, Vector(@finalx, @pos.y), 5, 0.02, 0)

        if @time > 0 and @moving == 1
            coroutine.resume(@movement_co)

        if @time > 6
            @vel = Vector(-1*(@time-6)*50, 0)
            @attackcounter = 0 --disable shooting while moving

        --Following Deletes enemy
        if @time > 6 and (@pos.x > 864 or @pos.x < -64 or @pos.y < -64 or @pos.y > 664)
            @screen\removeSprite(self)
            @screen.enemyCount -= 1
            @addSpareBonus()

        if @exploding
            @screen\removeSprite(self)

    draw: () =>
        super()

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM
                
        -- Draw enemy at angle of travel
        anim = @destroyAnim[@current_quad]
        @drawQuad(IMG_ENEMY_10, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
