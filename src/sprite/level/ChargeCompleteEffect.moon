-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

import IMG_CHARGECOMPLETE from require('Resources')

-- This is used in a number of minor effects

class ChargeCompleteEffect extends Sprite
    new: (pos = Vector(), @vel = Vector(), r = 255, g = 255, b = 255, screen) =>
        super(pos)

        @rgb = {r, g, b}
        @time = 0 -- relative to sprite's creation
        @alpha = 255
        @angle = 0
        @parentscreen = screen

        @layer = Layers.PARTICLE

    load: () =>
        super()

        @quad = love.graphics.newQuad(0, 0, 8, 8, IMG_CHARGECOMPLETE\getDimensions())

    update: (dt) =>
        -- Update velocity and position
        -- Update position
        @pos += dt * @vel

        @alpha = if @time < 0.5
                     255
                 elseif @time > 0.5 and @alpha > 0
                     255 - (@time - 0.5) * 255
                 else
                     @alpha

        if @alpha <= 0
            @parentscreen\removeSprite(self)

        @time += dt

    draw: () =>
        --Below handles translation of graphic
        super()

        love.graphics.setColor(@rgb[1], @rgb[2], @rgb[3], @alpha)
        love.graphics.setBlendMode('alpha')
        @drawQuad(IMG_CHARGECOMPLETE, @quad, @angle)

        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setBlendMode('alpha')
