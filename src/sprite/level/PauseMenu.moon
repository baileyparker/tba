-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite     = require('sprite/Sprite')
Vector     = require('math/Vector')
Rect       = require('math/Rect')

Layers = require('Layers')

import
    IMG_PAUSE_MENU
    from require('Resources')


class PauseMenu extends Sprite
    new: (@screen) =>
        dimensions = Vector(IMG_PAUSE_MENU\getDimensions())
        @center = dimensions / 2
        screen = Vector(love.graphics.getDimensions())
        super((screen - dimensions) / 2)
        @layer = Layers.OVERLAY

    update: (dt) =>
        super(dt)
        pos = Vector(love.mouse.getPosition()) - @pos

        if love.mouse.isDown('l')
            if Vector(83, 133) < pos and pos < Vector(200, 200)
                @screen\_pauseContinue()
            elseif Vector(241, 133) < pos and pos < Vector(343, 200)
                @screen\_pauseRestart()
            elseif Vector(378, 133) < pos and pos < Vector(462, 200)
                @screen\_pauseExit()


    draw: () =>
        super()
        @drawImage(IMG_PAUSE_MENU)
