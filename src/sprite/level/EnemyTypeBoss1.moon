-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
BossEnemy = require('sprite/level/BossEnemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import
    IMG_ENEMY_BOSS_1
    IMG_ENEMY_BOSS_1_SHIP
    SFX_DULLBURST
    SFX_SLICEWAVE
    SFX_DARKCHARGE
    SFX_SLASH
    SFX_ENEMYCHARGE
    from require('Resources')


QUAD_SIZEX      = 256
QUAD_SIZEY      = 256
FRAMES_PER_ANIM = 8
FRAMES_PER_ANIM2 = 13

class EnemyTypeBoss1 extends BossEnemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        @vel = Vector(-0.000000001,0)

        @attackcounter = 0
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(22, 105), Vector(212, 86)), Rect(Vector(44, 59), Vector(170, 46)), Rect(Vector(78, 28), Vector(104, 31))}


        @lifebarfillcolor = {64, 255, 64}

        @animdir = 1
        @animdir2 = 1
        @current_quad = 1
        @current_quad2 = 1

        -- time between animation frames
        @timeSinceLastFrame = 0 --ship
        @frameRate = 0.2 --ship
        @timeSinceLastFrame2 = 0 --boss
        @frameRate2 = 0.035 --boss

        @charging = false

        @dialoguestatus = 0

    load: () =>
        super()

        @center = Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)

        @shipAnim = {}
        @bossAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_BOSS_1_SHIP\getDimensions())
            table.insert(@shipAnim, quad)
        for i = 0, FRAMES_PER_ANIM2
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_BOSS_1\getDimensions())
            table.insert(@bossAnim, quad)

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt
        @timeSinceLastFrame2 += dt
        
        --Control fining shots here
        if @dialoguestatus > 0
            @attackcounter += dt

        if @attackcounter > 1 and @charging == false and @dialoguestatus > 0
            @charge()
            Audio\play(SFX_ENEMYCHARGE)
            @charging = true

        if @attackcounter > 2 and @hp > 200 and @hp <= 300 and @dialoguestatus > 0
            @attack_co = coroutine.create(() ->
                angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center)\angle() --2pi is due to Vector using a different coordinate system from Lua
                Audio\play(SFX_DARKCHARGE)
                for j = 0, 12
                    vel = 300
                    for i = -2, 2
                        newbullet = EnemyShot(@pos + @center, Vector(vel*math.cos(angleT + math.pi*2/12*i + math.pi/36 * math.sin(math.pi*2/12*j)), vel*math.sin(angleT + math.pi*2/12*i + math.pi/36 * math.sin(math.pi*2/12*j))), EnemyShot.SHOT_GRAPHIC_BALLINV_LAWNGREEN, @screen)
                        newbullet\load(@screen.resources)
                        @screen\addSprite(newbullet)
                        newbullet = EnemyShot(@pos + @center, Vector(vel*math.cos(angleT + math.pi*2/12*i - math.pi/36 * math.sin(math.pi*2/12*j)), vel*math.sin(angleT + math.pi*2/12*i - math.pi/36 * math.sin(math.pi*2/12*j))), EnemyShot.SHOT_GRAPHIC_BALLINV_LAWNGREEN, @screen)
                        newbullet\load(@screen.resources)
                        @screen\addSprite(newbullet)

                    for z = 1, 10 do
                        @attackcounter = 0
                        coroutine.yield()

                @charging = false
            )
            coroutine.resume(@attack_co)

        if @attackcounter > 2 and @hp > 100 and @hp <= 200 and @dialoguestatus > 0
            @attack_co = coroutine.create(() ->
                angleT = math.random(math.pi*2)
                for j = 0, 11
                    vel = 100
                    for i = 0, 8
                        newbullet = EnemyShot(@pos + @center, Vector(vel*math.cos(angleT), vel*math.sin(angleT)), EnemyShot.SHOT_GRAPHIC_DOT_LAWNGREEN + j, @screen)
                        newbullet\load(@screen.resources)
                        @screen\addSprite(newbullet)
                        angleT += math.pi*2/9
                        vel += 10

                    Audio\play(SFX_SLASH)
                    angleT += math.rad(23)
                    for z = 1, 10
                        @attackcounter = 0
                        coroutine.yield()

                @charging = false
            )
            coroutine.resume(@attack_co)

        if @attackcounter > 2 and @hp <= 100 and @dialoguestatus > 0
            @attack_co = coroutine.create(() ->
                angleT = math.random(math.pi*2)
                for j = 0, 2
                    vel = 200
                    for i = 0, 12
                        for k = -2, 2
                            newbullet = EnemyShot(@pos + @center, Vector((1.75-math.abs(k/4))*vel*math.cos(angleT+math.rad(k)), (1.75-math.abs(k/4))*vel*math.sin(angleT+math.rad(k))), EnemyShot.SHOT_GRAPHIC_BEAM_AQUA, @screen)
                            newbullet\load(@screen.resources)
                            @screen\addSprite(newbullet)

                        angleT += math.pi*2/12

                    Audio\play(SFX_SLICEWAVE)
                    angleT += math.rad(43)
                    for z = 1, 42
                        @attackcounter = 0
                        coroutine.yield()

                @charging = false
            )
            coroutine.resume(@attack_co)

        if @time > 0 and @moving == 0
            @vel = @vel\norm()
            @moving = 1
            @movement_co = coroutine.create(
                (dest, newtime, factor, reset) ->
                    delta = @pos - dest
                    t_old = @time
                    t_elapsed = 0 --since start
                    while t_elapsed < newtime do
                        t_elapsed = @time - t_old
                        delta = @pos - dest
                        -- one frame is 1/60 second. dt is supposed to be one frame. dt*60 is the scaling factor for lower FPS rates
                        @pos -= delta * factor * dt * 60
                        coroutine.yield()

                    if reset == 1
                        @pos = dest

                    @moving = 2
            )
            if @dialoguestatus == 0
                coroutine.resume(@movement_co, Vector(450, @pos.y), 10, 0.01, 0)
            else
                coroutine.resume(@movement_co, Vector(math.random(150) + 350, math.random(300) + 150 - 84), 10, 0.01, 0)

        if @time > 0 and @moving == 1
            coroutine.resume(@movement_co)

        if @moving >= 2 and @moving < 5
            @moving += dt

        if @moving >= 5
            @moving = 0 --allow moving again

        if @attack_co != nil
            coroutine.resume(@attack_co)

        if @exploding
            @screen\removeSprite(self)

    draw: () =>
        super()

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM

        if @timeSinceLastFrame2 > @frameRate2
            @timeSinceLastFrame2 = 0.0
            @current_quad2 += @animdir2

            if @current_quad2 == 1 and @animdir2 == -1
                @animdir2 = 1
            elseif @current_quad2 == (FRAMES_PER_ANIM2 + 1) and @animdir2 == 1
                @animdir2 = -1
                @current_quad2 = FRAMES_PER_ANIM2
                
        -- Draw enemy at angle of travel
        anim = @shipAnim[@current_quad]
        anim2 = @bossAnim[@current_quad2]
        @drawQuad(IMG_ENEMY_BOSS_1_SHIP, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
        @drawQuad(IMG_ENEMY_BOSS_1, anim2, 0, if @vel.x <= 0 then 1 else -1, 1)

        --START Lifebar
        love.graphics.origin()
        lifebarlength = 256
        love.graphics.setColor(64, 255, 64, 255)
        love.graphics.rectangle('line', 528, 16, lifebarlength, 16) --border

        love.graphics.setColor(64, 127 + 128*(1 - @hp/@maxhp), 64, 255)
        lifebarlength = 252 * (1 - @hp/@maxhp)
        love.graphics.rectangle('fill', 530, 18, lifebarlength, 12) --main

        love.graphics.setColor(255, 255, 255, 255)
        --END Lifebar
