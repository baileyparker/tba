-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')

Layers = require('Layers')

import FNT_BODY from require('Resources')


class ScoreText extends Sprite
    setText: (newText) =>
        @text = newText
        @pos = Vector(16, 9)
        size = Vector(FNT_BODY\getWidth(newText), FNT_BODY\getHeight())
        @center = size / 2

    draw: () =>
        super()

        love.graphics.setColor(215, 203, 198, 255)
        love.graphics.setFont(FNT_BODY)
        love.graphics.print(@text, 0, 0, 0, 1, 1, @center\vals())
