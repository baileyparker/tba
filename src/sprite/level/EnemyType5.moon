-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
EnemyShot = require('sprite/level/EnemyShot')

import IMG_ENEMY_5 from require('Resources')

--DO NOT USE THIS ENEMY AS A TEMPLATE PLEASE


QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 8

class EnemyType5 extends Enemy
    new: (pos, vel, screen, hitpoints, inputid) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)
        --@vel = Vector(-0.000000001,0)

        @id = inputid
        @pos_base = pos --The center of the circle

        @attackcounter = 0
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(9, 10), Vector(46, 32))}

        @animdir = 1
        @current_quad = 1

        -- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2
        
    load: () =>
        super()

        @center = Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_5\getDimensions())
            table.insert(@destroyAnim, quad)

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt
        
        @pos_base += dt * @vel --move the base

        --Control fining shots here
        @attackcounter += dt
        if @attackcounter > 0.3 and @hp > 0 and @pos.x > 196
            angleT = math.random(math.pi*2)
            newbullet = EnemyShot(@pos + @center/2, Vector(math.random(-50,50), math.random(-50,50)), EnemyShot.SHOT_GRAPHIC_DOT_CYAN, @screen)
            newbullet.accel = newbullet.vel\norm() * 75
            newbullet\load(@screen.resources)
            @screen\addSprite(newbullet)
            @attackcounter = 0

        --Movement involves ring of enemies
        @pos = @pos_base + Vector(90*math.cos(math.rad(@id*60) + @time), 90*math.sin(math.rad(@id*60) + @time))

        if @time > 15
            --@vel = Vector(-1*(@time-15)*20, 0)
            @attackcounter = 0 --disable shooting while moving

        --Following Deletes enemy
        if @time > 15 and (@pos.x > 864 or @pos.x < -64 or @pos.y < -64 or @pos.y > 664)
            @screen\removeSprite(self)
            @screen.enemyCount -= 1
            @addSpareBonus()

        if @exploding
            @screen\removeSprite(self)

    draw: () =>
        super()

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM

        -- Draw enemy at angle of travel
        anim = @destroyAnim[@current_quad]
        @drawQuad(IMG_ENEMY_5, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
