-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
BossEnemy = require('sprite/level/BossEnemy')
EnemyShot = require('sprite/level/EnemyShot')
EnemyLaser = require('sprite/level/EnemyLaser')
DelayLaser = require('sprite/level/DelayLaser')

import Audio from require('Services')
import
    IMG_WHALE_BAD
    SFX_DARKCHARGE
    SFX_LONGBURST
    SFX_ELEBURST
    SFX_SLICEWAVE
    SFX_SLASH
    SFX_ENEMYCHARGE
    SFX_THUNDER
    SFX_MECHA
    SFX_GENTLEWAVE
    FNT_BODY
    from require('Resources')

--ATTACK DOCUMENTATION
--Note: Calm and Anger reflects two-phased attacks where there are moments of hell and moments of recovery
--Note: Love and Hate reflects constant firing attacks
--ATTACK 1 (3000 - 2700): Calm and Anger "Eruption of Fiery Hatred" - Delay lasers are followed by flaming blasts from the ground
--ATTACK 2 (2700 - 2000): Love and Hate "Promised Love of the Twin Whales" - Two parametric orbs form and spiral inwards and outwards in an ellipse, depositing bullets that accelerate from 0 to x. 
--ATTACK 3 (2000 - 1600): Calm and Anger "Burial in Ten Cries of Hatred" - Rings of delay lasers coalesce into ten powerful lasers
--ATTACK 4 (1600 - 1250): Love and Hate "Trapped in Despair" - Ten lasers come from top and bottom and block off large parts of the screen. Spirals form around Willy and force circular dodging
--ATTACK 5 (1250 - 1000): Calm and Anger "Destroy the Love" - Cardioids are fired, expanding. Then they slow to a stop, and they are shattered brutally by lasers into fragments.
--ATTACK 6 (1000 - 0750): Love and Hate "Broken Heart" - Two parametric rings behind enemy move up and down using sine wave, firing bullets to the left. Boss fires a BoWaP with acceleration in a sine wave
--ATTACK 7 (0750 - 0500): Calm and Anger "Metaspear" - Multiple 2D arrays of metatable text smash into the left, then fall apart, only for Waily to cover the screen in lasers, forcing the player to dodge horizontally to avoid the falling fragments
--ATTACK 8 (0500 - 0000): Final Ignition "Shield of Reflected Inner Solitude" - A shield (seperate object) will toggle on and off. When on, all of Willy's shots will turn into enemy shots
--*NOTE: A player at full LOVE should run out at Attack 3-4

QUAD_SIZEX      = 383
QUAD_SIZEY      = 321
FRAMES_PER_ANIM = 1

class EnemyTypeBoss4 extends BossEnemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        @vel = Vector(-0.000000001,0)

        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(4, 130), Vector(318, 72)), Rect(Vector(20, 88), Vector(286, 160)), Rect(Vector(44, 56), Vector(240, 216)), Rect(Vector(78, 28), Vector(176, 270)), Rect(Vector(120, 6), Vector(88, 310))}

        @lifebarfillcolor = {255, 0, 96}

        @coroutines = {}

        @animdir = 1
        @current_quad = 1
        
        -- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2

        @charging = false

        @dialoguestatus = 0

        @killtime = 0 --used in debug

        @blankettime = 0 --used for flashing blanket

    load: () =>
        super()

        @center = Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_WHALE_BAD\getDimensions())
            table.insert(@destroyAnim, quad)

        @attack1()
        @attack2()
        @attack3()
        @attack4()
        @attack5()
        @attack6()
        @attack7()
        @attack8()

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt

        for i, cr in ipairs(@coroutines)
                coroutine.resume(cr, dt) if coroutine.status(cr) != "dead"

        if @time > 0 and @moving == 0
            @vel = @vel\norm()
            @moving = 1
            @movement_co = coroutine.create(
                (dest, newtime, factor, reset) ->
                    delta = @pos - dest
                    t_old = @time
                    t_elapsed = 0 --since start
                    while t_elapsed < newtime do
                        t_elapsed = @time - t_old
                        delta = @pos - dest
                        -- one frame is 1/60 second. dt is supposed to be one frame. dt*60 is the scaling factor for lower FPS rates
                        @pos -= delta * factor * dt * 60
                        coroutine.yield()

                    if reset == 1
                        @pos = dest

                    @moving = 2
            )
            if @dialoguestatus == 0
                coroutine.resume(@movement_co, Vector(450, @pos.y), 10, 0.01, 0)
            else
                coroutine.resume(@movement_co, Vector(math.random(150) + 400, math.random(300) + 150 - 84), 10, 0.01, 0)

        if @time > 0 and @moving == 1
            coroutine.resume(@movement_co)

        if @moving >= 2 and @moving < 5
            @moving += dt

        if @moving >= 5
            @moving = 0 --allow moving again

        --if @exploding
            --@screen\removeSprite(self)

        if love.keyboard.isDown('k') and @killtime < 0 --kill attack
            if @hp > 2700
                @hp = 2700
            elseif @hp > 2000
                @hp = 2000
            elseif @hp > 1600
                @hp = 1600
            elseif @hp > 1250
                @hp = 1250
            elseif @hp > 1000
                @hp = 1000
            elseif @hp > 750
                @hp = 750
            elseif @hp > 500
                @hp = 500
            elseif @hp > 1
                @hp = 1
            @killtime = 0.2 --prevent one click from going hafway down
        @killtime -= dt


    --While bullet has not entered screen, invulnerable
    setvulninbounds: (bullet) =>
        vuln_co = coroutine.create(() ->
            while bullet.pos.x < 0 or bullet.pos.x > 800 or bullet.pos.y < 0 or bullet.pos.y > 600
                coroutine.yield()
            bullet.undeletable = false
        )
        table.insert(@coroutines, vuln_co)

    attack1: () =>
        attack1_co = coroutine.create(() ->
            while @dialoguestatus == 0
                coroutine.yield()
            timer = @time
            while @time < timer + 1
                coroutine.yield()
            while @hp > 2700
                @charge()
                Audio\play(SFX_ENEMYCHARGE)
                timer = @time
                while @time < timer + 1
                    coroutine.yield()
                --MAIN BODY
                @attack1_groundburst()
                timer = @time
                while @time < timer + 5
                    coroutine.yield()
        )
        table.insert(@coroutines, attack1_co)

    attack1_groundburst: () =>
        basex = math.random(100)
        groundburst_co = coroutine.create(() ->
            timer = @time
            for i = 0, 9
                dl = DelayLaser(80 + i*100, 0, 80 + i*100, 600, 0.5, @screen)
                @screen\addSprite(dl)
            while @time < timer + 0.5
                coroutine.yield()
            for i = 0, 9
                for j = 0, 16
                    newbullet = EnemyShot(Vector(80 + i*100, 600), Vector(math.random(40) - 20, -1*math.random(300)-60), EnemyShot.SHOT_GRAPHIC_DOT_DARKORANGE, @screen)
                    newbullet.accel = Vector(0, math.random(60)+90)
                    newbullet\load(@screen.resources)
                    @screen\addSprite(newbullet)

            Audio\play(SFX_THUNDER)
            Audio\play(SFX_ELEBURST)
        )
        table.insert(@coroutines, groundburst_co)

    attack2: () =>
        attack2_co = coroutine.create(() ->
            while @hp > 2700
                coroutine.yield()
            timer = @time
            while @time < timer + 4 --wait for attack1 to clear away
                coroutine.yield()
            @charge()
            Audio\play(SFX_ENEMYCHARGE)
            timer = @time
            while @time < timer + 1
                coroutine.yield()
            --MAIN BODY
            @attack2_familiar(1, 1)
            @attack2_familiar(1, 2)
            @attack2_familiar(-1, 1)
            @attack2_familiar(-1, 2)
        )
        table.insert(@coroutines, attack2_co)

    attack2_familiar: (dir, id) =>
        timer = 0
        angle = id * math.pi
        rad = 0
        set1 = {}
        set2 = {}
        numinring = 6 --FPS dies if too high, but looks nicer
        for i = 1, numinring
            newbullet = EnemyShot(@pos + @center, Vector(0, 0), EnemyShot.SHOT_GRAPHIC_DOT_GREEN, @screen)
            newbullet\load(@screen.resources)
            newbullet.undeletable = true
            @screen\addSprite(newbullet)
            set1[i] = newbullet
            newbullet = EnemyShot(@pos + @center, Vector(0, 0), EnemyShot.SHOT_GRAPHIC_DOT_AQUA, @screen)
            newbullet\load(@screen.resources)
            newbullet.undeletable = true
            @screen\addSprite(newbullet)
            set2[i] = newbullet
        familiar_co = coroutine.create((dt) ->
            rad2 = 0
            attacktimer = 0
            while @hp > 2000
                if rad2 < 64
                    rad2 += dt * 30
                rad = 160 * math.sin(timer)
                angle += (math.pi / 63 * dir)

                dummy_vect = Vector(math.cos(angle), math.sin(angle))
                pos_familiar = @pos + @center + dir*rad*dummy_vect --center point of familiars

                for i = 1, numinring
                    fam_dx = rad2*math.cos(timer*1.5 + i*math.pi/(numinring/2))
                    fam_dy = rad2*math.sin(timer*3 + i*math.pi/(numinring/2))
                    set1[i].pos = pos_familiar + Vector(fam_dx, fam_dy)
                    set2[i].pos = pos_familiar + Vector(fam_dx, -1*fam_dy)

                if attacktimer > 0.2
                    newbullet = EnemyShot(pos_familiar, dummy_vect, EnemyShot.SHOT_GRAPHIC_BEAM_VIOLET, @screen)
                    newbullet\load(@screen.resources)
                    newbullet.accel = 64*dummy_vect
                    @screen\addSprite(newbullet)
                    newbullet = EnemyShot(pos_familiar, dummy_vect, EnemyShot.SHOT_GRAPHIC_BEAM_MIDNIGHT, @screen)
                    newbullet\load(@screen.resources)
                    newbullet.accel = 96*dummy_vect
                    @screen\addSprite(newbullet)
                    attacktimer = 0

                changeintime = coroutine.yield()
                timer += changeintime
                attacktimer += changeintime
            --Handle deletion of bullets here
            for i = 1, numinring
                @screen\bulletDeleteEffect(set1[i].pos)
                @screen\bulletDeleteEffect(set2[i].pos)
                @screen\removeSprite(set1[i])
                @screen\removeSprite(set2[i])
        )
        table.insert(@coroutines, familiar_co)

    attack3: () =>
        attack3_co = coroutine.create(() ->
            while @hp > 2000
                coroutine.yield()
            while @hp > 1600
                @charge()
                Audio\play(SFX_ENEMYCHARGE)
                timer = @time
                while @time < timer + 1
                    coroutine.yield()
                --MAIN BODY
                Audio\play(SFX_DARKCHARGE)
                for i = 0, 9
                    @attack3_laser()
                timer = @time
                while @time < timer + 0.75
                    coroutine.yield()
                Audio\play(SFX_SLASH)
                @attack3_ring()
                timer = @time
                while @time < timer + 2.5
                    coroutine.yield()
        )
        table.insert(@coroutines, attack3_co)

    attack3_laser: () =>
        basey = math.random(600)
        delayset = {}
        for i = 1, 5
            dl = DelayLaser(0, basey + 8, 800, basey + 8, 0.75, @screen) -- +8 is to offset the width of 16
            dl\load(@screen.resources)
            @screen\addSprite(dl)
            delayset[i] = dl
        timer = @time
        laserblast_co = coroutine.create(() ->
            while @time < timer + 0.75
                rad = 32 - 32*(@time - timer)/0.75
                for i = 1, 5
                    delayset[i].y1 = basey + 8 + rad*math.sin(math.pi*2/5 * i + @time * 5)
                    delayset[i].y2 = basey + 8 + rad*math.sin(math.pi*2/5 * i + @time * 5)
                coroutine.yield()
            --Spawn actual laser here
            laser = EnemyLaser(Vector(0, basey), 0, 800, 16, 2, 1, 0.5, @screen)
            laser\load(@screen.resources)
            @screen\addSprite(laser)
        )
        table.insert(@coroutines, laserblast_co)

    attack3_ring: () =>
        angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center)\angle() --2pi is due to Vector using a different coordinate system from Lua
        for i = 0, 23
            for j = 0, 6
                newbullet = EnemyShot(@pos + @center, Vector((150 + 50*j) * math.cos(angleT + i*math.pi*2/24), (150 + 50*j) * math.sin(angleT + i*math.pi*2/24)), EnemyShot.SHOT_GRAPHIC_BALL_ORANGE, @screen)
                newbullet\load(@screen.resources)
                @screen\addSprite(newbullet)

    attack4: () =>
        attack4_co = coroutine.create(() ->
            while @hp > 1600
                coroutine.yield()
            @charge()
            Audio\play(SFX_ENEMYCHARGE)
            timer = @time
            while @time < timer + 1
                coroutine.yield()
            @attack4_spiral()
            while @hp > 1250
                --MAIN BODY
                @attack4_laser()
                timer = @time
                while @time < timer + 1
                    coroutine.yield()
        )
        table.insert(@coroutines, attack4_co)

    attack4_laser: () =>
        timer = @time
        laserspawn_co = coroutine.create(() ->
            laser = EnemyLaser(Vector(0, 0), 0, 800, 16, math.pi, EnemyLaser.SHOT_GRAPHIC_LASER_MIDNIGHT, 0, @screen)
            laser\load(@screen.resources)
            @screen\addSprite(laser)
            laser2 = EnemyLaser(Vector(0, 600), 0, 800, 16, math.pi, EnemyLaser.SHOT_GRAPHIC_LASER_MIDNIGHT, 0, @screen)
            laser2\load(@screen.resources)
            @screen\addSprite(laser2)
            while @time < timer + math.pi
                dist = 150 * math.sin(@time - timer)
                laser.pos.y = dist
                laser2.pos.y = 600 - dist
                coroutine.yield()
        )
        table.insert(@coroutines, laserspawn_co)

    attack4_spiral: () =>
        angleT = math.pi/2
        timeelapsed = 0
        spiral_co = coroutine.create(() ->
            while @hp > 1250
                base = Vector(256, 300)
                shotlocation = base + 480*Vector(math.cos(angleT), math.sin(angleT))
                b1 = EnemyShot(shotlocation, 88*Vector(math.cos(angleT + math.pi), math.sin(angleT + math.pi)), EnemyShot.SHOT_GRAPHIC_PELLETINV_LIGHTRED, @screen)
                b1.undeletable = true
                b1\load(@screen.resources)
                @screen\addSprite(b1)
                @setvulninbounds(b1)
                shotlocation = base - 480*Vector(math.cos(angleT), math.sin(angleT))
                b2 = EnemyShot(shotlocation, 88*Vector(math.cos(angleT), math.sin(angleT)), EnemyShot.SHOT_GRAPHIC_PELLETINV_YELLOW, @screen)
                b2.undeletable = true
                b2\load(@screen.resources)
                @screen\addSprite(b2)
                @setvulninbounds(b2)

                if timeelapsed > 6
                    shotlocation = base + 480*Vector(math.cos(angleT + math.pi/2), math.sin(angleT + math.pi/2))
                    b3 = EnemyShot(shotlocation, 88*Vector(math.cos(angleT + math.pi + math.pi/2), math.sin(angleT + math.pi + math.pi/2)), EnemyShot.SHOT_GRAPHIC_PELLETINV_LIGHTGREEN, @screen)
                    b3.undeletable = true
                    b3\load(@screen.resources)
                    @screen\addSprite(b3)
                    @setvulninbounds(b3)
                    shotlocation = base - 480*Vector(math.cos(angleT + math.pi/2), math.sin(angleT + math.pi/2))
                    b4 = EnemyShot(shotlocation, 88*Vector(math.cos(angleT + math.pi/2), math.sin(angleT + math.pi/2)), EnemyShot.SHOT_GRAPHIC_PELLETINV_AQUA, @screen)
                    b4.undeletable = true
                    b4\load(@screen.resources)
                    @screen\addSprite(b4)
                    @setvulninbounds(b4)

                timer = @time
                while @time < timer + 0.3
                    timeelapsed += coroutine.yield()
                angleT = math.pi/2 + timeelapsed * 0.75
        )
        table.insert(@coroutines, spiral_co)

    attack5: () =>
        attack5_co = coroutine.create(() ->
            while @hp > 1250
                coroutine.yield()
            @screen\killBullets() --wipe bullets spawned by attack4
            @charge()
            Audio\play(SFX_ENEMYCHARGE)
            timer = @time
            while @time < timer + 1
                coroutine.yield()
            @attack4_spiral()
            while @hp > 1000
                --MAIN BODY
                for i = 0, 2
                    if @hp > 1000
                        @attack5_cardioid()
                        Audio\play(SFX_MECHA)
                    timer = @time
                    while @time < timer + 1
                        coroutine.yield()
                timer = @time
                while @time < timer + 3
                    coroutine.yield()
        )
        table.insert(@coroutines, attack5_co)

    attack5_cardioid: () =>
        angle = math.pi + math.random(3000)/1000 - 1.5
        set = {}
        rad = 0
        time = 0
        spd = math.random(250) / 200 + 0.25 --PER CALL TO YIELD
        pos = @pos + @center
        numinring = 15
        for i = 1, numinring
            newbullet = EnemyShot(@pos + @center, Vector(0, 0), EnemyShot.SHOT_GRAPHIC_BALL_CORAL, @screen)
            newbullet\load(@screen.resources)
            newbullet.undeletable = true
            @screen\addSprite(newbullet)
            set[i] = newbullet
        cardioid_co = coroutine.create(() ->
            while rad < 96
                pos += spd*Vector(math.cos(angle), math.sin(angle))
                for i = 1, numinring
                    ang = time + math.pi*2/numinring*i
                    set[i].pos = pos + Vector(rad*math.cos(ang)*(1-math.cos(ang)), rad*math.sin(ang)*(1-math.cos(ang)))
                time += coroutine.yield()
                if rad < 96
                    rad = time * 20
            rad = 96
            --Now prepare the laser
            if @hp > 1000
                @attack5_laser(pos)
            timer = @time
            while @time < timer + 2
                for i = 1, numinring
                    ang = time + math.pi*2/numinring*i
                    set[i].pos = pos + Vector(rad*math.cos(ang)*(1-math.cos(ang)), rad*math.sin(ang)*(1-math.cos(ang)))
                time += coroutine.yield()
            if @hp > 1000
                Audio\play(SFX_SLICEWAVE)
            for i = 1, numinring
                if @hp > 1000
                    for j = 0, 2
                        randspd = math.random(125) + 50
                        randangle = math.random(math.pi * 2000) / 1000
                        newbullet = EnemyShot(set[i].pos + set[i].center, randspd * Vector(math.cos(randangle), math.sin(randangle)), EnemyShot.SHOT_GRAPHIC_DOT_CORAL, @screen)
                        newbullet\load(@screen.resources)
                        @screen\addSprite(newbullet)
                @screen\removeSprite(set[i])

        )
        table.insert(@coroutines, cardioid_co)

    attack5_laser: (pos) =>
        laser = EnemyLaser(Vector(0, pos.y - 8), 0, 800, 32, 1.5, EnemyLaser.SHOT_GRAPHIC_LASER_CORAL, 0.5, @screen)
        laser\load(@screen.resources)
        @screen\addSprite(laser)
        Audio\play(SFX_DARKCHARGE)

    attack6: () =>
        attack6_co = coroutine.create(() ->
            while @hp > 1000
                coroutine.yield()
            @screen\killBullets() --wipe bullets spawned by attack5
            @charge()
            Audio\play(SFX_ENEMYCHARGE)
            timer = @time
            while @time < timer + 1
                coroutine.yield()
            @attack6_familiar(1)
            @attack6_familiar(-1)
            @attack6_bowap()
        )
        table.insert(@coroutines, attack6_co)

    attack6_familiar: (id) =>
        timer = 0
        dist = 0
        set1 = {}
        set2 = {}
        numinring = 6 --FPS dies if too high, but looks nicer
        for i = 1, numinring
            newbullet = EnemyShot(@pos + @center, Vector(0, 0), EnemyShot.SHOT_GRAPHIC_DOT_MAGENTA, @screen)
            newbullet\load(@screen.resources)
            newbullet.undeletable = true
            @screen\addSprite(newbullet)
            set1[i] = newbullet
            newbullet = EnemyShot(@pos + @center, Vector(0, 0), EnemyShot.SHOT_GRAPHIC_DOT_MIDNIGHT, @screen)
            newbullet\load(@screen.resources)
            newbullet.undeletable = true
            @screen\addSprite(newbullet)
            set2[i] = newbullet
        familiar_co = coroutine.create((dt) ->
            rad2 = 0
            attacktimer = 0
            while @hp > 750
                if rad2 < 64
                    rad2 += dt * 30
                dist = 240 * math.sin(timer * 3) * id

                pos_familiar = Vector(680, 300 + dist) --center point of familiars

                for i = 1, numinring
                    fam_dx = rad2*math.cos(timer*1.5 + i*math.pi/(numinring/2))
                    fam_dy = rad2*math.sin(timer*3 + i*math.pi/(numinring/2))
                    set1[i].pos = pos_familiar + Vector(fam_dx, fam_dy)
                    set2[i].pos = pos_familiar + Vector(fam_dx, -1*fam_dy)

                if attacktimer > 0.2
                    newbullet = EnemyShot(pos_familiar, Vector(-200, 0), EnemyShot.SHOT_GRAPHIC_BEAM_BLUE, @screen)
                    newbullet\load(@screen.resources)
                    newbullet.accel = Vector(-36, 0)
                    @screen\addSprite(newbullet)
                    newbullet = EnemyShot(pos_familiar, Vector(-200, 0), EnemyShot.SHOT_GRAPHIC_BEAM_CYAN, @screen)
                    newbullet\load(@screen.resources)
                    newbullet.accel = Vector(-72, 0)
                    @screen\addSprite(newbullet)
                    attacktimer = 0

                changeintime = coroutine.yield()
                timer += changeintime
                attacktimer += changeintime
            --Handle deletion of bullets here
            for i = 1, numinring
                @screen\bulletDeleteEffect(set1[i].pos)
                @screen\bulletDeleteEffect(set2[i].pos)
                @screen\removeSprite(set1[i])
                @screen\removeSprite(set2[i])
        )
        table.insert(@coroutines, familiar_co)

    attack6_bowap: () =>
        angleT = math.random(math.pi*2)
        currtime = 0
        bowap_co = coroutine.create(() ->
            while @hp > 750
                accelval = 50*math.cos(currtime * 5)*math.cos(currtime * 5)
                for i = 0, 4
                    newbullet = EnemyShot(@pos + @center, Vector(125*math.cos(angleT), 125*math.sin(angleT)), EnemyShot.SHOT_GRAPHIC_BALL_CORAL, @screen)
                    newbullet\load(@screen.resources)
                    newbullet.accel = Vector(accelval*math.cos(angleT), accelval*math.sin(angleT))
                    @screen\addSprite(newbullet)
                    angleT += math.pi * 2 / 5

                Audio\play(SFX_DULLBURST)
                angleT += math.cos(currtime)
                timer = @time
                while @time < timer + 0.2
                    currtime += coroutine.yield()
        )
        table.insert(@coroutines, bowap_co)

    attack7: () =>
        attack7_co = coroutine.create(() ->
            while @hp > 750
                coroutine.yield()
            @charge()
            Audio\play(SFX_ENEMYCHARGE)
            timer = @time
            while @time < timer + 1
                coroutine.yield()
            @attack7_metatable()
        )
        table.insert(@coroutines, attack7_co)

    attack7_metatable: () =>
        matrix = {} --5x53
        matrix[1] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1}
        matrix[2] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1}
        matrix[3] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 1}
        matrix[4] = {0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0}
        matrix[5] = {1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1}
        metatable_co = coroutine.create(() ->
            while @hp > 500
                basex = 800
                basey = math.random(200)

                --delay laser for warning
                dl = DelayLaser(0, basey + 30, 800, basey + 30, 0.5, @screen)
                dl\load(@screen.resources)
                @screen\addSprite(dl)

                timer = @time
                while @time < timer + 0.5
                    coroutine.yield()
                for i = 1, 5
                    for j = 1, 53
                        if matrix[i][j] == 1
                            newbullet = EnemyShot(Vector(basex + j*10, basey + i*10), Vector(0, 0), EnemyShot.SHOT_GRAPHIC_BALL_GRAY, @screen)
                            newbullet\load(@screen.resources)
                            newbullet.undeletable = true
                            @attack7_positioncommands(newbullet, i, j)
                            @screen\addSprite(newbullet)
                while basex > 0
                    basex -= (coroutine.yield() * 720)
                basex = 0
                Audio\play(SFX_THUNDER)
                Audio\play(SFX_ELEBURST)

                --fire lasers
                for k = 0, 12
                    if @hp > 500
                        laser = EnemyLaser(Vector(0, math.random(600)), 0, 800, 16, 0.5, EnemyLaser.SHOT_GRAPHIC_LASER_DEEPGREEN, 0.5, @screen)
                        laser\load(@screen.resources)
                        @screen\addSprite(laser)
                        Audio\play(SFX_SLASH)
                        timer = @time
                        while @time < timer + 0.25
                            coroutine.yield()

                timer = @time
                while @time < timer + 1
                    coroutine.yield()

        )
        table.insert(@coroutines, metatable_co)

    attack7_positioncommands: (blt, i, j) =>
        pos_co = coroutine.create(() ->
            basex = 800
            while basex > 0
                blt.pos.x = basex + j*10
                basex -= (coroutine.yield() * 720)
            blt.pos.x = j*10
            --bullet now scatters
            blt.vel = Vector(math.random(300), math.random(300) - 80)
            blt.accel = Vector(0, math.random(80) + 30)
            blt.undeletable = false
        )
        table.insert(@coroutines, pos_co)

    attack8: () =>
        attack8_co = coroutine.create(() ->
            while @hp > 500
                coroutine.yield()
            @screen\killBullets() --wipe bullets spawned by attack7
            @charge()
            Audio\play(SFX_ENEMYCHARGE)
            timer = @time
            while @time < timer + 1
                coroutine.yield()
            @attack8_shield()
        )
        table.insert(@coroutines, attack8_co)

    attack8_shield: () =>
        difficultyflag = 0 
        shield_co = coroutine.create(() ->
            while @hp > 0
                @blanket = false
                if difficultyflag == 0 and @hp < 300
                    difficultyflag = 1
                if difficultyflag == 1 and @hp < 150
                    difficultyflag = 2

                randoffset = math.random(3000)/9000
                for i = 0, 18
                    tempang = math.pi/19 * i + randoffset
                    for j = 0, (2 + difficultyflag*2)
                        graph = 0
                        if difficultyflag == 0
                            graph = EnemyShot.SHOT_GRAPHIC_BALLINV_DARKORANGE
                        elseif difficultyflag == 1
                            graph = EnemyShot.SHOT_GRAPHIC_BALLINV_LIGHTRED
                        elseif difficultyflag == 2
                            graph = EnemyShot.SHOT_GRAPHIC_BALL_ORANGE
                        if @hp > 0
                            bullet = EnemyShot(@pos + @center, Vector(0, 0), graph - 2, @screen)
                            bullet\load(@screen.resources)
                            @attack8_onoffmovement(bullet, math.pi*3/2 - tempang, j * 2) -- 2 is to stop overlap and increase difficulty.
                            @screen\addSprite(bullet)
                            bullet = EnemyShot(@pos + @center, Vector(0, 0), graph, @screen)
                            bullet\load(@screen.resources)
                            @attack8_onoffmovement(bullet, math.pi/2 + tempang + math.pi/38, j) --last math.pi/40 is offset
                            @screen\addSprite(bullet)
                    if @hp > 0
                        Audio\play(SFX_GENTLEWAVE)
                    timer = @time
                    while @time < timer + 0.1
                        coroutine.yield()

                if @hp > 0
                    @charge()
                    Audio\play(SFX_ENEMYCHARGE)
                timer = @time
                while @time < timer + 1
                    coroutine.yield()

                if @hp > 0
                    @blanket = true
                    @blankettime = 5

                timer = @time
                while @time < timer + 5
                    @blankettime -= coroutine.yield()
        )
        table.insert(@coroutines, shield_co)

    attack8_onoffmovement: (bullet, angle, id) =>
        speedchange_co = coroutine.create(() ->
            for i = 0, 8
                if i % 2 == 0
                    bullet.vel = (160 + id*32)*Vector(math.cos(angle), math.sin(angle))
                else
                    bullet.vel = Vector(0, 0)
                timer = @time
                while @time < timer + 0.75
                    coroutine.yield()
        )
        table.insert(@coroutines, speedchange_co) --this one has to be dealt with in the parent screen

    draw: () =>
        super()

        if @blanket
            alpha = 255
            if @blankettime < 0.5
                alpha = 128 + 128*math.cos(@blankettime * math.pi * 16)
            hpoffset = 1 - @hp/500
            love.graphics.setColor(128 + 32*math.cos(@time*4) + 90 * hpoffset, 96 + 32*math.cos(@time*5) + 90 * hpoffset, 96 + 32*math.sin(@time*3) + 90 * hpoffset, alpha)
            love.graphics.circle("fill", -28, 0, 172, 64)
            love.graphics.setColor(255, 255, 255, 255)

        --for hitbox in *@hitboxes
        --    love.graphics.setColor(255, 0, 0, 100)
        --    position = hitbox.top_left - Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        --    love.graphics.rectangle('fill', position.x, position.y, hitbox.dimensions.x, hitbox.dimensions.y)

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM

        @current_quad = 1 --override

        -- Draw enemy at angle of travel
        anim = @destroyAnim[@current_quad]
        @drawQuad(IMG_WHALE_BAD, anim, 0, if @vel.x <= 0 then 1 else -1, 1)

        --START Lifebar
        love.graphics.origin()
        lifebarlength = 256
        love.graphics.setColor(255, 64, 150, 255)
        love.graphics.rectangle('line', 528, 16, lifebarlength, 16) --border

        love.graphics.setColor(192+48*@hp/@maxhp, 16+64*@hp/@maxhp, 128+96*@hp/@maxhp, 255)
        lifebarlength = 252 * (1 - @hp/@maxhp)
        love.graphics.rectangle('fill', 530, 18, lifebarlength, 12) --main

        love.graphics.setColor(255, 255, 255, 255)
        --END Lifebar

        text = ""
        if @hp > 2700 and @dialoguestatus > 0
            text = "Calm and Anger: Eruption of Fiery Hatred"
        elseif @hp > 2000 and @dialoguestatus > 0
            text = "Love and Hate: Promised Love of the Twin Whales"
        elseif @hp > 1600 and @dialoguestatus > 0
            text = "Calm and Anger: Burial in Ten Cries of Hatred"
        elseif @hp > 1250 and @dialoguestatus > 0
            text = "Love and Hate: Trapped in Despair"
        elseif @hp > 1000 and @dialoguestatus > 0
            text = "Calm and Anger: Destroy the Love"
        elseif @hp > 750 and @dialoguestatus > 0
            text = "Love and Hate: Broken Heart"
        elseif @hp > 500 and @dialoguestatus > 0
            text = "Calm and Anger: Metaspear"
        elseif @hp > 0 and @dialoguestatus > 0
            text = "Final Ignition: Shield of Reflected Inner Solitude"

        love.graphics.setFont(FNT_BODY)
        love.graphics.printf(text, 184, 42, 600, "right")
