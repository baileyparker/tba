-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import IMG_ENEMY_6, SFX_GENTLEWAVE from require('Resources')

--This enemy moves in a sine wave


QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 8

class EnemyType6 extends Enemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)
        --@vel = Vector(-0.000000001,0)

        @pos_base = pos --The path around which it oscillates

        @attackcounter = 0
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(9, 10), Vector(46, 32))}

        @animdir = 1
        @current_quad = 1

        -- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2
        
    load: () =>
        super()

        @center = Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_6\getDimensions())
            table.insert(@destroyAnim, quad)

        Audio\play(SFX_GENTLEWAVE) --Only plays once

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt
        
        @pos_base += dt * @vel --move the base

        --Control fining shots here
        @attackcounter += dt
        if @attackcounter > 0.225 and @hp > 0 and @pos.x > 228
            @attack_pattern1(@time * -3)
            @attack_pattern2(@time * 3)

        --Movement involves ring of enemies
        @pos = @pos_base + Vector(0, 30*math.sin(@time*2))

        if @time > 15
            --@vel = Vector(-1*(@time-15)*20, 0)
            @attackcounter = 0 --disable shooting while moving

        --Following Deletes enemy
        if @time > 15 and (@pos.x > 864 or @pos.x < -64 or @pos.y < -64 or @pos.y > 664)
            @screen\removeSprite(self)
            @screen.enemyCount -= 1
            @addSpareBonus()

        if @exploding
            @screen\removeSprite(self)

    --Takes angle parameter in radians
    attack_pattern1: (angle) =>
        newbullet = EnemyShot(@pos + @center/2, Vector(120*math.cos(angle), 120*math.sin(angle)), EnemyShot.SHOT_GRAPHIC_BALL_LAWNGREEN, @screen)
        newbullet\load(@screen.resources)
        @screen\addSprite(newbullet)
        newbullet = EnemyShot(@pos + @center/2, Vector(120*math.cos(angle + math.pi), 120*math.sin(angle + math.pi)), EnemyShot.SHOT_GRAPHIC_BALL_LAWNGREEN, @screen)
        newbullet\load(@screen.resources)
        @screen\addSprite(newbullet)
        @attackcounter = 0

    --Takes angle parameter in radians
    attack_pattern2: (angle) =>
        newbullet = EnemyShot(@pos + @center/2, Vector(150*math.cos(angle - math.pi/2), 150*math.sin(angle - math.pi/2)), EnemyShot.SHOT_GRAPHIC_BALL_CYAN, @screen)
        newbullet\load(@screen.resources)
        @screen\addSprite(newbullet)
        newbullet = EnemyShot(@pos + @center/2, Vector(150*math.cos(angle + math.pi/2), 150*math.sin(angle + math.pi/2)), EnemyShot.SHOT_GRAPHIC_BALL_CYAN, @screen)
        newbullet\load(@screen.resources)
        @screen\addSprite(newbullet)
        @attackcounter = 0

    draw: () =>
        super()

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM

        -- Draw enemy at angle of travel
        anim = @destroyAnim[@current_quad]
        @drawQuad(IMG_ENEMY_6, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
