-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import IMG_ENEMY_12, SFX_GENTLEWAVE from require('Resources')

--This enemy moves in a sine wave


QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 8

class EnemyType12 extends Enemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)
        --@vel = Vector(-0.000000001,0)

        @pos_base = pos --The path around which it oscillates

        @attackcounter = 0
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(9, 10), Vector(46, 32))}

        @animdir = 1
        @current_quad = 1

        -- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2
        
    load: () =>
        super()

        @center = Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_12\getDimensions())
            table.insert(@destroyAnim, quad)

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt
        
        @pos_base += dt * @vel --move the base

        --Control fining shots here
        @attackcounter += dt
        if @attackcounter > 2 and @hp > 0
            @fireShot()

        if @time > 0 and @moving == 0
            @vel = @vel\norm()
            @moving = 1
            @movement_co = coroutine.create(
                (dest, newtime, factor, reset) ->
                    delta = @pos - dest
                    t_old = @time
                    t_elapsed = 0 --since start
                    while t_elapsed < newtime do
                        t_elapsed = @time - t_old
                        delta = @pos - dest
                        -- one frame is 1/60 second. dt is supposed to be one frame. dt*60 is the scaling factor for lower FPS rates
                        @pos -= delta * factor * dt * 60
                        coroutine.yield()

                    if reset == 1 then
                        @pos = dest

                    @moving = 2 --disable movement
            )
            coroutine.resume(@movement_co, Vector(650, @pos.y), 5, 0.02, 0)

        if @time > 0 and @moving == 1
            --print(coroutine.status(@movement_co))
            coroutine.resume(@movement_co)

        if @time > 8
            @vel = Vector(-1*(@time-8)*20, 0)
            @attackcounter = 0 --disable shooting while moving

        --Following Deletes enemy
        if @time > 8 and (@pos.x > 864 or @pos.x < -64 or @pos.y < -64 or @pos.y > 664)
            @screen\removeSprite(self)
            @screen.enemyCount -= 1
            @addSpareBonus()

        if @exploding
            @screen\removeSprite(self)

    --Takes angle parameter in radians
    fireShot: () =>
        angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center/2)\angle()
        Audio\play(SFX_GENTLEWAVE)
        for i = -2, 2
            newbullet = EnemyShot(@pos + @center/2, Vector(250*math.cos(angleT + math.rad(i*42)), 250*math.sin(angleT + math.rad(i*42))), EnemyShot.SHOT_GRAPHIC_BEAM_GREENYELLOW, @screen)
            newbullet\load(@screen.resources)
            @screen\addSprite(newbullet)
        for i = -2, 2
            newbullet = EnemyShot(@pos + @center/2, Vector(225*math.cos(angleT + math.rad(i*42)), 225*math.sin(angleT + math.rad(i*42))), EnemyShot.SHOT_GRAPHIC_BEAM_CYAN, @screen)
            newbullet\load(@screen.resources)
            @screen\addSprite(newbullet)
            @homingins(newbullet)
        for i = -2, 2
            newbullet = EnemyShot(@pos + @center/2, Vector(200*math.cos(angleT + math.rad(i*42)), 200*math.sin(angleT + math.rad(i*42))), EnemyShot.SHOT_GRAPHIC_BEAM_GREENYELLOW, @screen)
            newbullet\load(@screen.resources)
            @screen\addSprite(newbullet)
        for i = -2, 2
            newbullet = EnemyShot(@pos + @center/2, Vector(175*math.cos(angleT + math.rad(i*42)), 175*math.sin(angleT + math.rad(i*42))), EnemyShot.SHOT_GRAPHIC_BEAM_MAGENTA, @screen)
            newbullet\load(@screen.resources)
            @screen\addSprite(newbullet)
            @splayins(newbullet)
        @attackcounter = 0

    homingins: (newbullet) =>
        home_co = coroutine.create(() ->
            timei = @screen.controltime
            while @screen.controltime < timei + 1
                coroutine.yield()
            angleT = 2*math.pi - @getAngleToHeroPos(newbullet.pos + newbullet.center/2)\angle()
            newbullet.vel = Vector(225*math.cos(angleT), 225*math.sin(angleT))
            newbullet.accel = Vector(25*math.cos(angleT), 25*math.sin(angleT))
        )
        table.insert(@screen.coroutines, home_co)

    splayins: (newbullet) =>
        splay_co = coroutine.create(() ->
            timei = @screen.controltime
            while @screen.controltime < timei + 1
                coroutine.yield()
            angleT = 2*math.pi - @getAngleToHeroPos(newbullet.pos + newbullet.center/2)\angle()
            newbullet.vel = Vector(100*math.cos(angleT), 100*math.sin(angleT))
            newbullet.graphic = EnemyShot.SHOT_GRAPHIC_FLOWER_MIDNIGHT
            newbullet.setIntersection()
        )
        table.insert(@screen.coroutines, splay_co)

    draw: () =>
        super()

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM

        -- Draw enemy at angle of travel
        anim = @destroyAnim[@current_quad]
        @drawQuad(IMG_ENEMY_12, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
