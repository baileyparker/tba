-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

import
    IMG_DIALOGUE
    from require('Resources')

class DialogueManager extends Sprite
    new: (@state, imageid, @parentscreen, @deletetime = 2) =>
        super()
        @image = IMG_DIALOGUE[imageid]

        @layer = Layers.OVERLAY
        @center = Vector(0, 0) --so that ignored by sprite draw

        @_reposition()

        @time = 0

    update: (dt) =>
        @time += dt
        if @time > @deletetime
            @parentscreen\removeSprite(self)
        @_reposition()

    _reposition: () =>
        @pos = switch @state
            when 0
                @parentscreen.hero.pos - Vector(256, 256) + Vector(32, -32)
            when 1
                @parentscreen.boss.pos - Vector(256, 256) + Vector(-256, -32)

    draw: () =>
        super()
        @drawImage(@image)
