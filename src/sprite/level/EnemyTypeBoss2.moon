-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
BossEnemy = require('sprite/level/BossEnemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import
    IMG_ENEMY_BOSS_2
    IMG_ENEMY_BOSS_2_SHIP
    SFX_MOOSE
    SFX_DARKCHARGE
    SFX_LONGBURST
    SFX_ENEMYCHARGE
    from require('Resources')


QUAD_SIZEX      = 256
QUAD_SIZEY      = 256
FRAMES_PER_ANIM = 8
FRAMES_PER_ANIM2 = 8

class EnemyTypeBoss2 extends BossEnemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        @vel = Vector(-0.000000001,0)

        @attackcounter = 0
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(22, 105), Vector(212, 86)), Rect(Vector(44, 59), Vector(170, 46)), Rect(Vector(78, 28), Vector(104, 31))}

        @lifebarfillcolor = {255, 128, 64}

        @animdir = 1
        @current_quad = 1
        @animdir2 = 1
        @current_quad2 = 1
        
        -- time between animation frames
        @timeSinceLastFrame = 0 --ship
        @frameRate = 0.2 --ship
        @timeSinceLastFrame2 = 0 --boss
        @frameRate2 = 0.05 --boss

        @charging = false

        @dialoguestatus = 0

    load: () =>
        super()

        @center = Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)

        @shipAnim = {}
        @bossAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_BOSS_2_SHIP\getDimensions())
            table.insert(@shipAnim, quad)
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_BOSS_2\getDimensions())
            table.insert(@bossAnim, quad)

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt
        @timeSinceLastFrame2 += dt
        
        --Control fining shots here
        if @dialoguestatus > 0
            @attackcounter += dt

        if @attackcounter > 1 and @charging == false and @dialoguestatus > 0
            @charge()
            Audio\play(SFX_ENEMYCHARGE)
            @charging = true

        if @attackcounter > 2 and @hp > 200 and @hp <= 300 and @dialoguestatus > 0
            @attack_co = coroutine.create(() ->
                for j = 0, 6
                    angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center)\angle() --2pi is due to Vector using a different coordinate system from Lua
                    vel = 300
                    for i = -1, 1
                        for k = 2, 5
                            newbullet = EnemyShot(@pos + @center, Vector((30*k + vel)*math.cos(angleT + math.rad(i*18)), (30*k + vel)*math.sin(angleT + math.rad(i*18))), EnemyShot.SHOT_GRAPHIC_BALLINV_RED, @screen)
                            newbullet\load(@screen.resources)
                            @screen\addSprite(newbullet)

                    Audio\play(SFX_LONGBURST)
                    for z = 1, 15 do
                        @attackcounter = 0
                        coroutine.yield()

                @charging = false
            )
            coroutine.resume(@attack_co)

        if @attackcounter > 2 and @hp > 100 and @hp <= 200 and @dialoguestatus > 0
            @attack_co = coroutine.create(() ->
                angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center)\angle() --2pi is due to Vector using a different coordinate system from Lua
                Audio\play(SFX_DARKCHARGE)
                for j = 0, 17
                    vel = 350
                    rad = j*12
                    for i = 0, 3
                        newbullet = EnemyShot(@pos + @center + Vector(rad*math.cos(angleT + math.rad(i*360/3)), rad*math.sin(angleT + math.rad(i*360/3))), (vel-j*10)*@getAngleToHeroPos(@pos + @center), EnemyShot.SHOT_GRAPHIC_DOT_RED, @screen)
                        newbullet\load(@screen.resources)
                        @screen\addSprite(newbullet)

                    angleT += math.rad(53)
                    for z = 1, 6
                        @attackcounter = 0
                        coroutine.yield()

                @charging = false
            )
            coroutine.resume(@attack_co)

        if @attackcounter > 0.075 and @hp <= 100 and @hp > 0 and @dialoguestatus > 0
            rad = math.random(120)
            newbullet = EnemyShot(@pos + @center + Vector(rad*math.cos(math.random(math.pi*2)), rad*math.sin(math.random(math.pi*2))), Vector(math.random(200) - 100, math.random(200) - 100), EnemyShot.SHOT_GRAPHIC_FLOWER_RED, @screen)
            newbullet\load(@screen.resources)
            @screen\addSprite(newbullet)
            @attackcounter = 0

        if @hp <= 75 and @dialoguestatus > 0
            destpos = @screen.hero.pos + Vector(32, 32)
            delta = @pos + Vector(QUAD_SIZEX/2, QUAD_SIZEY/2) - destpos
            @pos -= delta * dt / 4
            @moving = -1 --disable normal movement

        if @moving == -1 and @dialoguestatus > 0
            Audio\play(SFX_MOOSE)

        if @time > 0 and @moving == 0 and @hp > 100
            @vel = @vel\norm()
            @moving = 1
            @movement_co = coroutine.create(
                (dest, newtime, factor, reset) ->
                    delta = @pos - dest
                    t_old = @time
                    t_elapsed = 0 --since start
                    while t_elapsed < newtime do
                        t_elapsed = @time - t_old
                        delta = @pos - dest
                        -- one frame is 1/60 second. dt is supposed to be one frame. dt*60 is the scaling factor for lower FPS rates
                        @pos -= delta * factor * dt * 60
                        coroutine.yield()

                    if reset == 1
                        @pos = dest

                    @moving = 2
            )
            if @dialoguestatus == 0
                coroutine.resume(@movement_co, Vector(450, @pos.y), 10, 0.01, 0)
            else
                coroutine.resume(@movement_co, Vector(math.random(150) + 350, math.random(300) + 150 - 84), 10, 0.01, 0)

        if @time > 0 and @moving == 1
            coroutine.resume(@movement_co)

        if @moving >= 2 and @moving < 5
            @moving += dt

        if @moving >= 5
            @moving = 0 --allow moving again

        if @attack_co != nil
            coroutine.resume(@attack_co)

        if @exploding
            @screen\removeSprite(self)

    draw: () =>
        super()

        --love.graphics.setColor(255, 0, 0, 50)
        --pos = self.hitboxes[1].top_left - Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        --love.graphics.rectangle('fill', pos.x, pos.y, self.hitboxes[1].dimensions.x, self.hitboxes[1].dimensions.y)
        --love.graphics.setColor(255, 0, 0, 50)
        --pos = self.hitboxes[2].top_left - Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        --love.graphics.rectangle('fill', pos.x, pos.y, self.hitboxes[2].dimensions.x, self.hitboxes[2].dimensions.y)
        --love.graphics.setColor(255, 0, 0, 50)
        --pos = self.hitboxes[3].top_left - Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        --love.graphics.rectangle('fill', pos.x, pos.y, self.hitboxes[3].dimensions.x, self.hitboxes[3].dimensions.y)

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM

        if @timeSinceLastFrame2 > @frameRate2
            @timeSinceLastFrame2 = 0.0
            @current_quad2 += @animdir2

            if @current_quad2 == 1 and @animdir2 == -1
                @animdir2 = 1
            elseif @current_quad2 == (FRAMES_PER_ANIM2 + 1) and @animdir2 == 1
                @animdir2 = -1
                @current_quad2 = FRAMES_PER_ANIM2

        -- Draw enemy at angle of travel
        anim = @shipAnim[@current_quad]
        anim2 = @bossAnim[@current_quad2]
        @drawQuad(IMG_ENEMY_BOSS_2_SHIP, anim, 0, if @vel.x <= 0 then 1 else -1, 1)
        @drawQuad(IMG_ENEMY_BOSS_2, anim2, 0, if @vel.x <= 0 then 1 else -1, 1)

        --START Lifebar
        love.graphics.origin()
        lifebarlength = 256
        love.graphics.setColor(255, 128, 64, 255)
        love.graphics.rectangle('line', 528, 16, lifebarlength, 16) --border

        love.graphics.setColor(127 + 128*(1 - @hp/@maxhp), 64 + 128*(1 - @hp/@maxhp), 64, 255)
        lifebarlength = 252 * (1 - @hp/@maxhp)
        love.graphics.rectangle('fill', 530, 18, lifebarlength, 12) --main

        love.graphics.setColor(255, 255, 255, 255)
        --END Lifebar
