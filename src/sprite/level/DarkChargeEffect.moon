-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

import IMG_DARKCHARGE from require('Resources')


class DarkChargeEffect extends Sprite
    new: (@angle = 0, @parentboss, @rad = 256, @dir = 1, screen) =>
        super(Vector(0,0))

        @vel = Vector(0,0)
        @time = 0 -- relative to sprite's creation
        @alpha = 255
        @parentscreen = screen
        @center = Vector(32, 32)

        @pos = @parentboss.pos + @parentboss.center + Vector(@rad * math.cos(@angle), @rad * math.sin(@angle)) - @center

        @layer = Layers.PARTICLE

    load: () =>
        super()

        @quad = love.graphics.newQuad(0, 0, 64, 64, IMG_DARKCHARGE\getDimensions())

    update: (dt) =>
        -- Update velocity and position
        -- Update position
        @pos = @parentboss.pos + @parentboss.center + Vector(@rad * math.cos(@angle), @rad * math.sin(@angle)) - @center

        @alpha = if @time < 0.5
                     @time * 255
                 elseif @time > 0.9
                     128 - (128 / 0.1) * (@time - 0.9)
                 else
                     @alpha

        if @time > 1
            @parentscreen\removeSprite(self)

        @time += dt
        @rad -= dt * @rad / (1 - @time)
        @angle += dt * 8 * @dir

    draw: () =>
        --Below handles translation of graphic
        super()

        love.graphics.setColor(255, 255, 255, @alpha)
        love.graphics.setBlendMode('alpha')
        @drawQuad(IMG_DARKCHARGE, @quad, @angle)

        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setBlendMode('alpha')
