-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

import IMG_LASER from require('Resources')

--AS OF THE TIME OF WRITING, ONLY HORIZONTAL LASERS WITH AN ANGLE OF 0 MAY BE USED. USE NEGATIVE LENGTH FOR LEFT DIRECTION. POSITION IS TOP LEFT

class EnemyLaser extends Sprite
    --Below are shot constants that can be used to refer to specific graphics.
    --As a note, there are exactly 21 colors per bullet type.

    @SHOT_GRAPHIC_LASER_GRAY             = 1
    @SHOT_GRAPHIC_LASER_RED              = 2
    @SHOT_GRAPHIC_LASER_DARKORANGE       = 3
    @SHOT_GRAPHIC_LASER_ORANGE           = 4
    @SHOT_GRAPHIC_LASER_YELLOW           = 5
    @SHOT_GRAPHIC_LASER_GREENYELLOW      = 6
    @SHOT_GRAPHIC_LASER_LAWNGREEN        = 7
    @SHOT_GRAPHIC_LASER_GREEN            = 8
    @SHOT_GRAPHIC_LASER_DEEPGREEN        = 9
    @SHOT_GRAPHIC_LASER_LIGHTGREEN       = 10
    @SHOT_GRAPHIC_LASER_AQUA             = 11
    @SHOT_GRAPHIC_LASER_CYAN             = 12
    @SHOT_GRAPHIC_LASER_BLUE             = 13
    @SHOT_GRAPHIC_LASER_DARKBLUE         = 14
    @SHOT_GRAPHIC_LASER_INDIGO           = 15
    @SHOT_GRAPHIC_LASER_MIDNIGHT         = 16
    @SHOT_GRAPHIC_LASER_VIOLET           = 17
    @SHOT_GRAPHIC_LASER_MAGENTA          = 18
    @SHOT_GRAPHIC_LASER_FUSCHIA          = 19
    @SHOT_GRAPHIC_LASER_CORAL            = 20
    @SHOT_GRAPHIC_LASER_LIGHTRED         = 21
    @SHOT_GRAPHIC_LASERINV_GRAY          = 22
    @SHOT_GRAPHIC_LASERINV_CYAN          = 23
    @SHOT_GRAPHIC_LASERINV_BLUE          = 24
    @SHOT_GRAPHIC_LASERINV_DARKBLUE      = 25
    @SHOT_GRAPHIC_LASERINV_INDIGO        = 26
    @SHOT_GRAPHIC_LASERINV_MIDNIGHT      = 27
    @SHOT_GRAPHIC_LASERINV_VIOLET        = 28
    @SHOT_GRAPHIC_LASERINV_MAGENTA       = 29
    @SHOT_GRAPHIC_LASERINV_FUSCHIA       = 30
    @SHOT_GRAPHIC_LASERINV_CORAL         = 31
    @SHOT_GRAPHIC_LASERINV_LIGHTRED      = 32
    @SHOT_GRAPHIC_LASERINV_RED           = 33
    @SHOT_GRAPHIC_LASERINV_DARKORANGE    = 34
    @SHOT_GRAPHIC_LASERINV_ORANGE        = 35
    @SHOT_GRAPHIC_LASERINV_YELLOW        = 36
    @SHOT_GRAPHIC_LASERINV_GREENYELLOW   = 37
    @SHOT_GRAPHIC_LASERINV_LAWNGREEN     = 38
    @SHOT_GRAPHIC_LASERINV_GREEN         = 39
    @SHOT_GRAPHIC_LASERINV_DEEPGREEN     = 40
    @SHOT_GRAPHIC_LASERINV_LIGHTGREEN    = 41
    @SHOT_GRAPHIC_LASERINV_AQUA          = 42

    -- Bullet fired by the player (sidescrolling section)
    new: (pos, @angle = 0, @length = 0, @width = 16, @deletetime = 0, @graphic = 1, @delaytime = 0, screen) =>
        super(pos)

        @accel = Vector(0, 0)
        @time = 0 -- relative to sprite's creation
        @parentscreen = screen
        @sprites = {}
        @vel = Vector(0, 0)

        @layer = Layers.ENEMYSHOT

    load: () =>
        super()

        -- The quads for the bullets in the shotsheet
        for j = 0, 1
            for i = 0, 21
                quadtoadd = love.graphics.newQuad(16 * i, 64*j, 16, 64, IMG_LASER\getDimensions())
                table.insert(@sprites, j*21 + i + 1, quadtoadd)

        -- The sprites are 16x64
        @center = Vector(@length / 2, @width / 2)

    update: (dt) =>
        -- Update velocity and position
        @vel += dt * @accel
        @pos += dt * @vel

        @time += dt

        -- Add hitbox
        if @time > @delaytime 
            @setIntersection()


        if @time > @delaytime + @deletetime
            @parentscreen\removeSprite(self)

    draw: () =>
        love.graphics.origin()
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.translate((@pos)\vals()) --Origin is now at @pos

        -- Draw EnemyShot at angle of travel
        angle = @angle - math.pi/2

        love.graphics.setBlendMode("additive")
        if @time < @delaytime
            love.graphics.draw(IMG_LASER, @sprites[@graphic], 0, @center.y, angle, 0.1, @length/64, 0, 0)
        else
            love.graphics.draw(IMG_LASER, @sprites[@graphic], 0, @width, angle, @width/16, @length/64, 0, 0)

        love.graphics.setBlendMode("alpha")

        --Hitboxes are based off of the @pos of the object
        --love.graphics.origin()
        --love.graphics.setColor(255, 255, 255, 255)
        --love.graphics.translate((@pos + @center)\vals())
        --for hitbox in *@hitboxes
        --    love.graphics.setColor(255, 0, 0, 100)
        --    position = hitbox.top_left - @center
        --    love.graphics.rectangle('fill', position.x, position.y, hitbox.dimensions.x, hitbox.dimensions.y)

        --SANITY CHECK. Line runs above laser
        --love.graphics.origin()
        --love.graphics.setColor(255, 255, 255, 255)
        --love.graphics.setLineWidth(1)
        --if @angle == 0 or @angle == math.pi
        --    love.graphics.line(@pos.x, @pos.y, @pos.x + @length, @pos.y)
        --elseif @angle == math.pi/2 or @angle == math.pi*3/2
        --    love.graphics.line(@pos.x, @pos.y, @pos.x, @pos.y + @length)

    setIntersection: (l, w) =>
        --First vector is top left of hitbox. Second vector is dementions
        @hitboxes = if @angle == 0 or @angle == math.pi
                        {Rect(Vector(1, 1), Vector(@length - 2, @width - 2))}
                    elseif @angle == math.pi/2 or @angle == math.pi*3/2
                        {Rect(Vector(1, 1), Vector(@length - 2, @width - 2))}
