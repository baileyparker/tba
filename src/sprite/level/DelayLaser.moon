-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')

Layers = require('Layers')

class DelayLaser extends Sprite
 
    new: (@x1, @y1, @x2, @y2, @deletetime = 0, @screen) =>
        @time = 0
        @layer = Layers.ENEMYSHOT

    load: () =>

    update: (dt) =>
        @time += dt
        if @time > @deletetime
            @screen\removeSprite(self)

    draw: () =>
        love.graphics.origin()
        love.graphics.setColor(255, 255, 255, 255)
        love.graphics.setLineWidth(1)
        love.graphics.line(@x1, @y1, @x2, @y2)