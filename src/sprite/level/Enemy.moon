-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Vector = require('math/Vector')
Rect   = require('math/Rect')
DarkChargeEffect = require('sprite/level/DarkChargeEffect')
BarFillEffect    = require('sprite/level/BarFillEffect')

Layers = require('Layers')

QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 8

class Enemy extends Sprite
    new: (pos = Vector(), @vel = Vector(), @screen, @hp = 0) =>
        super(pos)

        @accel = Vector(0,0)

        @current_quad = 1

        @maxhp = @hp
        @basesparelovebonus = 100 --Base love for sparing enemy
        @perhpsparelovebonus = 50

        @exploding = false

        @time = 0
        @explosionStart = nil

        @layer = Layers.ENEMY

        @blanket = false --only used by final boss

    update: (dt) =>
        super(dt)
        -- Update position
        @pos += dt * @vel
        @vel += dt * @accel

        -- Bounce off walls
        width, height = love.graphics.getDimensions()

        @time += dt

        if @exploding and @explosionStart == nil then
            @explosionStart = @time

    addSpareBonus: () =>
        if @maxhp == @hp
            @screen.love += @basesparelovebonus + @maxhp * @perhpsparelovebonus
            @screen.score += 75 + 10 * @maxhp

    --In an ideal world, this would actually get the correct angle.
    --However, pos is the top left corner of each sprite and bullets are never spawned at pos, so...
    --Yeah this doesn't work completely but it does the basic job.
    getAngleToHero: () =>
        (@screen.hero.pos + Vector(32, 32) - @pos)\norm()

    --This version takes the spawning position
    getAngleToHeroPos: (spawn) =>
        (@screen.hero.pos + Vector(32, 32) - spawn)\norm()

    addLifebarParticle: (screen) =>
        eff = BarFillEffect(Vector(534 + 252 * (1 - @hp/@maxhp), 24), @lifebarfillcolor[1], @lifebarfillcolor[2], @lifebarfillcolor[3], screen)
        eff\load(@resources)
        screen\addSprite(eff, false)

    charge: () =>
        for i = 0, 35 do
            dir = 1
            if i % 2 == 0
                dir = -1
            eff = DarkChargeEffect(i*math.rad(10*i), self, math.random(256), dir, @screen)
            eff\load(@screen.resources)
            @screen\addSprite(eff, false)

    draw: () =>
        super()
