-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite = require('sprite/Sprite')
Enemy  = require('sprite/level/Enemy')
Vector = require('math/Vector')
Rect   = require('math/Rect')

--This is a dummy class used for convenience

QUAD_SIZEX      = 64
QUAD_SIZEY      = 64
FRAMES_PER_ANIM = 10


class BossEnemy extends Enemy