-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Sprite    = require('sprite/Sprite')
Vector    = require('math/Vector')
Rect      = require('math/Rect')
Enemy     = require('sprite/level/Enemy')
BossEnemy = require('sprite/level/BossEnemy')
EnemyShot = require('sprite/level/EnemyShot')

import Audio from require('Services')
import
    IMG_ENEMY_BOSS_3_PSY
    IMG_ENEMY_BOSS_3_SHIP
    SFX_DARKCHARGE
    SFX_LONGBURST
    SFX_ENEMYCHARGE
    from require('Resources')


QUAD_SIZEX      = 256
QUAD_SIZEY      = 256
FRAMES_PER_ANIM = 8
PSY_FRAMES      = 192 -- most of these are dups (and were omitted), see load code
PSY_FRAME_WIDTH = 8
PSY_REAL_FRAMES = 43
PSY_FPS         = 30
PSY_DURATION    = PSY_FRAMES / PSY_FPS

class EnemyTypeBoss3 extends BossEnemy
    new: (pos, vel, screen, hitpoints) =>
        super(pos, vel, screen, hitpoints)

        --To force the center of the graphic to be at the specified pos
        @pos -= Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        @vel = Vector(-0.000000001,0)

        @attackcounter = 0
        @attackcounter2 = 0
        @moving = 0 --used for movement coroutine
        @hitboxes = {Rect(Vector(22, 105), Vector(212, 86)), Rect(Vector(44, 59), Vector(170, 46)), Rect(Vector(78, 28), Vector(104, 31))}

        @lifebarfillcolor = {64, 96, 128}

        @animdir = 1
        @current_quad = 1
        
        -- time between animation frames
        @timeSinceLastFrame = 0
        @frameRate = 0.2

        @charging = false

        @dialoguestatus = 0

    load: () =>
        super()

        @center = Vector(QUAD_SIZEX / 2, QUAD_SIZEY / 2)

        @destroyAnim = {}

        -- loads sprite quads into destroyAnim table
        for i = 0, FRAMES_PER_ANIM
            quad = love.graphics.newQuad(QUAD_SIZEX * i, 0, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_BOSS_3_SHIP\getDimensions())
            table.insert(@destroyAnim, quad)

        @psyQuad = {}
        for i = 0, (PSY_FRAMES - 1)
            actualFrame = 0
            if i <= 95
                actualFrame = i % 24
            elseif i <= 101 -- between 96 and 101 inclusive
                actualFrame = 24 + i - 96
            else
                actualFrame = 31 + (i + 6) % 12

            x = (actualFrame % PSY_FRAME_WIDTH) * QUAD_SIZEX
            y = math.floor(actualFrame / PSY_FRAME_WIDTH) * QUAD_SIZEY
            quad = love.graphics.newQuad(x, y, QUAD_SIZEX, QUAD_SIZEY, IMG_ENEMY_BOSS_3_PSY\getDimensions())
            table.insert(@psyQuad, quad)

    update: (dt) =>
        super(dt)

        @timeSinceLastFrame += dt
        
        --Control fining shots here
        if @dialoguestatus > 0
            @attackcounter += dt
            @attackcounter2 += dt

        if @attackcounter2 > 1 and @charging == false
            @charge()
            Audio\play(SFX_ENEMYCHARGE)
            @charging = true

        if @attackcounter > 0.125 and @hp > 450 and @hp <= 600 and @dialoguestatus > 0
            @attack1(@time*2, 0)
            @attackcounter = 0

        if @attackcounter2 > 2 and @hp > 300 and @hp <= 450 and @dialoguestatus > 0
            @attack_co = coroutine.create(() ->
                angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center)\angle() --2pi is due to Vector using a different coordinate system from Lua
                Audio\play(SFX_DARKCHARGE)
                for j = 0, 24
                    dest = math.random(600)
                    for i = 0, 5
                        newbullet = EnemyShot(Vector(800 + 32*math.cos(i*math.pi*2/6), dest + 32*math.sin(i*math.pi*2/6)), Vector(-250, 0), EnemyShot.SHOT_GRAPHIC_DOT_CORAL, @screen)
                        newbullet\load(@screen.resources)
                        @screen\addSprite(newbullet)

                    for z = 1, 10
                        @attackcounter2 = 0
                        coroutine.yield()

                @charging = false
            )
            coroutine.resume(@attack_co)

        if @attackcounter2 > 1 and @hp > 150 and @hp <= 300 and @dialoguestatus > 0
            @attack2(@time*2)
            @attack2a(@time*2, 0)
            @attackcounter2 = 0

        if @attackcounter2 > 1 and @hp <= 150  and @dialoguestatus > 0
            @attack2a(@time*2, 6)
            @attackcounter2 = 0

        if @attackcounter > 0.175 and @hp <= 150 and @dialoguestatus > 0
            @attack1(@time*2, -9)
            @attackcounter = 0

        if @time > 0 and @moving == 0
            @vel = @vel\norm()
            @moving = 1
            @movement_co = coroutine.create(
                (dest, newtime, factor, reset) ->
                    delta = @pos - dest
                    t_old = @time
                    t_elapsed = 0 --since start
                    while t_elapsed < newtime do
                        t_elapsed = @time - t_old
                        delta = @pos - dest
                        -- one frame is 1/60 second. dt is supposed to be one frame. dt*60 is the scaling factor for lower FPS rates
                        @pos -= delta * factor * dt * 60
                        coroutine.yield()

                    if reset == 1
                        @pos = dest

                    @moving = 2
            )
            if @dialoguestatus == 0
                coroutine.resume(@movement_co, Vector(450, @pos.y), 10, 0.01, 0)
            else
                coroutine.resume(@movement_co, Vector(math.random(150) + 350, math.random(300) + 150 - 84), 10, 0.01, 0)

        if @time > 0 and @moving == 1
            coroutine.resume(@movement_co)

        if @moving >= 2 and @moving < 5
            @moving += dt

        if @moving >= 5
            @moving = 0 --allow moving again

        if @attack_co != nil
            coroutine.resume(@attack_co)

        if @exploding
            @screen\removeSprite(self)

    attack1: (ang, graphoffset) =>
        for j = 0, 2
            for i = -1, 1
                newbullet = EnemyShot(@pos + @center, Vector((200 + math.abs(i)*50)*math.cos(ang + math.rad(i*1.5 + j*120)), (200 + math.abs(i)*50)*math.sin(ang + math.rad(i*1.5 + j*120))), EnemyShot.SHOT_GRAPHIC_DOT_AQUA + j + graphoffset, @screen)
                newbullet\load(@screen.resources)
                @screen\addSprite(newbullet)

    attack2: (ang) =>
        for i = 0, 11
            newbullet = EnemyShot(@pos + @center + Vector(96*math.cos(ang), 96*math.sin(ang)), Vector(150 * math.cos(ang), 150 * math.sin(ang)), EnemyShot.SHOT_GRAPHIC_FLOWER_CYAN, @screen)
            newbullet\load(@screen.resources)
            @screen\addSprite(newbullet)
            ang += math.pi*2/12

    attack2a: (ang, graphoffset) =>
        Audio\play(SFX_LONGBURST)
        ang += math.pi*2/24
        for i = 0, 11
            angleT = 2*math.pi - @getAngleToHeroPos(@pos + @center + Vector(96*math.cos(ang), 96*math.sin(ang)))\angle()
            newbullet = EnemyShot(@pos + @center + Vector(64*math.cos(ang), 64*math.sin(ang)), Vector(250 * math.cos(angleT), 250 * math.sin(angleT)), EnemyShot.SHOT_GRAPHIC_PELLET_LIGHTGREEN + graphoffset, @screen)
            newbullet\load(@screen.resources)
            @screen\addSprite(newbullet)
            ang += math.pi*2/12

    draw: () =>
        super()

        --love.graphics.setColor(255, 0, 0, 50)
        --pos = self.hitboxes[1].top_left - Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        --love.graphics.rectangle('fill', pos.x, pos.y, self.hitboxes[1].dimensions.x, self.hitboxes[1].dimensions.y)
        --love.graphics.setColor(255, 0, 0, 50)
        --pos = self.hitboxes[2].top_left - Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        --love.graphics.rectangle('fill', pos.x, pos.y, self.hitboxes[2].dimensions.x, self.hitboxes[2].dimensions.y)
        --love.graphics.setColor(255, 0, 0, 50)
        --pos = self.hitboxes[3].top_left - Vector(QUAD_SIZEX/2, QUAD_SIZEY/2)
        --love.graphics.rectangle('fill', pos.x, pos.y, self.hitboxes[3].dimensions.x, self.hitboxes[3].dimensions.y)

        if @timeSinceLastFrame > @frameRate
            @timeSinceLastFrame = 0.0
            @current_quad += @animdir

            if @current_quad == 1 and @animdir == -1
                @animdir = 1
            elseif @current_quad == (FRAMES_PER_ANIM + 1) and @animdir == 1
                @animdir = -1
                @current_quad = FRAMES_PER_ANIM

        psy = @psyQuad[math.floor((@time % PSY_DURATION) / PSY_DURATION * PSY_FRAMES) + 1]
        love.graphics.draw(IMG_ENEMY_BOSS_3_PSY, psy, -84, -100, 0, if @vel.x <= 0 then 0.65 else -0.65, 0.65, @center.x, @center.y)

        -- Draw enemy at angle of travel
        anim = @destroyAnim[@current_quad]
        @drawQuad(IMG_ENEMY_BOSS_3_SHIP, anim, 0, if @vel.x <= 0 then 1 else -1, 1)

        --START Lifebar
        love.graphics.origin()
        lifebarlength = 256
        love.graphics.setColor(0, 32, 32, 255)
        love.graphics.rectangle('line', 528, 16, lifebarlength, 16) --border

        love.graphics.setColor(16+48*@hp/@maxhp, 16+64*@hp/@maxhp, 16+64*@hp/@maxhp, 255)
        lifebarlength = 252 * (1 - @hp/@maxhp)
        love.graphics.rectangle('fill', 530, 18, lifebarlength, 12) --main

        love.graphics.setColor(255, 255, 255, 255)
        --END Lifebar
