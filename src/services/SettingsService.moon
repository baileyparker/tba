-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

JSON = require('lib/JSON')

-- JSON will reencode tables as objects/arrays appropriately
JSON.strictTypes = true


SETTINGS_FILE = 'settings.json'
SETTINGS_VERSION = '1.0.0'

DEFAULT_BRIGHTNESS = 0.5
DEFAULT_CONTRAST = 0.5
DEFAULT_MASTER_VOLUME = 1
DEFAULT_MUSIC_VOLUME = 1
DEFAULT_SFX_VOLUME = 1


obj = (o) -> JSON\newObject(o)


class SettingsService
    new: (@audioService) =>
        @brightness = DEFAULT_BRIGHTNESS
        @contrast   = DEFAULT_CONTRAST

        @masterVolume = DEFAULT_MASTER_VOLUME
        @musicVolume  = DEFAULT_MUSIC_VOLUME
        @sfxVolume    = DEFAULT_SFX_VOLUME

    reset: () =>
        @setBrightness(DEFAULT_BRIGHTNESS)
        @setContrast(DEFAULT_CONTRAST)

        @setMasterVolume(DEFAULT_MASTER_VOLUME)
        @setMusicVolume(DEFAULT_MUSIC_VOLUME)
        @setSfxVolume(DEFAULT_SFX_VOLUME)

    load: () =>
        contents = love.filesystem.read(SETTINGS_FILE)

        if contents
            settings = JSON\decode(contents)

            if settings.version == SETTINGS_VERSION
                config = settings.settings

                if config.video
                    @setBrightness(config.video.brightness or @brightness)
                    @setContrast(config.video.contrast or @contrast)

                if config.audio
                    @setMasterVolume(config.audio.master_volume or @masterVolume)
                    @setMusicVolume(config.audio.music_volume or @musicVolume)
                    @setSfxVolume(config.audio.sfx_volume or @sfxVolume)

                @dirty = false
                true

        @dirty = true
        false

    setBrightness: (@brightness) =>
        @dirty = true
        @game\setBrightness(@brightness)

    setContrast: (@contrast) =>
        @dirty = true
        @game\setContrast(@contrast)

    setMasterVolume: (@masterVolume) =>
        @dirty = true
        @audioService\setMasterVolume(@masterVolume)

    setMusicVolume: (@musicVolume) =>
        @dirty = true
        @audioService\setMusicVolume(@musicVolume)

    setSfxVolume: (@sfxVolume) =>
        @dirty = true
        @audioService\setSfxVolume(@sfxVolume)

    save: () =>
        unless @dirty
            return

        contents = JSON\encode(obj({
            version: SETTINGS_VERSION
            settings: obj({
                video: obj({
                    brightness: @brightness
                    contrast:   @contrast
                })
                audio: obj({
                    master_volume: @masterVolume
                    music_volume:  @musicVolume
                    sfx_volume:    @sfxVolume
                })
            })
        }))

        if contents
            @dirty = not love.filesystem.write(SETTINGS_FILE, contents)
            not @dirty
        else
            false
