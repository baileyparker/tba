-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)


STICK_EPSILON = 0.1

BTN_A     = 'a'             -- PS4 X
BTN_B     = 'b'             -- PS4 O
BTN_X     = 'x'             -- PS4 Square
BTN_Y     = 'y'             -- PS4 Triangle
BTN_BACK  = 'back'
BTN_GUIDE = 'guide'
BTN_START = 'start'
BUMPER_L  = 'leftshoulder'
BUMPER_R  = 'rightshoulder'
DUP       = 'dpup'
DDOWN     = 'dpdown'
DLEFT     = 'dpleft'
DRIGHT    = 'dpright'

AXIS_LS_X = 'leftx'
AXIS_LS_Y = 'lefty'
AXIS_RS_X = 'rightx'
AXIS_RS_Y = 'righty'

AXIS_LT = 'triggerleft'
AXIS_RT = 'triggerright'


keyDown = (...) -> love.keyboard.isDown(...)
isJoystick = (js) -> js and js\isConnected() and js\isGamepad()
joystickDown = (js, ...) -> isJoystick(js) and js\isGamepadDown(...)
isAxis = (js, axis, cmp) -> isJoystick(js) and cmp(js\getGamepadAxis(axis))
isAxisNeg = (js, axis) -> isAxis(js, axis, (v) -> v < -STICK_EPSILON)
isAxisPos = (js, axis) -> isAxis(js, axis, (v) -> v >  STICK_EPSILON)


class InputService
    new: () =>
        @mouseVisible = false

        @MOVE_UP    = (js) -> keyDown('up', 'w')                   or isAxisNeg(js, AXIS_LS_Y)
        @MOVE_DOWN  = (js) -> keyDown('down', 's')                 or isAxisPos(js, AXIS_LS_Y)
        @MOVE_LEFT  = (js) -> keyDown('left', 'a')                 or isAxisNeg(js, AXIS_LS_X)
        @MOVE_RIGHT = (js) -> keyDown('right', 'd')                or isAxisPos(js, AXIS_LS_X)
        @FIRE       = (js) -> keyDown(' ')                         or isAxisPos(js, AXIS_RT)
        @POWERUP    = (js) -> keyDown('f')                         or joystickDown(js, BUMPER_L)
        @NAV_UP     = (js) -> keyDown('up', 'w')                   or joystickDown(js, DUP)
        @NAV_DOWN   = (js) -> keyDown('down', 's')                 or joystickDown(js, DDOWN)
        @NAV_LEFT   = (js) -> keyDown('left', 'a')                 or joystickDown(js, DLEFT)
        @NAV_RIGHT  = (js) -> keyDown('right', 'd')                or joystickDown(js, DRIGHT)
        @NAV_SELECT = (js) -> keyDown('return')                    or joystickDown(js, BTN_A)
        @NAV_SKIP   = (js) -> keyDown(' ')                         or joystickDown(js, BTN_B)
        @NAV_BACK   = (js) -> keyDown('backspace', 'escape', 'up') or joystickDown(js, BTN_B)
        @NAV_PAUSE  = (js) -> keyDown('escape')                    or joystickDown(js, BTN_BACK)

    onMouseVisibility: (@mouseVisible) =>
        -- pass

    isMouseVisible: () =>
        @mouseVisible

    setJoystick: (@joystick) =>
        -- pass

    isDown: (inputType) =>
        inputType and inputType(@joystick)
