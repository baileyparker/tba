-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

JSON = require 'lib/JSON'

-- JSON will reencode tables as objects/arrays appropriately
JSON.strictTypes = true


HIGHSCORES_FILE = 'highscores.json'
HIGHSCORES_VERSION = '1.0.0'
HIGHSCORES_NUM = 10

class HighScoreService
    new: () =>
        @highscores = JSON\newArray()
        @_read()

    _read: () =>
        contents = love.filesystem.read(HIGHSCORES_FILE)

        if contents
            scores = JSON\decode(contents)

            if scores.version == HIGHSCORES_VERSION
                @highscores = scores.highscores
                true
        false

    _write: () =>
        contents = JSON\encode(JSON\newObject({
            version: HIGHSCORES_VERSION,
            highscores: @highscores
        }))

        if contents
            love.filesystem.write(HIGHSCORES_FILE, contents)
        else
            false

    size: () =>
        #@highscores

    get: () =>
        i, len = 1, math.min(#@highscores, HIGHSCORES_NUM)

        () ->
            if i <= len
                entry = @highscores[i]
                i += 1
                return entry.initials, entry.score

    isHighscore: (score) =>
        @_getInsertionIndex(score) <= HIGHSCORES_NUM

    addScore: (initials, score) =>
        insertIndex = @_getInsertionIndex(score)
        entry = @_makeScoreEntry(initials, score)
        table.insert(@highscores, insertIndex, entry)

        @_write()

    _getInsertionIndex: (score) =>
        -- standard bisect algorithm modified for Lua's 1-indexed tables
        low, high, mid = 1, #@highscores, 1

        while low <= high
            mid = math.floor((low + high) / 2)

            if score > @highscores[mid].score
                high = mid - 1
            else
                -- If we are returning mid, since score is greater
                -- we want to insert it in the next location (mid + 1)
                mid += 1
                low = mid

        mid

    _makeScoreEntry: (initials, score) =>
        JSON\newObject({:initials, :score})
