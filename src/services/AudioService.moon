-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Set         = require('util/Set')
SoundEffect = require('audio/SoundEffect')
Music       = require('audio/Music')


class AudioService
    new: () =>
        @playingMusic = Set()

        -- Set defaults
        @setMasterVolume(1)
        @setMusicVolume(1)
        @setSfxVolume(1)

    setMasterVolume: (@masterVolume) =>
        love.audio.setVolume(@masterVolume)

    setMusicVolume: (@musicVolume) =>
        -- Music is generally more long-lived
        -- so we'll update the volume of all playing music
        for music in *@playingMusic\toList()
            if music\isPlaying()
                music\setVolume(@musicVolume)
            else
                -- Prune the set of non-playing music
                @playingMusic\remove(music)

    setSfxVolume: (@sfxVolume) =>
        -- SFX are ephemeral and happen very frequently, so
        -- keeping track of which ones are playing would likely
        -- be inefficient and they'll complete before the user
        -- notices the SFX volume change. For this reason,
        -- we choose not to set the volume of all playing SFX

    isPlaying: (audio) =>
        audio\isPlaying()

    play: (audio) =>
        if is_a(audio, SoundEffect)
            audio\setVolume(@sfxVolume)
            audio\play()
        elseif is_a(audio, Music)
            audio\setVolume(@musicVolume)
            audio\play()
            @playingMusic\add(audio)

    isPaused: (audio) =>
        audio\isPaused()

    pause: (audio) =>
        audio\pause()

    resume: (audio) =>
        audio\resume()

    stop: (audio) =>
        if is_a(audio, SoundEffect)
            audio\stop()
        elseif is_a(audio, Music)
            audio\stop()
            @playingMusic\remove(audio)
