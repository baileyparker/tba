-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

Campaign = require('campaign/Campaign')
JSON     = require('lib/JSON')

-- JSON will reencode tables as objects/arrays appropriately
JSON.strictTypes = true


NUM_LEVELS = 4

CAMPAIGNS_FILE = 'campaigns.json'
CAMPAIGNS_VERSION = '1.0.0'


obj = (o) -> JSON\newObject(o)
arr = (a) -> JSON\newArray(a)


class CampaignService
    new: () =>
        @campaigns = [Campaign(self) for i = 1, 3]
        @activeCampaign = nil
        @load()

    load: () =>
        contents = love.filesystem.read(CAMPAIGNS_FILE)

        if contents
            campaigns = JSON\decode(contents)

            if campaigns.version == CAMPAIGNS_VERSION and campaigns.campaigns
                for i, campaign in ipairs campaigns.campaigns
                    if i <= #@campaigns
                        @campaigns[i]\updateFromFile(campaign)
                true
        false

    get: (i) =>
        if i <= #@campaigns
            @campaigns[i]

    setActive: (@activeCampaign) =>
        @activeCampaign\use()

    saveProgress: (@completedLevel) =>
        if @activeCampaign
            @activeCampaign\setLevel(math.min(NUM_LEVELS, @completedLevel + 1))

    save: () =>
        contents = JSON\encode(obj({
            version: CAMPAIGNS_VERSION
            campaigns: arr([obj(c\toData()) for c in *@campaigns])
        }))

        if contents
            love.filesystem.write(CAMPAIGNS_FILE, contents)
        else
            false
