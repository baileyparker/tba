-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutle1@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

import newShader from love.graphics


SHADER_DIR = 'shaders/'

loadShader = (path) -> newShader(SHADER_DIR .. path)


{
    ALPHA_MASK: loadShader('alpha_mask.glsl')
    VIDEO_FX: loadShader('video_fx.glsl')
}
