-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

-- If we are in a development environment, require moonscript so that
-- we don't have to recompile
-- Use pcall to prevent errors in non-dev environment
pcall(function() require('moonscript') end)

-- Boostrap game
require('bootstrap')
