-- Bailey Parker (bailey@jhu.edu - bparke20)
-- Andrew Fan (afan5@jhu.edu)
-- Matt Beutler (mbeutler@jhu.edu)
-- Patrick Castaldi (pcastal1@jhu.edu)

SettingsService  = require('services/SettingsService')
AudioService     = require('services/AudioService')
HighScoreService = require('services/HighScoreService')
InputService     = require('services/InputService')
CampaignService  = require('services/CampaignService')


audio = AudioService()

{
    Settings: SettingsService(audio)
    Audio: audio
    HighScores: HighScoreService()
    Input: InputService()
    Campaign: CampaignService()
}
