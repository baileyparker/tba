require('moonscript')
local lint = require('linter')
local lfs = require('lfs')

function love.run()
    local width, height = love.window.getDesktopDimensions()
    love.window.setMode(100, 100, {x = width, y = height})

    if (#arg < 3) then
        print('Usage: lint files..')
        return(1)
    else
        local retval = 0

        for i = 3, #arg do
            local code = lint(arg[2] .. '/' .. arg[i], arg[i], #arg == 3)

            if (code ~= 0) then
                retval = code
            end
        end

        return retval
    end
end
