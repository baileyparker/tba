import concat from table


print_stderr = (str) ->
    io.stderr\write(str)

read_file = (path) ->
    f, errstr, errcode = io.open(path, 'r')

    if not f
        print_stderr("Failed to open #{path}: #{errstr} (#{errcode})\n")
        return nil

    f\read('*a')

split = (str, sep) ->
    fields = {}
    pattern = string.format('([^%s]+)', sep)
    str\gsub(pattern, (c) -> fields[#fields + 1] = c)
    fields

lines = (str) ->
    split(str, '\n')

indent = (str) ->
    concat(["\t#{v}" for _, v in ipairs(lines(str))], '\n')

lint = (path, relativePath, single=true) ->
    code = read_file(path)

    if not code
        return 1

    success, result = pcall(() -> love.graphics.newShader(code))

    if not success
        -- Remove first line (which always just says Compilation error)
        errors = concat([v for v in *lines(result)[2,]], '\n')

        if single
            print_stderr("#{errors}\n")
        else
            print_stderr("#{relativePath}:\n#{indent(errors)}\n")

        return 1

    warnings = result\getWarnings()

    if #warnings > 0
        if single
            print_stderr("#{warnings}\n")
        else
            print_stderr("#{relativePath}:\n\t#{indent(warnings)}\n")

        return 1

    0

lint
