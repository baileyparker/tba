# TBA (To Be Announced) Productions present: Spread the Löve #

Contributors:

 - Andrew Fan (afan5@jhu.edu)
 - Bailey Parker (bailey@jhu.edu - bparke20)
 - Matt Beutler (mbeutle1@jhu.edu)
 - Patrick Castaldi (pcastal1@jhu.edu)

# Notes to the Player #

Welcome to TBA Productions' Share the Löve, an adventure of Willy the whale 
from Planet Löve on his mission to Spread the Love and rescue his friends 
from the clutches of the evil Planet Lua.

Instructions: 

  - SPACE to shoot (can either hold or rapid push)
  - SPACE to skip cutscenes
  - Arrow keys or WASD to move
  - Use mouse on menus to select options
  - F to enable blanket shield (Stage 2 onwards)

Debug Keys:

  - P to enable FPS debug in-game
  - I to enable invincibility
  - U to disable invincibility
  - L to skip to end of stage (stages only)
  - K to skip a boss attack (Final Boss only)
  - ; to fast-forward in-game

# Gameplay

The Lövebar: 
  - A bar at the top of the screen that represents your LOVE.
  - Shooting enemies depletes LOVE
  - LOVE regenerates over time, or when you LOVE enemies
  - LOVE enemies by spreading your LOVE
  - Regain lots of LOVE by successfully LOVEing an enemy
  - Colliding with an enemy or bullet will deplete 25% Max LOVE
  - Colliding with less than 25% Max LOVE will result in a Game Over

Scoring:
  - Hitting an enemy with LOVE increases score by 10
  - Fully LOVEing an enemy increases score by 50. For bosses, an extra 250 is granted
  - Sparing an enemy increases score by 75 + 10 x hp

# Graphics Resource Credits #

  - bad_whale.png - Bailey Parker
  - bg/background_gradient.png - Matt Beutler
  - bg/background_stars.png - Matt Beutler
  - bg/background_dust.png - Matt Beutler
  - bg/openWorld_stage1.png - Matt Beutler
  - bg/planet_* - Matt Beutler
  - bg/background_stage2base.jpg - Matt Beutler
  - bg/background_stage3_base.jpg - Matt Beutler
  - bg/background_stage3_clouds.png - Matt Beutler
  - bg/background_stage4.png - Matt Beutler
  - blanket_* - Bailey Parker
  - boss1_ship.png - Matt Beutler
  - boss1animation.png - LOVE (edited by Matt Beutler)
  - boss2_ship.png - Matt Beutler
  - boss2animation.png - LOVE (edited by Matt Beutler)
  - boss3_ship.png - Matt Beutler
  - bulletdelete.png - Achy (edited by Andrew Fan)
  - button_* - Matt Beutler
  - cursor.png - Bailey Parker
  - cutscenes/* - Bailey Parker (with LOVE)
  - dyingWilly.png - LOVE (edited by Matt Beutler, Andrew Fan)
  - eff_particle1.png - Andrew Fan
  - eff_particle2.png - Andrew Fan
  - enemy*.png - Matt Beutler
  - enemy1_psy.png - LOVE (edited by Matt Beutler and Bailey Parker)
  - highscores_title.png - Bailey Parker
  - laser.png - Andrew Fan
  - logo.png - Bailey Parker
  - menu_hover_* - Bailey Parker
  - menu_sprites.png - Bailey Parker
  - menu.png - Bailey Parker
  - pause_menu.png - Bailey Parker
  - settings_title.png - Bailey Parker
  - settingsBg.png - Bailey Parker
  - shotsheet.png - Andrew Fan
  - slider_notch.png - Bailey Parker
  - slider_track.png - Bailey Parker
  - spreadTheLove.png - Bailey Parker
  - textfield.png - Patrick Castaldi

# Sound Resource Credits #

  - bgm_boss1.mp3 - Andrew Fan
  - bgm_boss2.mp3 - Andrew Fan
  - bgm_boss3.mp3 - Rakohus (https://www.youtube.com/watch?v=4wqo_xcUdv0)
  - bgm_boss4.mp3 - 8 Bit Universe (https://www.youtube.com/watch?v=Z2Pa66C8Y1c)
  - bgm_stage1.mp3 - Andrew Fan
  - bgm_stage2.mp3 - Andrew Fan
  - bgm_stage3.mp3 - Andrew Fan
  - bgm_stage4.mp3 - Andrew Fan
  - menu_song.mp3 - ???
  - se_arrow.mp3 - Osabisi
  - se_astralup.mp3 - ZUN
  - se_bounce.mp3 - Osabisi
  - se_crash.mp3 - Osabisi
  - se_cutscenekidnap.mp3 - Osabisi (edited by Andrew)
  - se_darkcharge.mp3 - ZUN
  - se_dullburst.mp3 - Osabisi
  - se_eleburst.mp3 - ZUN
  - se_enemycharge.mp3 - ZUN
  - se_enemyhit.mp3 - Osabisi
  - se_enemyhit2.mp3 - Osabisi
  - se_enemyhit3.mp3 - Osabisi
  - se_enemyhit4.mp3 - Osabisi
  - se_fall.mp3 - Osabisi
  - se_gentlewave.mp3 - ZUN
  - se_longburst.mp3 - ZUN
  - se_mecha.mp3 - Osabisi
  - se_moose.mp3 - http://www.smouse.force9.co.uk
  - se_playershot.mp3 - Osabisi
  - se_powerup.mp3 - Osabisi
  - se_slicewave.mp3 - Osabisi
  - se_slash.mp3 - ZUN

# Libraries Used
  - regex.info/code/JSON.lua
