SHELL:=/usr/bin/env bash -O globstar

SRC=src/
SHADERS=$(SRC)shaders/**/*.glsl
BUILD=build/
README=README.md

NUM=XXX
ASSIGNMENT_PREFIX=cs255-assignment-
ASSIGNMENT_SUFFIX=-tba
ASSIGNMENT_EXT=.tar.gz
ASSIGNMENT=$(ASSIGNMENT_PREFIX)$(NUM)$(ASSIGNMENT_SUFFIX)


build:
	cd $(SRC); \
	moonc -t ../$(BUILD) .
	rsync -a --out-format='%n%L' --exclude '*.moon' --exclude '.*' $(SRC) $(BUILD)

assignment: clean build
	cp -r $(BUILD) $(ASSIGNMENT)
	rsync -a --include='*.moon' --include='*/' --exclude='*' $(SRC) $(ASSIGNMENT)
	cp $(README) $(ASSIGNMENT)
	GZIP=-9 tar -cvzf $(ASSIGNMENT)$(ASSIGNMENT_EXT) $(ASSIGNMENT)
	rm -rf $(ASSIGNMENT)

clean:
	rm -f "`echo $(ASSIGNMENT_PREFIX)*$(ASSIGNMENT_SUFFIX)$(ASSIGNMENT_EXT)`"
	rm -rf $(BUILD)

lintshaders:
	./tools/lintglsl/lint $(SHADERS)
